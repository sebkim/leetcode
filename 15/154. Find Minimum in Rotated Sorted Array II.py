class Solution(object):
	def __init__(self):
		self.nums = None
	def findMin(self, nums):
		"""
		:type nums: List[int]
		:rtype: int
		"""
		self.nums = nums
		return self.rec(0, len(nums)-1)
	def rec(self, lo, hi):
		while hi-lo>1 and self.nums[lo] == self.nums[lo+1]:
			lo+=1
		while hi-lo>1 and self.nums[hi] == self.nums[hi-1]:
			hi-=1
		if hi-lo <= 1:
			return min(self.nums[lo:hi+1])
		mid = (lo+hi)//2
		if self.nums[hi] - self.nums[mid] < 0:
			ret = min(self.rec(mid+1, hi), self.nums[mid])
		elif self.nums[hi] - self.nums[mid] > 0:
			ret = min(self.rec(lo, mid-1), self.nums[mid])
		return ret
