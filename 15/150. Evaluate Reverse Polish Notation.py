class Solution(object):
	def evalRPN(self, tokens):
		"""
		:type tokens: List[str]
		:rtype: int
		"""
		stack = []
		for each in tokens:
			# print stack
			if len(each[1:])>0 and (each[0]=='+' or each[0]=='-'):
				if each[1:].isdigit():
					if each[0]=='+':
						each = int(each[1:])
					elif each[0]=='-':
						each = -int(each[1:])
					stack.append(each)
			elif each.isdigit(): 
				stack.append(int(each))
			else:
				popped2 = stack.pop()
				popped1 = stack.pop()
				evalStr = "int(({}){}float({}))".format(popped1, each, popped2)
				evalRet = eval(evalStr)
				stack.append(evalRet)
				# print evalStr
		return stack[-1]
