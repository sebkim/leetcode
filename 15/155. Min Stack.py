# point is that when storing val, stores it as (val, curMin)
class MinStack(object):
	def __init__(self):
		"""
		initialize your data structure here.
		"""
		self.stack = []
		self.curMin = float('inf')
	def push(self, x):
		"""
		:type x: int
		:rtype: void
		"""
		if x < self.curMin:
			self.curMin = x
		self.stack.append((x, self.curMin))
	def pop(self):
		"""
		:rtype: void
		"""
		if len(self.stack)==0: return
		self.stack.pop()
		self.curMin = self.stack[-1][1] if len(self.stack)>0 else float('inf')
	def top(self):
		"""
		:rtype: int
		"""
		if len(self.stack)==0: return
		return self.stack[-1][0]
	def getMin(self):
		"""
		:rtype: int
		"""
		return self.curMin

# Your MinStack object will be instantiated and called as such:
# obj = MinStack()
# obj.push(x)
# obj.pop()
# param_3 = obj.top()
# param_4 = obj.getMin()