class Solution(object):
    def findMinHeightTrees(self, n, edges):
        """
        :type n: int
        :type edges: List[List[int]]
        :rtype: List[int]
        """
        if n==1 and len(edges)==0: return [0]
        adj_set = [set() for _ in xrange(n)]
        for edge in edges:
            adj_set[edge[0]].add(edge[1])
            adj_set[edge[1]].add(edge[0])
        leaves = [i for i in xrange(len(adj_set)) if len(adj_set[i])==1]
        while n>2:
            n -= len(leaves)
            new_leaves = []
            for leaf in leaves:
                leaf_nei = adj_set[leaf].pop()
                adj_set[leaf_nei].remove(leaf)
                if len(adj_set[leaf_nei]) == 1:
                    new_leaves.append(leaf_nei)
            leaves = new_leaves
        return leaves
