class Solution(object):
	def multiply(self, A, B):
		"""
		:type A: List[List[int]]
		:type B: List[List[int]]
		:rtype: List[List[int]]
		"""
		if len(A)==0 or len(B)==0: return [[]]
		m = len(A)
		n = len(A[0])
		nB = len(B[0])
		# rowDicA stores rowIndNum as key, and list that contains not 0 values' ind and val as VALUE
		rowDicA = {}
		rowDicB = {}
		for i in xrange(m):
			for j in xrange(n):
				if A[i][j]!=0:
					rowDicA.setdefault(i, []).append((j, A[i][j]))
		for i in xrange(n):
			for j in xrange(nB):
				if B[i][j]!=0:
					rowDicB.setdefault(i, []).append((j, B[i][j]))
		resMat = [[0 for _ in xrange(nB)] for _2 in xrange(m)]
		for i in xrange(m):
			if rowDicA.get(i) is None: continue
			for eachCol, eachVal in rowDicA.get(i):
				if rowDicB.get(eachCol) is None: continue
				for eachCol2, eachVal2 in rowDicB.get(eachCol):
					resMat[i][eachCol2] += eachVal * eachVal2
		return resMat

A = [
  [ 1, 0, 0],
  [-1, 0, 3]
]
B = [
  [ 7, 0, 0 ],
  [ 0, 0, 0 ],
  [ 0, 0, 1 ]
]
A=[[0]]
B=[[0]]
sol = Solution()
print sol.multiply(A, B)