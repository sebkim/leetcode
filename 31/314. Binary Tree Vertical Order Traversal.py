# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

from collections import defaultdict
class Solution(object):
	def verticalOrder(self, root):
		"""
		:type root: TreeNode
		:rtype: List[List[int]]
		"""
		#myDic stores colNum as KEY, and list that contains nodes that belong to corresponding colNum as VALUE
		myDic = defaultdict(list)
		# pythonish BFS
		q = [(root, 0)]
		for here, col in q:
			if here:
				myDic[col].append(here.val)
				q+= [(here.left, col-1), (here.right, col+1)]
		return [myDic[i] for i in sorted(myDic)]
		