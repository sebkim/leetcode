import random
class RandomizedSet(object):
	def __init__(self):
		"""
		Initialize your data structure here.
		"""
		self.myList = list()
		self.posForList = {}
	def insert(self, val):
		"""
		Inserts a value to the set. Returns true if the set did not already contain the specified element.
		:type val: int
		:rtype: bool
		"""
		if val not in self.posForList:
			self.myList.append(val)
			self.posForList[val] = len(self.myList)-1
			return True
		return False
	def remove(self, val):
		"""
		Removes a value from the set. Returns true if the set contained the specified element.
		:type val: int
		:rtype: bool
		"""
		if val not in self.posForList:
			return False
		pos = self.posForList[val]
		last = self.myList[-1]
		self.myList[pos] = last
		self.myList.pop()
		self.posForList[last] = pos 
		self.posForList.pop(val)
		return True
	def getRandom(self):
		"""
		Get a random element from the set.
		:rtype: int
		"""
		return self.myList[random.randint(0, len(self.myList)-1)]

# Your RandomizedSet object will be instantiated and called as such:
# obj = RandomizedSet()
# param_1 = obj.insert(val)
# param_2 = obj.remove(val)
# param_3 = obj.getRandom()
obj = RandomizedSet()
obj.insert(1)
obj.insert(5)
print obj.remove(1)
print obj.remove(5)