# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
	def postorderTraversal(self, root):
		"""
		:type root: TreeNode
		:rtype: List[int]
		"""
		# stack
		fs = []
		ss = []
		if root is None: return []
		fs.append(root)
		while len(fs)!=0:
			popped = fs.pop()
			ss.append(popped.val)
			if popped.left:
				fs.append(popped.left)
			if popped.right:
				fs.append(popped.right)
		return list(reversed(ss))
