# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
	# floyd's cycle finding (tortoise and hare algo)
	def hasCycle(self, head):
		"""
		:type head: ListNode
		:rtype: bool
		"""
		if head is None: return False
		slow = fast = head
		while True:
			slow = slow.next
			if fast.next is not None:
				fast = fast.next.next
			else:
				return False
			if slow is None or fast is None:
				return False
			if slow is fast:
				return True
