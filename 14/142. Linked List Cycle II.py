# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

# class Solution(object):
# 	def detectCycle(self, head):
# 		"""
# 		:type head: ListNode
# 		:rtype: ListNode
# 		"""
# 		discovered = {}
# 		cur = head
# 		while cur:
# 			if discovered.get(cur) is None:
# 				discovered[cur] = True
# 			else:
# 				return cur
# 			cur = cur.next
# 		return None

class Solution(object):
	def detectCycle(self, head):
		"""
		:type head: ListNode
		:rtype: ListNode
		"""
		if head is None: return None
		entry = head
		slow = head
		fast = head
		while fast.next and fast.next.next:
			slow = slow.next
			fast = fast.next.next
			if slow is fast:
				while entry is not slow:
					entry = entry.next
					slow = slow.next
				return entry
		return None
