# Definition for a point.
# class Point(object):
#     def __init__(self, a=0, b=0):
#         self.x = a
#         self.y = b

class Solution(object):
	def maxPoints(self, points):
		"""
		:type points: List[Point]
		:rtype: int
		"""
		dic = {}
		vertical = {}
		horizontal = {}
		if len(points)==0: return 0
		for sourcePoint in points:
			for targetPoint in points:
				if sourcePoint is targetPoint: continue
				dy = targetPoint.y - sourcePoint.y
				dx = targetPoint.x - sourcePoint.x
				if dx==0. or dy==0.:
					if dx==0:
						if vertical.get(targetPoint.x) is None:
							vertical[targetPoint.x] = set()
						vertical[targetPoint.x].add(targetPoint)
						vertical[targetPoint.x].add(sourcePoint)
					if dy==0:
						if horizontal.get(targetPoint.y) is None:
							horizontal[targetPoint.y] = set()
						horizontal[targetPoint.y].add(targetPoint)
						horizontal[targetPoint.y].add(sourcePoint)	
				else:
					slope = dy / float(dx)
					hereKey = (slope, sourcePoint.x, sourcePoint.y)
					if dic.get(hereKey) is None:
						dic[hereKey] = set()
					dic[hereKey].add(targetPoint)
					dic[hereKey].add(sourcePoint)
		ret = max(len(dic[k]) for k in dic.keys()) if dic.keys()!=[] else 1
		ret = max(ret, *(len(vertical[k]) for k in vertical.keys())) if vertical.keys()!=[] else ret
		ret = max(ret, *(len(horizontal[k]) for k in horizontal.keys())) if horizontal.keys()!=[] else ret
		return ret
		