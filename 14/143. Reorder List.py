# Definition for singly-linked list.
# class ListNode(object):
# 	def __init__(self, x):
# 		self.val = x
# 		self.next = None

class Solution(object):
	def reorderList(self, head):
		"""
		:type head: ListNode
		:rtype: void Do not return anything, modify head in-place instead.
		"""
		size = self.getSize(head)
		if size<=2: return
		stack = []
		cur = head
		# mode 0 is to jump far and save next to stack, and 1 is pop from stack and glue
		mode=0
		jumpDegree = size-1
		while cur:
			if mode==0:
				if jumpDegree<=0:
					cur.next = None
					return
				stack.append(cur.next)
				farNode = cur
				for i in range(jumpDegree):
					farNode = farNode.next
				cur.next = farNode
				jumpDegree -= 2
				mode=1
			elif mode==1:
				popped = stack.pop()
				if popped == cur:
					cur.next = None
					return
				cur.next = popped
				mode=0
			cur = cur.next
	def getSize(self, head):
		size = 0
		cur = head
		while cur:
			size+=1
			cur=cur.next
		return size
