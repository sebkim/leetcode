# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
	def preorderTraversal(self, root):
		"""
		:type root: TreeNode
		:rtype: List[int]
		"""
		res = []
		stack = []
		if root is None: return []
		stack.append(root)
		while len(stack)!=0:
			cur = stack.pop()
			res.append(cur.val)
			if cur.right:
				stack.append(cur.right)
			if cur.left:
				stack.append(cur.left)
		return res