# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
	def insertionSortList(self, head):
		"""
		:type head: ListNode
		:rtype: ListNode
		"""
		size = self.getSize(head)
		if size<=1: return head
		boolBreak = False
		for i in xrange(1, size):
			comp = head
			cur = head
			nodeBeforeCur = None
			for j in xrange(i):
				comp=comp.next
			while comp.val >= cur.val:
				if cur is comp:
					boolBreak = True
					break
				nodeBeforeCur = cur
				cur = cur.next
			if boolBreak:
				boolBreak = False
				continue
			prevCompNext = comp.next
			nodeBeforeComp = head
			for j in xrange(i-1):
				nodeBeforeComp = nodeBeforeComp.next
			comp.next = cur
			if nodeBeforeCur:
				nodeBeforeCur.next = comp
			nodeBeforeComp.next = prevCompNext
			if cur is head: head = comp
		return head
	def getSize(self, head):
		size = 0
		cur = head
		while cur:
			size+=1
			cur=cur.next
		return size
