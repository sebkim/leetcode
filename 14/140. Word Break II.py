import copy
class Solution(object):
	def __init__(self):
		self.wordDict = None
		self.cached = None
		self.s = None
		self.possibleSize = None
	def wordBreak(self, s, wordDict):
		"""
		:type s: str
		:type wordDict: Set[str]
		:rtype: bool
		"""
		self.s = s
		self.wordDict = wordDict
		self.cached = [ [None] for i in range(len(s))]
		self.possibleSize = [ [] for i in range(len(s))]
		if len(wordDict)==0: return []
		self.rec(0)
		print self.possibleSize
		print self.cached
		res = []
		self.recon([], 0, res)
		processedRes = []
		for i in res:
			processedRes.append(' '.join(i))
		return processedRes
	def rec(self, startInd):
		if startInd == len(self.s): return True
		cac = self.cached[startInd]
		if cac[0] is not None: return cac[0]
		ret = False
		for candSize in xrange(1, len(self.s)-startInd+1):
			isThisInDict = self.s[startInd:startInd+candSize] in self.wordDict
			if self.rec(startInd+candSize) & isThisInDict:
				self.possibleSize[startInd].append(candSize)
				ret = True
		cac[0] = ret
		return ret
	def recon(self, partial, startInd, res):
		if startInd == len(self.s):
			res.append(copy.copy(partial))
			return
		for ps in self.possibleSize[startInd]:
			partial.append(self.s[startInd:startInd+ps])
			self.recon(partial, startInd + ps, res)
			partial.pop()

sol = Solution()
# s = 'leetcode'
# wd = set(['leet', 'code'])
s = 'aaaaaaa'
wd = set(['aaaa', 'aaa'])
# s="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab"
# wd=["a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"]
s="catsanddog"
wd=set(["cat","cats","and","sand","dog"])
print sol.wordBreak(s, wd)
