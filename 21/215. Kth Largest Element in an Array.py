from heapq import nlargest
import heapq
from copy import copy
from random import choice
class Solution(object):
	def findKthLargest(self, nums, k):
		"""
		:type nums: List[int]
		:type k: int
		:rtype: int
		"""
		## it is just using function in python
		# return nlargest(k, nums)[k-1]
		## it uses selection algorithm (randomized).
		return self.findKth(nums, len(nums)-k+1)
		# return self.findKthLargestUsingHeap(nums, k)
	def findKth(self, nums, k):
		if len(set(nums))==1: return nums[0]
		pivot = choice(nums)
		L, R = Solution.split(nums, pivot)
		if k<len(L)+1: return self.findKth(L, k)
		else: return self.findKth(R, k-len(L))
	@staticmethod
	def split(nums, pivot):
		leftWall = -1
		# res = copy(nums)
		res = nums
		for i in xrange(len(res)):
			if res[i] <= pivot:
				leftWall+=1
				res[i], res[leftWall] = res[leftWall], res[i]
		L, R = res[:leftWall+1], res[leftWall+1:]
		return L,R
	def findKthLargestUsingHeap(self, nums, k):
		heap = [-i for i in nums]
		heapq.heapify(heap)
		for i in xrange(k):
			ret = heapq.heappop(heap)
		return -ret

from random import shuffle
nums = range(1000000)
shuffle(nums)
sol = Solution()
print sol.findKthLargest(nums, 1000000)
