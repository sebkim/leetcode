class Solution(object):
	def numberOfPatterns(self, m, n):
		"""
		:type m: int
		:type n: int
		:rtype: int
		"""
		ret = 0
		for i in range(m, n+1):
			ret += self.rec([1], 1, i)*4
			ret += self.rec([2], 1, i)*4
			ret += self.rec([5], 1, i)
		return ret
	def rec(self, partial, k, lenKey):
		if len(partial) == lenKey:
			return 1
		ret = 0
		if len(partial)==0:
			cands= range(1, 10)
		else:
			lastNum = partial[-1]
			cands = self.getNeighbors(partial, lastNum)
		for c in cands:
			partial.append(c)
			ret += self.rec(partial, k+1, lenKey)
			partial.pop()
		return ret
	def getNeighbors(self, partial, here):
		neighDic = {1: [2,4,5,6,8], 2: [1,3,4,5,6,7,9], 3:[2,4,5,6,8], 4: [1,2,3,5,7,8,9], \
		 5: [1,2,3,4,6,7,8,9], 6: [1,2,3,5,7,8,9], 7:[2,4,5,6,8], 8:[1,3,4,5,6,7,9], 9: [2,4,5,6,8]}
		cands = neighDic[here]
		res = []
		for c in cands:
			if c not in partial:
				res.append(c)
		jumps = {2:[[[1,3], [3,1]]], 4:[[[1,7], [7,1]]], 6:[[[3,9], [9,3]]], 8:[[[7,9], [9,7]]],\
		 5:[[[2,8], [8,2]], [[4,6], [6,4]], [[1,9], [9,1]], [[3,7], [7,3]]]}
		for between in jumps.keys():
			if between in partial:
				for eachPair in jumps[between]:
					for i in range(2):
						thing = eachPair[i][0]
						counter = eachPair[i][1]
						if thing == here and counter not in partial: res.append(counter)
		return res
		
m=8
n=8
sol = Solution()
print sol.numberOfPatterns(m, n)
