from collections import defaultdict
import heapq
from collections import deque
from itertools import islice, count
class Twitter(object):
	def __init__(self):
		"""
		Initialize your data structure here.
		"""
		self.postedTweet = defaultdict(deque)
		self.followDic = defaultdict(set)
		self.timer = count(step=-1)
	def postTweet(self, userId, tweetId):
		"""
		Compose a new tweet.
		:type userId: int
		:type tweetId: int
		:rtype: void
		"""
		self.postedTweet[userId].appendleft((next(self.timer), tweetId))
	def getNewsFeed(self, userId):
		"""
		Retrieve the 10 most recent tweet ids in the user's news feed. Each item in the news feed must be posted by users who the user followed or by the user herself. Tweets must be ordered from most recent to least recent.
		:type userId: int
		:rtype: List[int]
		"""
		checkUsers = self.followDic[userId] | set([userId])
		print [self.postedTweet[u] for u in checkUsers]
		tweets = heapq.merge(*(self.postedTweet[u] for u in checkUsers))
		# tweets = heapq.merge((self.postedTweet[u] for u in checkUsers))
		print list(tweets)
		tweets = [t for _, t in islice(tweets, 10)]
		return tweets
	def follow(self, followerId, followeeId):
		"""
		Follower follows a followee. If the operation is invalid, it should be a no-op.
		:type followerId: int
		:type followeeId: int
		:rtype: void
		"""
		if followeeId not in self.followDic[followerId]:
			self.followDic[followerId].add(followeeId)
	def unfollow(self, followerId, followeeId):
		"""
		Follower unfollows a followee. If the operation is invalid, it should be a no-op.
		:type followerId: int
		:type followeeId: int
		:rtype: void
		"""
		if followeeId in self.followDic[followerId]:
			self.followDic[followerId].remove(followeeId)

# Your Twitter object will be instantiated and called as such:
obj = Twitter()
userId = 0
tweetId= 1
followerId = 0
followeeId = 1
obj.postTweet(userId,tweetId)
obj.postTweet(0, 5)
obj.postTweet(1, 10)
obj.follow(followerId,followeeId)
# obj.unfollow(followerId,followeeId)
param_2 = obj.getNewsFeed(userId)
print param_2
