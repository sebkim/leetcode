class Solution(object):
	def simplifyPath(self, path):
		"""
		:type path: str
		:rtype: str
		"""
		stack = []
		rawPath = [i for i in path.split('/') if i!='']
		for each in rawPath:
			if each=='.':
				pass
			else:
				if len(stack)==0 and each=='..':
					pass
				if len(stack)>0 and each=='..':
					stack.pop()
				if each!='..':
					stack.append(each)
		res = "/"
		for each in stack:
			res+=each + '/'
		res = res[:-1] if res != "/" else res
		return res

sol = Solution()
# path = "/a/./b/../../c/"
path = '/'
print sol.simplifyPath(path)