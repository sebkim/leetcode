class Solution(object):
	def __init__(self):
		self.cached = None
		self.n = None
	def climbStairs(self, n):
		"""
		:type n: int
		:rtype: int
		"""
		self.cached = [[None] for i in xrange(n+1)]
		self.n = n
		return self.fiboLike(0)
		
	def fiboLike(self, a):
		if a>=self.n-1:
			return 1
		ret = self.cached[a]
		if ret[0] is not None: return ret[0]
		ret[0] = self.fiboLike(a+1) + self.fiboLike(a+2)
		return ret[0]
	
sol = Solution()
m=7
print sol.climbStairs(m)