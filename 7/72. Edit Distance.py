class Solution(object):
	def __init__(self):
		self.cached = None
		self.word1 = None
		self.word2 = None
		import sys
		sys.setrecursionlimit(10000)
		self.recDepth = 0; self.maxRecDepth = 0;
	def minDistance(self, word1, word2):
		"""
		:type word1: str
		:type word2: str
		:rtype: int
		"""
		self.word1 = word1
		self.word2 = word2
		nWord1 = len(word1); nWord2 = len(word2)
		self.cached = [[[None] for i in xrange(nWord2+1)] for j in xrange(nWord1+1)]	
		if (nWord1==0 or nWord2==0):
			return max(nWord1, nWord2) 
		return self.editDist(nWord1, nWord2)
		# self.editDist(nWord1, nWord2); print self.maxRecDepth;
		
	def editDist(self, y, x):
		if y<0 or x<0:
			return float('inf')
		if y==0 and x==0:
			return 0
		ret = self.cached[y][x]
		if ret[0] is not None: return ret[0]
		# self.recDepth+=1
		# self.maxRecDepth = max(self.maxRecDepth, self.recDepth)
		ret[0] = min(self.editDist(y, x-1)+1, self.editDist(y-1, x)+1)
		ret[0] = min(ret[0], self.editDist(y-1, x-1)) if self.word1[y-1] == self.word2[x-1] \
		else min(ret[0], self.editDist(y-1, x-1) +1)
		# self.recDepth-=1
		return ret[0]

sol = Solution()
word1 = 'sea'
word2 = 'eat'
word1="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdef"
word2="bcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefg"
print len(word1), len(word2)
print sol.minDistance(word1, word2)
