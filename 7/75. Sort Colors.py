class Solution(object):
	def sortColors(self, nums):
		"""
		:type nums: List[int]
		:rtype: void Do not return anything, modify nums in-place instead.
		"""
		from collections import Counter
		c = Counter(nums)
		zero = c.get(0)
		one = c.get(1)
		two = c.get(2)
		itera=0
		if zero:
			for i in range(zero):
				nums[itera] = 0
				itera+=1
		if one:
			for i in range(one):
				nums[itera] = 1
				itera+=1
		if two:
			for i in range(two):
				nums[itera] = 2
				itera+=1
		return nums

sol = Solution()
print sol.sortColors([0,1,2,2,1])
