class Solution(object):
	def setZeroes(self, matrix):
		"""
		:type matrix: List[List[int]]
		:rtype: void Do not return anything, modify matrix in-place instead.
		"""
		if matrix==[]:
			return
		m = len(matrix)
		n = len(matrix[0])
		whichIJ = []
		for i in range(len(matrix)):
			for j in range(len(matrix[0])):
				if matrix[i][j] == 0:
					whichIJ.append((i,j))
		whichIJToRemove = set()
		for i in range(len(whichIJ)):
			for itera in range(2):
				if itera==0:
					for eachTuple in ((whichIJ[i][itera],x) for x in range(n)):
						whichIJToRemove.add(eachTuple)
				else:
					for eachTuple in ((x,whichIJ[i][itera]) for x in range(m)):
						whichIJToRemove.add(eachTuple)
		for i in whichIJToRemove:
			matrix[i[0]][i[1]] = 0


sol = Solution()
matrix = [[1,0,3],[2,1,0], [4,5,6]]
sol.setZeroes(matrix)
print matrix