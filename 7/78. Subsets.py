class Solution(object):
	def __init__(self):
		import copy
	def subsets(self, nums):
		"""
		:type nums: List[int]
		:rtype: List[List[int]]
		"""
		partial = []
		res = []
		self.subsetsRec(partial, 0, nums, res)
		return res
	def subsetsRec(self, partial, k, nums, res):
		if k==len(nums):
			res.append([i for iInd, i in enumerate(nums) if partial[iInd]])
			return
		cand = [False, True]
		for c in cand:
			partial.append(c)
			self.subsetsRec(partial, k+1, nums, res)
			partial.pop()
        
sol = Solution()
nums = [1,2,3]
print sol.subsets(nums)