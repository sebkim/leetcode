class Solution(object):
	def __init__(self):
		self.usedMap = []
		self.complete = False

	def exist(self, board, word):
		"""
		:type board: List[List[str]]
		:type word: str
		:rtype: bool
		"""
		a = []
		for i in range(len(board)):
			self.usedMap.append([False for j in range(len(board[0]))])

		self.backtrack(a, 0, board, word)
		if self.complete:
			return True
		else:
			return False

	def backtrack(self, a, k, board, word):
		if self.complete:
			return
		c = []
		if self.is_a_solution(a, k, board, word):
			self.process_solution(a, k, board, word)
		else:
			k+=1
			self.construct_candidates(a, k, c, board, word)
			for eachC in c:
				a.append(eachC)
				self.usedMap[eachC[0]][eachC[1]] = True
				self.backtrack(a, k, board, word)
				popped = a.pop()
				self.usedMap[popped[0]][popped[1]] = False
	def is_a_solution(self, a, k, board, word):
		if k==len(word):
			return True
		else:
			return False
	def process_solution(self, a, k, board, word):
		# print a
		self.complete = True
	def construct_candidates(self, a, k, c, board, word):
		if k==1:
			for i in range(len(board)):
				for j in range(len(board[0])):
					if board[i][j]==word[0]:
						if not self.usedMap[i][j]:
							c.append((i,j))
		else:
			nowIJ = a[k-2]
			if nowIJ[1]>0:
				if board [ nowIJ[0] ] [nowIJ[1] - 1] == word[k-1]:
					if not self.usedMap[nowIJ[0]][nowIJ[1]-1]:
						c.append((nowIJ[0], nowIJ[1]-1))
			if nowIJ[1]<len(board[0])-1:
				if board [ nowIJ[0] ] [nowIJ[1] +1 ] == word[k-1]:
					if not self.usedMap[nowIJ[0]][nowIJ[1]+1]:
						c.append((nowIJ[0], nowIJ[1]+1))
			if nowIJ[0] > 0:
				if board[nowIJ[0]-1][nowIJ[1]] == word[k-1]:
					if not self.usedMap[nowIJ[0]-1][nowIJ[1]]:
						c.append((nowIJ[0]-1, nowIJ[1]))
			if nowIJ[0] < len(board)-1:
				if board[nowIJ[0]+1][nowIJ[1]] == word[k-1]:
					if not self.usedMap[nowIJ[0]+1][nowIJ[1]]:
						c.append((nowIJ[0]+1, nowIJ[1]))

sol = Solution()
# board = [
#   ['A','B','C','E'],
#   ['S','F','C','S'],
#   ['A','D','E','E']
# ]
# word = 'BCE'
board = [
  ['A','A']
]
word = 'AA'
board=["ABCE","SFES","ADEE"]
word="ABCESEEEFS"
res = sol.exist(board, word)
print res
