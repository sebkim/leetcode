import copy
class Solution(object):
	def __init__(self):
		self.res = []

	def combine(self, n, k):
		"""
		:type n: int
		:type k: int
		:rtype: List[List[int]]
		"""
		a = []
		self.backtrack(a, 0, n, k)
		return self.res

	def backtrack(self, a, k, n, r):
		c = []
		if self.is_a_solution(a, k, n, r):
			self.process_solution(a, k, n, r)
		else:
			k+=1
			self.construct_candidates(a, k, c, n, r)
			for eachC in c:
				a.append(eachC)
				self.backtrack(a, k, n, r)
				a.pop()
	def is_a_solution(self, a, k, n, r):
		if k==r:
			return True
		else:
			return False

	def process_solution(self, a, k, n, r):
		self.res.append(copy.copy(a))

	def construct_candidates(self, a, k, c, n, r):
		cand = (i for i in xrange(1, n+1) if all(eachA < i for eachA in a))
		for eachC in cand:
			c.append(eachC)

        
sol = Solution()
res = sol.combine(20, 2)
print res
