class Solution(object):
	def __init__(self):
		self.usedMap = None
		self.board = None
		self.word = None
		## this is for 8 direction
		# self.dy = [-1, -1, -1, 0, 1, 1, 1, 0]
		# self.dx = [-1, 0, 1, 1, 1, 0, -1, -1]
		## this is for 4 direction
		self.dy = [-1, 0, 0, 1]
		self.dx = [0, -1, 1, 0]
	def exist(self, board, word):
		"""
		:type board: List[List[str]]
		:type word: str
		:rtype: bool
		"""
		self.usedMap = [[False for i in range(len(board[0]))] for j in range(len(board))]
		self.board = board
		self.word = word
		for y in range(len(board)):
			for x in range(len(board[0])):
				if self.wordSearch(0, y, x):
					return True
		return False
		
	def wordSearch(self, posInWord, y, x):
		# base case
		if y>= len(self.board) or x>= len(self.board[0]) or y<0 or x<0:
			return False
		##
		# base case
		if self.word[posInWord] != self.board[y][x]:
			return False
		if len(self.word[posInWord:]) == 1:
			return True
		##
		for i in range(len(self.dy)):
			nextY = y+self.dy[i]; nextX = x+self.dx[i];
			if (0<= nextY < len(self.board)) and\
			(0<= nextX < len(self.board[0])) and\
			self.usedMap[nextY][nextX] is False:
				self.usedMap[y][x] = True
				if self.wordSearch(posInWord+1, nextY, nextX):
					return True
				self.usedMap[y][x] = False
			else: continue
		return False
	
sol = Solution()

board = [
  ['A','B','C','E'],
  ['S','F','C','S'],
  ['A','D','E','E']
]
word = 'BCD'
board=["ABCE","SFES","ADEE"]
word="ABCESEEEFS"
# board = [
#   ['A','A']
# ]
# word = 'AA'
res = sol.exist(board, word)
print res
