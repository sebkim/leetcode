# Definition for a binary tree node.
class TreeNode(object):
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None

class Solution(object):
	def sortedArrayToBST(self, nums):
		"""
		:type nums: List[int]
		:rtype: TreeNode
		"""
		if nums == []:
			return None
		return self.buildHelper(nums)
	def buildHelper(self, nums):
		midInd = len(nums)//2
		node = TreeNode(nums[midInd])
		# if leftSub
		if nums[:midInd]:
			node.left = self.buildHelper(nums[:midInd])
		if nums[midInd+1:]:
			node.right = self.buildHelper(nums[midInd+1:])
		return node