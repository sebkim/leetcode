# Definition for a binary tree node.
class TreeNode(object):
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None

class Solution(object):
	def __init__(self):
		self.postInd = None
	def buildTree(self, inorder, postorder):
		"""
		:type inorder: List[int]
		:type postorder: List[int]
		:rtype: TreeNode
		"""
		if postorder == inorder == []:
			return None
		self.postInd = len(postorder)-1
		return self.buildHelper(inorder, postorder, 0, len(inorder))
	def buildHelper(self, inorder, postorder, start, end):
		node = TreeNode(postorder[self.postInd])
		middleInd = inorder.index(postorder[self.postInd])
		self.postInd-=1
		# if rightSub exists
		if inorder[middleInd+1:end]:
			node.right = self.buildHelper(inorder, postorder, middleInd+1, end)
		# if leftSub exists
		if inorder[start:middleInd]:
			node.left = self.buildHelper(inorder, postorder, start, middleInd)
		return node
	

      