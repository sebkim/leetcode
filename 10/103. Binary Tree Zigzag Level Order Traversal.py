from collections import deque
# Definition for a binary tree node.
class TreeNode(object):
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None

class Solution(object):
	def zigzagLevelOrder(self, root):
		"""
		:type root: TreeNode
		:rtype: List[List[int]]
		"""
		res = []
		queue = deque([])
		if root is None:
			return []
		queue.append((root, 0))
		fromRight = True
		prevLevel = -1
		while len(queue)!=0:
			popped = queue.popleft()
			if prevLevel != popped[1]:
				if prevLevel != -1:
					if fromRight:
						res.append(list(reversed(thisLevelList)))
					else:
						res.append(thisLevelList)
				thisLevelList = []
				fromRight = not fromRight
			thisLevelList.append(popped[0].val)
			if popped[0].left:
				queue.append((popped[0].left, popped[1]+1))
			if popped[0].right:
				queue.append((popped[0].right, popped[1]+1))
			prevLevel = popped[1]
		if len(thisLevelList)!=0:
			if fromRight:
				res.append(list(reversed(thisLevelList)))
			else:
				res.append(thisLevelList)
		return res
