# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

from collections import deque
class Solution(object):
	def levelOrder(self, root):
		"""
		:type root: TreeNode
		:rtype: List[List[int]]
		"""
		res = []
		if root is None: return []
		q = deque()
		prevLevel = None
		partial = []
		q.append((root, 0))
		while len(q)!=0:
			popped = q.popleft()
			level = popped[1]
			if prevLevel is not None and prevLevel != level:
				res.append(partial)
				partial = []
			partial.append(popped[0].val)
			if popped[0].left:
				q.append((popped[0].left, level+1))
			if popped[0].right:
				q.append((popped[0].right, level+1))
			prevLevel = level
		if len(partial)!=0: res.append(partial)
		return res

        