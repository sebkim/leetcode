# Definition for a binary tree node.
class TreeNode(object):
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None

class Solution(object):
	def buildTree(self, preorder, inorder):
		"""
		:type preorder: List[int]
		:type inorder: List[int]
		:rtype: TreeNode
		"""
		if preorder == inorder == []:
			return None
		return self.buildHelper(preorder, inorder)
	def buildHelper(self, preorder, inorder):
		# print preorder
		# print inorder
		element, leftSub, rightSub = self.partitionHelper(preorder, inorder)
		node = TreeNode(element)
		if leftSub:
			node.left = self.buildHelper(preorder[1:len(leftSub)+1], leftSub)
		if rightSub:
			node.right = self.buildHelper(preorder[len(leftSub)+1:], rightSub)
		return node
	def partitionHelper(self, preorder, inorder):
		middle = inorder.index(preorder[0])
		return (inorder[middle], inorder[:middle], inorder[middle+1:])
