# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
	def isSymmetric(self, root):
		"""
		:type root: TreeNode
		:rtype: bool
		"""
		if root is None: return True
		return self.IsMirror(root.left, root.right)
	def IsMirror(self, p, q):
		if p is None and q is None: return True
		if p is None or q is None: return False
		return p.val == q.val and self.IsMirror(p.left, q.right) and self.IsMirror(p.right, q.left)
