# Definition for singly-linked list.
class ListNode(object):
	def __init__(self, x):
		self.val = x
		self.next = None

# Definition for a binary tree node.
class TreeNode(object):
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None

class Solution(object):
	def sortedListToBST(self, head):
		"""
		:type head: ListNode
		:rtype: TreeNode
		"""
		if head is None:
			return None
		return self.buildHelper(head)
	def buildHelper(self, head):
		node = TreeNode(None)
		lenListOfHead = self.lenList(head)
		midInd = lenListOfHead//2
		leftSub = None
		rightSub = None
		cur = head
		# if leftSub exists
		if midInd > 0:
			leftSub = ListNode(cur.val)
			leftSubCur = leftSub
			cur=cur.next
			for i in range(midInd-1):
				lNode = ListNode(cur.val)
				leftSubCur.next = lNode
				leftSubCur = leftSubCur.next
				cur = cur.next
		midElem = cur.val
		cur=cur.next
		# if rightSub exists
		if lenListOfHead>=3:
			rightSub = ListNode(cur.val)
			rightSubCur = rightSub
			cur=cur.next
			while cur:
				lNode = ListNode(cur.val)
				rightSubCur.next = lNode
				rightSubCur = rightSubCur.next
				cur=cur.next
		if leftSub:
			node.left = self.buildHelper(leftSub)
		node.val = midElem
		if rightSub:
			node.right = self.buildHelper(rightSub)
		return node
	def lenList(self, head):
		cur = head
		count=0
		while cur:
			count+=1
			cur = cur.next
		return count
