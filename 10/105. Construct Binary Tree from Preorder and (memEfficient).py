# Definition for a binary tree node.
class TreeNode(object):
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None

class Solution(object):
	def __init__(self):
		self.preInd = 0
	def buildTree(self, preorder, inorder):
		"""
		:type preorder: List[int]
		:type inorder: List[int]
		:rtype: TreeNode
		"""
		if preorder == inorder == []:
			return None
		return self.buildHelper(preorder, inorder, 0, len(preorder))
	def buildHelper(self, preorder, inorder, start, end):
		node = TreeNode(preorder[self.preInd])
		middleInd = inorder.index(preorder[self.preInd])
		self.preInd+=1
		# if leftSub exists
		if inorder[start:middleInd]:
			node.left = self.buildHelper(preorder, inorder, start, middleInd)
		# if rightSub exists
		if inorder[middleInd+1:end]:
			node.right = self.buildHelper(preorder, inorder, middleInd+1, end)
		return node
	

