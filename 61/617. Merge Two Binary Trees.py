# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def mergeTrees(self, t1, t2):
        """
        :type t1: TreeNode
        :type t2: TreeNode
        :rtype: TreeNode
        """
        if t1 is None and t2 is None: return None
        elif t1 is None:
            new_node = TreeNode(t2.val)
            new_node.left = self.mergeTrees(t2.left, None)
            new_node.right = self.mergeTrees(t2.right, None)
            return new_node
        elif t2 is None:
            new_node = TreeNode(t1.val)
            new_node.left = self.mergeTrees(t1.left, None)
            new_node.right = self.mergeTrees(t1.right, None)
            return new_node
        new_node = TreeNode( (t1.val if t1 else 0) + (t2.val if t2 else 0))
        new_node.left = self.mergeTrees((t1.left if t1.left else None), (t2.left if t2.left else None))
        new_node.right = self.mergeTrees((t1.right if t1.right else None), (t2.right if t2.right else None))
        return new_node
