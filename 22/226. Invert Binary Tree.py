# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def invertTree(self, root):
        """
        :type root: TreeNode
        :rtype: TreeNode
        """
        if root is None: return None
        return self.rec_invert(root)
    def rec_invert(self, root):
        left = None; right = None
        if root.left:
            left = self.rec_invert(root.left)
        if root.right:
            right = self.rec_invert(root.right)
        root.left = right if right is not None else None
        root.right = left if left is not None else None
        return root
