# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def countNodes(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if root is None: return 0
        return self.count_rec(root)
    def height(self, root):
        if root.left is None and root.right is None:
            return 1
        lh = 0
        if root.left:
            lh = self.height(root.left)
        return lh+1
    def count_rec(self, root):
        h = self.height(root)
        if h==1:
            return 1
        else:
            rh = self.height(root.right) if root.right is not None else 0
            if h == rh + 1:
                return pow(2, rh) + self.count_rec(root.right)
            else:
                return pow(2, rh) + self.count_rec(root.left)
