# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def tree2str(self, t):
        """
        :type t: TreeNode
        :rtype: str
        """
        ret_str = ""
        if t is not None:
            ret_str = "{}".format(t.val)
            left_str = self.tree2str(t.left)
            right_str = self.tree2str(t.right)
            if left_str == '' and right_str != '':
                ret_str += "()"
                ret_str += "({})".format(right_str)
            else:
                if left_str != '':
                    ret_str += "({})".format(left_str)
                if right_str != '':
                    ret_str += "({})".format(right_str)
        return ret_str
