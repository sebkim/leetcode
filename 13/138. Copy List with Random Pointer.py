# Definition for singly-linked list with a random pointer.
# class RandomListNode(object):
#     def __init__(self, x):
#         self.label = x
#         self.next = None
#         self.random = None

class Solution(object):
	def copyRandomList(self, head):
		"""
		:type head: RandomListNode
		:rtype: RandomListNode
		"""
		if head is None: return None
		discoveredRandom = {}
		newHead = RandomListNode(head.label)
		cur = head
		newCur = newHead
		while cur:
			if cur.next:
				newNext = RandomListNode(cur.next.label)
				newCur.next = newNext
			if cur.random:
				if discoveredRandom.get(cur.random) is None:
					newRandom = RandomListNode(cur.random.label)
					discoveredRandom[cur.random] = newRandom
					newCur.random = newRandom
				else:
					newCur.random = discoveredRandom.get(cur.random)
			cur = cur.next
			newCur = newCur.next
		return newHead
