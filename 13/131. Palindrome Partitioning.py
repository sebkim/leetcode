import sys
import copy
class Solution(object):
	def __init__(self):
		self.s = None
		# sys.setrecursionlimit(10000)
	def partition(self, s):
		"""
		:type s: str
		:rtype: List[List[str]]
		"""
		self.s = s
		res = []
		self.rec([], s, res)
		return res
	def rec(self, partial, remainingS, res):
		if len(remainingS)==0:
			res.append(copy.copy(partial))
			return
		for cand in range(1, len(remainingS)+1):
			hereS = remainingS[:cand]
			if self.isPalin(hereS):
				partial.append(hereS)
				self.rec(partial, remainingS[cand:], res)
				partial.pop()
			else: continue
	def isPalin(self, s):
		if len(s)%2==0: middle = len(s)//2
		else: middle = len(s)//2+1
		middle-=1
		ret = True
		for i in range(middle+1):
			if s[i] != s[len(s)-i-1]: return False
		return ret

sol = Solution()
s='aab'
# s="amanaplanacanalpanama"
# s='efe'
print sol.partition(s)
