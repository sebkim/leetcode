import sys
from collections import deque
class Solution(object):
	def __init__(self):
		self.board = None
		self.discoverMap = None
		# self.dy = [-1, -1, -1, 0, 1, 1, 1, 0]
		# self.dx = [-1, 0, 1, 1, 1, 0, -1, -1]
		self.dy = [-1, 0, 1, 0]
		self.dx = [0, 1, 0, -1]
		self.isBoundaryCollide = None
		sys.setrecursionlimit(10000)
	def solve(self, board):
		"""
		:type board: List[List[str]]
		:rtype: void Do not return anything, modify board in-place instead.
		"""
		temBoard = [list(i) for i in board]
		self.board = temBoard
		self.discoverMap = [[False for i in range(len(board[0]))] for j in range(len(board))]
		for rowInd, row in enumerate(board):
			for eachInd, each in enumerate(row):
				self.discoverMap[rowInd][eachInd] = True if each=='X' else False
		for i in range(1, len(board)-1):
			for j in range(1, len(board[0])-1):
				if self.discoverMap[i][j] is False:
					exploredCells = []
					self.isBoundaryCollide = False
					# self.dfs(i, j, exploredCells)
					self.bfs(i, j, exploredCells)
					if not self.isBoundaryCollide:
						for eachCell in exploredCells:
							temBoard[eachCell[0]][eachCell[1]] = 'X'
		temBoard = [''.join(i) for i in temBoard]
		for i in range(len(board)):
			board[i] = temBoard[i]
		# print board
	def dfs(self, rowHere, colHere, exploredCells):
		self.discoverMap[rowHere][colHere] = True
		# if here is boundary, 
		if rowHere==0 or rowHere==len(self.board)-1 or colHere==0 or colHere==len(self.board[0])-1:
			self.isBoundaryCollide = True
		for dy, dx in zip(self.dy, self.dx):
			thereRow = rowHere + dy
			thereCol = colHere + dx
			if thereRow<0 or thereRow>=len(self.board) or thereCol<0 or thereCol>=len(self.board[0]): continue
			if self.discoverMap[thereRow][thereCol] is False:
				self.dfs(thereRow, thereCol, exploredCells)
		exploredCells.append((rowHere, colHere))
	def bfs(self, startRow, startCol, exploredCells):
		q = deque()
		q.append((startRow, startCol))
		self.discoverMap[startRow][startCol] = True
		exploredCells.append((startRow, startCol))
		while len(q)!=0:
			rowHere, colHere = q.popleft()
			cost = self.discoverMap[rowHere][colHere]
			# if here is boundary, 
			if rowHere==0 or rowHere==len(self.board)-1 or colHere==0 or colHere==len(self.board[0])-1:
				self.isBoundaryCollide = True
			for dy, dx in zip(self.dy, self.dx):
				thereRow = rowHere + dy
				thereCol = colHere + dx
				if thereRow<0 or thereRow>=len(self.board) or thereCol<0 or thereCol>=len(self.board[0]): continue
				if self.discoverMap[thereRow][thereCol] is False:
					q.append((thereRow, thereCol))
					self.discoverMap[thereRow][thereCol] = True
					exploredCells.append((thereRow, thereCol))

sol = Solution()
board = ["XXXX","XOOX","XXOX","XOXX"]
sol.solve(board)
