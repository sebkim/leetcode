class Solution(object):
	def __init__(self):
		self.wordDict = None
		self.cached = None
		self.s = None
	def wordBreak(self, s, wordDict):
		"""
		:type s: str
		:type wordDict: Set[str]
		:rtype: bool
		"""
		self.s = s
		self.wordDict = wordDict
		self.cached = [ [None] for i in range(len(s))]
		if len(wordDict)==0: return False
		return self.rec(0)
	def rec(self, startInd):
		if startInd == len(self.s): return True
		cac = self.cached[startInd]
		if cac[0] is not None: return cac[0]
		for candSize in xrange(1, len(self.s)-startInd+1):
			isThisInDict = self.s[startInd:startInd+candSize] in self.wordDict
			ret = self.rec(startInd+candSize) & isThisInDict
			if ret==True:
				break
		cac[0] = ret
		return ret

sol = Solution()
# s = 'leetcode'
# wd = set(['leet', 'code'])
# s = 'aaaaaaa'
# wd = set(['aaaa', 'aaa'])
s="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab"
wd=["a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"]
print sol.wordBreak(s, wd)
