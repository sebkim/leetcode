# Definition for a undirected graph node
# class UndirectedGraphNode:
#     def __init__(self, x):
#         self.label = x
#         self.neighbors = []
from collections import deque
class Solution:
	# @param node, a undirected graph node
	# @return a undirected graph node
	def cloneGraph(self, node):
		if node is None: return None
		q = deque()
		discovered = {}
		newNode = UndirectedGraphNode(node.label)
		q.append((node, newNode))
		discovered[node] = newNode
		while len(q)!=0:
			here, hereNewNode = q.popleft()
			for there in here.neighbors:
				if discovered.get(there) is None:
					thereNewNode = UndirectedGraphNode(there.label)
					discovered[there] = thereNewNode
					hereNewNode.neighbors.append(thereNewNode)
					q.append((there, thereNewNode))
				else:
					hereNewNode.neighbors.append(discovered[there])
		return newNode
		