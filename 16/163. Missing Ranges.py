class Solution(object):
	def findMissingRanges(self, nums, lower, upper):
		"""
		:type nums: List[int]
		:type lower: int
		:type upper: int
		:rtype: List[str]
		"""
		if nums==[]:
			if lower!=upper:
				return ["{}->{}".format(lower, upper)]
			else:
				return ["{}".format(lower)]
		res = []
		if nums[0] != lower:
			lo = lower
			hi = nums[0]-1
			if lo!=hi:
				hereStr = "{}->{}".format(lo, hi)
			else:
				hereStr = "{}".format(lo)
			res.append(hereStr)
		for ind in xrange(len(nums)-1):
			first = nums[ind]
			second = nums[ind+1]
			lo = first+1
			hi = second-1
			if first == second or first+1 == second:
				continue
			if lo!=hi:
				hereStr = "{}->{}".format(lo, hi)
			else:
				hereStr = "{}".format(lo)
			res.append(hereStr)
		if nums[-1] != upper:
			lo = nums[-1]+1
			hi = upper
			if lo!=hi:
				hereStr = "{}->{}".format(lo, hi)
			else:
				hereStr = "{}".format(lo)
			res.append(hereStr)
		return res