# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
	def getIntersectionNode(self, headA, headB):
		"""
		:type head1, head1: ListNode
		:rtype: ListNode
		"""
		aLen = Solution.getLen(headA)
		bLen = Solution.getLen(headB)
		aCur = headA; bCur = headB;

		if aLen>bLen:
			for i in xrange(aLen-bLen): aCur = aCur.next
		elif aLen<bLen:
			for i in xrange(bLen-aLen): bCur = bCur.next
		while aCur:
			if aCur is bCur: return aCur
			aCur = aCur.next; bCur = bCur.next
		return None
	@staticmethod
	def getLen(head):
		cur = head
		ret = 0
		while cur:
			cur = cur.next
			ret+=1
		return ret