import operator
from collections import defaultdict
class Solution(object):
	def __init__(self):
		self.lennums = None
		self.nums = None
		self.dupdic = None
	def permuteUnique(self, nums):
		"""
		:type nums: List[int]
		:rtype: List[List[int]]
		"""
		if nums==[]:
			return []
		self.lennums = len(nums)
		self.nums = nums
		self.dupdic = defaultdict(list)
		for iInd, i in enumerate(nums):
			self.dupdic[i].append(iInd)
		res = []
		self.backtrack(0, [], res)
		return res

	def backtrack(self, k, partial, res):
		if k==self.lennums:
			onesol = operator.itemgetter(*partial)(self.nums)
			if type(onesol) == type(1):
				onesol = (onesol, )
			res.append(onesol)
		# construct candidates
		cand = []
		partialset = set(partial)
		in_perm = [i in partialset for i in xrange(self.lennums)]
		for i in xrange(self.lennums):
			if in_perm[i] is False and self.check_validty_cand(cand, i):
				cand.append(i)
		###
		for c in cand:
			partial.append(c)
			self.backtrack(k+1, partial, res)
			partial.pop()
	def check_validty_cand(self, cand, ind):
		dup_index = self.dupdic[self.nums[ind]]
		for each_dup_ind in dup_index:
			if each_dup_ind in cand:
				return False
		return True

sol = Solution()
nums = [1,1,0,0,1,-1,-1,1]
# nums = [1]
print sol.permuteUnique(nums)
print len(sol.permuteUnique(nums))
