class Solution(object):
	def trap(self, height):
		"""
		:type height: List[int]
		:rtype: int
		"""
		if len(height)==0:
			return 0
		# left highest
		left = []
		left.append(height[0])
		for i in range(1, len(height)):
			left.append(max(height[i], left[i-1]))
		# right highest
		right = [0 for i in range(len(height))]
		right[-1] = height[-1]
		for i in range(len(height)-2, -1, -1):
			right[i] = max(height[i], right[i+1])
		hereSum = 0
		for i in range(len(height)):
			minWall = min(left[i], right[i])
			hereSum += minWall - height[i]
		return hereSum

sol = Solution()
height = [0,1,0,2,1,0,1,3,2,1,2,1]
print sol.trap(height)
        