import copy
import sys
from collections import Counter

class Solution(object):
	def combinationSum2(self, candidates, target):
		"""
		:type candidates: List[int]
		:type target: int
		:rtype: List[List[int]]
		"""
		a=[]
		candidates = Counter(candidates)
		solution=[]
		self.backtrack(a, 0, (candidates, target), solution)
		return solution

	def backtrack(self, a, k, inputParam, solution):
		"""
		a: list[int], solution vector
		k: int, depth or index
		inputParam: int
		ret: print all subsets of range(1, input+1)
		"""
		c = [] # candidates for next position
		if self.is_a_solution(a, k, inputParam):
			self.process_solution(a, k, inputParam, solution)
		else:
			k+=1
			self.construct_candidates(a, k, inputParam, c)
			for i in range(len(c)):
				a.append(c[i])
				inputParam[0][c[i]]-=1
				if inputParam[0][c[i]]==0:
					inputParam[0].pop(c[i])
				self.backtrack(a, k, inputParam, solution)
				# if inputParam[0][c[i]]==0:
				if inputParam[0].get(c[i]) is None:
					inputParam[0][c[i]]=0
				inputParam[0][c[i]]+=1
				a.pop()

	def is_a_solution(self, a, k, inputParam):
		return sum(a)==inputParam[1]

	def process_solution(self, a, k, inputParam, solution):
		solution.append(copy.copy(a))

	def construct_candidates(self, a, k, inputParam, c):
		pastSum = sum(a)
		for i in inputParam[0].keys():
			topValInA = a[-1] if len(a)>=1 else 0
			if pastSum + i <= inputParam[1] and i >= topValInA:
				c.append(i)
        
sol = Solution()
x=sol.combinationSum2([10, 1, 2, 7, 6, 1, 5], 8)
print x
