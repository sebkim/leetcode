import operator
class Solution(object):
	def permuteUnique(self, nums):
		"""
		:type nums: List[int]
		:rtype: List[List[int]]
		"""
		if nums==[]:
			return []
		a=[]
		res = set()
		nums.sort()
		dupDic = {}
		for iInd, i in enumerate(nums):
			if i not in dupDic:
				dupDic[i] = []
			else:
				pass
			dupDic[i].append(iInd)
		self.backtrack(a, 0, nums, res, dupDic)
		res = list(res)
		res = [list(i) for i in res]
		return res

	def backtrack(self, a, k, inputParam, res, dupDic):
		"""
		a: list[int], solution vector
		k: int, depth or index
		inputParam: int
		ret: print all subsets of range(1, input+1)
		"""
		c = [] # candidates for next position
		if self.is_a_solution(a, k, inputParam):
			self.process_solution(a, k, inputParam, res)
		else:
			k+=1
			self.construct_candidates(a, k, inputParam, c, dupDic)
			for i in range(len(c)):
				a.append(c[i])
				self.backtrack(a, k, inputParam, res, dupDic)
				a.pop()

	def is_a_solution(self, a, k, inputParam):
		return k==len(inputParam)

	def process_solution(self, a, k, inputParam, res):
		oneSol = operator.itemgetter(*a)(inputParam)
		if type(oneSol) == type(1):
			oneSol = (oneSol, )
		res.add(oneSol)

	def construct_candidates(self, a, k, inputParam, c, dupDic):
		in_perm = [False for i in range(len(inputParam))]
		for i in range(len(a)):
			in_perm[a[i]] = True
		for i in range(len(inputParam)):
			if in_perm[i]==False and self.checkValidityCand(c, dupDic, inputParam[i]):
				c.append(i)
	def checkValidityCand(self, c, dupDic, actualNum):
		dupIndex = dupDic[actualNum]
		for eachDupInd in dupIndex:
			if eachDupInd in c:
				return False
		return True

sol = Solution()
nums = [1,1,0,0,1,-1,-1,1]
print sol.permuteUnique(nums)
print len(sol.permuteUnique(nums))
