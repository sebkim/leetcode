class Solution(object):
	def firstMissingPositive(self, nums):
		"""
		:type nums: List[int]
		:rtype: int
		"""
		if len(nums)==0:
			return 1
		# leave only +vals
		plusStartInd = self.partition(nums)
		newNums = nums[plusStartInd:]
		for i in range(len(newNums)):
			if abs(newNums[i]) -1 < len(newNums) and newNums[abs(newNums[i])-1] > 0:
				newNums[abs(newNums[i]) -1] = - newNums[abs(newNums[i])-1]
		for i in range(len(newNums)):
			if newNums[i]>0:
				return i+1
		return len(newNums)+1
	# partition nums into leftside(zero or -vals) and rightside(+vals)
	def partition(self, nums):
		left_wall = -1
		for i in range(len(nums)):
			if nums[i] <= 0:
				left_wall+=1
				nums[left_wall], nums[i] = nums[i], nums[left_wall]
		return left_wall+1

sol = Solution()
# nums = [-2,3,4,-1,-2,-5, 0, 1]
nums = [1,2,4,]
print sol.firstMissingPositive(nums)
        