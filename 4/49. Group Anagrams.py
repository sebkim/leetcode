from collections import Counter
class Solution(object):
	def groupAnagrams(self, strs):
		"""
		:type strs: List[str]
		:rtype: List[List[str]]
		"""
		allCounter = {}
		for eachStr in strs:
			c = Counter(eachStr)
			hasable = frozenset(c.items())
			if hasable not in allCounter:
				allCounter[hasable] = list()
			else:
				pass
			allCounter[hasable].append(eachStr)
		res = []
		for eachGroup in allCounter.values():
			res.append(eachGroup)
		return res

sol = Solution()
strs = ["eat", "tea", "tan", "ate", "nat", "bat"]
# strs = ["cab","pug","pei","nay","ron","rae","ems","ida","mes"]
print sol.groupAnagrams(strs)
