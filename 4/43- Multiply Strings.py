class Solution(object):
	def multiply(self, num1, num2):
		"""
		:type num1: str
		:type num2: str
		:rtype: str
		"""
		ans = 0
		for iInd, i in enumerate(num1):
			for jInd, j in enumerate(num2):
				ans+=int(i) * pow(10, len(num1) - iInd -1) * int(j) * pow(10, len(num2) - jInd -1)
		return str(ans)

sol = Solution()
print sol.multiply("9369162965141127216164882458728854782080715827760307787224298083754", "7204554941577564438132312312312312312")
