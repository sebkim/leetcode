import operator
class Solution(object):
	def permute(self, nums):
		"""
		:type nums: List[int]
		:rtype: List[List[int]]
		"""
		if nums==[]:
			return []
		a=[]
		res = []
		self.backtrack(a, 0, nums, res)
		return res

	def backtrack(self, a, k, inputParam, res):
		"""
		a: list[int], solution vector
		k: int, depth or index
		inputParam: int
		ret: print all subsets of range(1, input+1)
		"""
		c = [] # candidates for next position
		if self.is_a_solution(a, k, inputParam):
			self.process_solution(a, k, inputParam, res)
		else:
			k+=1
			self.construct_candidates(a, k, inputParam, c)
			for i in range(len(c)):
				a.append(c[i])
				self.backtrack(a, k, inputParam, res)
				a.pop()

	def is_a_solution(self, a, k, inputParam):
		return k==len(inputParam)

	def process_solution(self, a, k, inputParam, res):
		oneSol = operator.itemgetter(*a)(inputParam)
		if type(oneSol) == type(1):
			oneSol = [oneSol]
		res.append(list(oneSol))

	def construct_candidates(self, a, k, inputParam, c):
		a = set(a)
		in_perm = [i in a for i in xrange(len(inputParam))]
		[c.append(i_ind) for i_ind, i in enumerate(in_perm) if i==False]

sol = Solution()
nums = [1,1,2]
print sol.permute(nums)
