# Definition for a binary tree node.
class TreeNode(object):
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None

class Solution(object):
	def flatten(self, root):
		"""
		:type root: TreeNode
		:rtype: void Do not return anything, modify root in-place instead.
		"""
		if root is None: pass
		else: root = self.flattenRec(root)
	def flattenRec(self, root):
		if root is None: return None
		flattenedRtree = self.flattenRec(root.right)
		root.right = self.flattenRec(root.left)
		root.left = None
		cur = root
		while cur:
			prev = cur
			cur = cur.right
		prev.right = flattenedRtree
		return root

sol = Solution()
a=TreeNode(1)
a.right = TreeNode(2)
a.left = TreeNode(100)
sol.flatten(a)

