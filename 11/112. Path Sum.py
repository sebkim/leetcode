# Definition for a binary tree node.
class TreeNode(object):
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None

class Solution(object):
	def __init__(self):
		self.target = None
		self.finished = False
	def hasPathSum(self, root, target):
		"""
		:type root: TreeNode
		:type sum: int
		:rtype: bool
		"""
		self.target = target
		if root is None: return False
		self.hasPathSumRec(root, 0)
		if self.finished==True: return True
		else: return False
	def hasPathSumRec(self, root, hereSum):
		if root.left is None and root.right is None:
			if hereSum+root.val==self.target:
				self.finished = True
				return
			else: return
		if self.finished == True: return
		if root.left:
			self.hasPathSumRec(root.left, hereSum + root.val)
		if root.right:
			self.hasPathSumRec(root.right, hereSum + root.val)
		return

sol = Solution()
a=TreeNode(1)
# a.left = TreeNode(2)
x=sol.hasPathSum(a, 2)
print x
