# Definition for a binary tree node.
class TreeNode(object):
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None

import copy
class Solution(object):
	def __init__(self):
		self.target = None
		self.finished = False
		self.res = []
	def pathSum(self, root, target):
		"""
		:type root: TreeNode
		:type sum: int
		:rtype: bool
		"""
		self.target = target
		partial = []
		if root is None: return []
		self.pathSumRec(root, 0, partial)
		return self.res
	def pathSumRec(self, root, hereSum, partial):
		if root.left is None and root.right is None:
			if hereSum+root.val==self.target:
				partial.append(root.val)
				self.res.append(copy.copy(partial))
				partial.pop()
				return
			else: return
		if root.left:
			partial.append(root.val)
			self.pathSumRec(root.left, hereSum + root.val, partial)
			partial.pop()
		if root.right:
			partial.append(root.val)
			self.pathSumRec(root.right, hereSum + root.val, partial)
			partial.pop()
		return

sol = Solution()
a=TreeNode(1)
a.left = TreeNode(2)
x=sol.pathSum(a, 3)
print x
