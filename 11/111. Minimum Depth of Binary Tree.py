# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
	def minDepth(self, root):
		"""
		:type root: TreeNode
		:rtype: int
		"""
		return self.rec(root)
	def rec(self, root):
		if root is None:
			return 0
		ls = self.rec(root.left)
		rs = self.rec(root.right)
		if ls==0 or rs==0: return max(ls, rs)+1
		return min(ls, rs)+1
