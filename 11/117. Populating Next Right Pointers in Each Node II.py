# Definition for binary tree with next pointer.
# class TreeLinkNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
#         self.next = None

class Solution:
	# @param root, a tree link node
	# @return nothing
	def connect(self, root):
		while root:
			thisNode = root
			while thisNode:
				if thisNode.left and thisNode.right:
					thisNode.left.next = thisNode.right
					thisNode.right.next = self.getNextLevelFirst(thisNode.next)
				elif thisNode.left or thisNode.right:
					self.getNextLevelFirst(thisNode).next = self.getNextLevelFirst(thisNode.next)
				thisNode = thisNode.next
			root = self.getNextLevelFirst(root)
	def getNextLevelFirst(self, root):
		if root is None: return None
		elif root.left: return root.left
		elif root.right: return root.right
		else: return self.getNextLevelFirst(root.next)
