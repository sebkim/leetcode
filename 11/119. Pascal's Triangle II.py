
class Solution(object):
	def getRow(self, rowIndex):
		"""
		:type rowIndex: int
		:rtype: List[int]
		"""
		ret = [None for i in xrange(rowIndex+1)]
		for i in xrange(rowIndex//2+1):
			ret[i] = self.ncr(rowIndex, i)
			ret[rowIndex-i] = ret[i]
		return ret
	def ncr(self, n, r):
		if r==0: return 1
		if n==r: return 1
		if r>n/2.0: r = n-r
		nom = reduce(lambda x,y:x*y, range(n, n-r,-1))
		return nom // (reduce(lambda x,y:x*y, range(r, 1, -1), 1))

sol = Solution()
print sol.getRow(4)