class Solution(object):
	def isBalanced(self, root):
		"""
		:type root: TreeNode
		:rtype: bool
		"""		
		return self.isBalancedHelper(root)[0]
	def isBalancedHelper(self, node):
		if node is None:
			return (True, 0)
		lBalanced, ls = self.isBalancedHelper(node.left)
		rBalanced, rs= self.isBalancedHelper(node.right)
		height = max(ls, rs)+1
		if lBalanced and rBalanced and abs(ls - rs) <= 1:
			return True, height
		return False, height
