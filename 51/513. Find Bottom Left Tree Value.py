# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def __init__(self):
        self.maxlevel = -1
        self.curval = None
    def findBottomLeftValue(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        self.preorder(root, 0)
        return self.curval
    def preorder(self, root, level):
        if root:
            if self.maxlevel < level:
                self.curval = root.val
                self.maxlevel = level
            self.preorder(root.left, level+1)
            self.preorder(root.right, level+1)
