class Solution(object):
    def findMinMoves(self, machines):
        """
        :type machines: List[int]
        :rtype: int
        """
        h_sum = sum(machines)
        if h_sum % len(machines) != 0:
            return -1
        avg = h_sum / len(machines)
        to_left = 0
        res = 0
        for each in machines:
            to_right = each - avg - to_left
            res = max(res, to_left, to_right, to_left + to_right)
            to_left = -to_right
        return res
