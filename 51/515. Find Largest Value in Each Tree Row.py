# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

from collections import defaultdict
class Solution(object):
    def __init__(self):
        self.res = defaultdict(list)
    def largestValues(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        self.inorder(root)
        return [max(v) for k, v in self.res.iteritems() ]
    def inorder(self, root, level=0):
        if root:
            self.inorder(root.left, level+1)
            self.res[level].append(root.val)
            self.inorder(root.right, level+1)
