from collections import defaultdict
class llNode(object):
	def __init__(self, key=None, freq=1, value=None):
		self.key = key
		self.next = None
		self.prev = None
		self.freq = freq
		self.value = value
class LinkedList(object):
	def __init__(self):
		self.root = None
		self.lastNode = None
		self.size = 0
	# return newNode
	def insertAfter(self, afterNode, nNode):
		self.size +=1
		if self.root is None:
			self.root = nNode
			self.root.next = self.root
			self.root.prev = self.root
			self.lastNode = self.root
			return self.root
		else:
			newNode = nNode
			newNode.next = afterNode.next
			newNode.prev = afterNode
			afterNode.next = newNode
			newNode.next.prev = newNode
			if afterNode is self.lastNode:
				self.lastNode = newNode
			return newNode
	# it returns deleted node.
	def erase(self, thisNode):
		self.size-=1
		thisNode.prev.next = thisNode.next
		thisNode.next.prev = thisNode.prev
		if thisNode is self.root:
			if self.size==0: self.root=None
			else: self.root = self.root.next
		if thisNode is self.lastNode:
			if self.size==0: self.lastNode = None
			else: self.lastNode = self.lastNode.prev
		return thisNode
	def __str__(self):
		cur = self.root
		hStr = ""
		while cur.next and cur.next is not self.root:
			hStr += repr(cur.key) + "({})".format(cur.freq) + "[{}]".format(cur.value)
			hStr += "-->"
			cur = cur.next
		hStr += repr(cur.key) + "({})".format(cur.freq) + "[{}]".format(cur.value)
		return hStr

class LFUCache(object):
	def __init__(self, capacity):
		"""
		:type capacity: int
		"""
		self.cap = capacity
		self.dic = {}
		self.curSize = 0
		self.freqLL = defaultdict(LinkedList)

	def get(self, key):
		"""
		:type key: int
		:rtype: int
		"""
		willUpdateNode = self.dic.get(key)
		if willUpdateNode is None: return -1
		thisFreq = willUpdateNode.freq
		self.freqLL[thisFreq].erase(willUpdateNode)
		if self.freqLL[thisFreq].root is None: self.freqLL.pop(thisFreq)
		willUpdateNode.freq+=1
		hereLL = self.freqLL[thisFreq+1]
		hereLL.insertAfter(hereLL.lastNode, willUpdateNode)
		return willUpdateNode.value
	def set(self, key, value):
		"""
		:type key: int
		:type value: int
		:rtype: void
		"""
		if self.dic.get(key) is None:
			if self.curSize < self.cap:
				self.curSize+=1
			else:
				if self.freqLL.keys() == []: return
				minFreq = min(self.freqLL.keys())
				hereLL = self.freqLL[minFreq]
				nextEvictNode = hereLL.root
				hereLL.erase(nextEvictNode)
				self.dic.pop(nextEvictNode.key)
				if hereLL.root is None: self.freqLL.pop(minFreq)
			hereLL = self.freqLL[1]
			newNode = hereLL.insertAfter(hereLL.lastNode, llNode(key, 1, value))
			self.dic[key] = newNode
		else:
			willUpdateNode = self.dic.get(key)
			thisFreq = willUpdateNode.freq
			self.freqLL[thisFreq].erase(willUpdateNode)
			if self.freqLL[thisFreq].root is None: self.freqLL.pop(thisFreq)
			willUpdateNode.freq = thisFreq+1
			willUpdateNode.value = value
			hereLL = self.freqLL[thisFreq+1]
			hereLL.insertAfter(hereLL.lastNode, willUpdateNode)
			self.dic[key] = willUpdateNode

# Your LFUCache object will be instantiated and called as such:
capacity = 10
# cache = LFUCache(capacity)
# cache.set(10, 13);
# cache.set(3,17);
# cache.set(6,11);
# cache.set(10,15);
# cache.set(9, 10);
# cache.get(1);    # returns 1
# cache.set(3, 3);    # evicts key 2
# cache.get(2);       # returns -1 (not found)
# cache.get(3);       # returns 3.
# cache.set(4, 4);    # evicts key 1.
# cache.get(1);       # returns -1 (not found)
# cache.get(3);       # returns 3
# cache.get(4);       # returns 4

a=["LFUCache","set","set","set","set","set","get","set","get","get","set","get","set","set","set","get","set","get","get","get","get","set","set","get","get","get","set","set","get","set","get","set","get","get","get","set","set","set","get","set","get","get","set","set","get","set","set","set","set","get","set","set","get","set","set","get","set","set","set","set","set","get","set","set","get","set","get","get","get","set","get","get","set","set","set","set","get","set","set","set","set","get","get","get","set","set","set","get","set","set","set","get","set","set","set","get","get","get","set","set","set","set","get","set","set","set","set","set","set","set"]
b=[[10],[10,13],[3,17],[6,11],[10,5],[9,10],[13],[2,19],[2],[3],[5,25],[8],[9,22],[5,5],[1,30],[11],[9,12],[7],[5],[8],[9],[4,30],[9,3],[9],[10],[10],[6,14],[3,1],[3],[10,11],[8],[2,14],[1],[5],[4],[11,4],[12,24],[5,18],[13],[7,23],[8],[12],[3,27],[2,12],[5],[2,9],[13,4],[8,18],[1,7],[6],[9,29],[8,21],[5],[6,30],[1,12],[10],[4,15],[7,22],[11,26],[8,17],[9,29],[5],[3,4],[11,30],[12],[4,29],[3],[9],[6],[3,4],[1],[10],[3,29],[10,28],[1,20],[11,13],[3],[3,12],[3,8],[10,9],[3,26],[8],[7],[5],[13,17],[2,27],[11,15],[12],[9,19],[2,15],[3,16],[1],[12,17],[9,1],[6,19],[4],[5],[5],[8,1],[11,7],[5,2],[9,28],[1],[2,2],[7,4],[4,22],[7,24],[9,26],[13,28],[11,26]]
cache = LFUCache(b[0][0])
upto = 50
for x,y in zip(a[1:upto], b[1:upto]):
	if x == "set":
		hereStr = "cache.{}({},{})".format(x, y[0], y[1])
	else:
		hereStr= "cache.{}({})".format(x, y[0])
	x=eval(hereStr)
	print hereStr, x
print
print cache.freqLL
for i in cache.freqLL:
	print i, cache.freqLL[i]
print cache.dic
