from itertools import groupby
import sys
import copy
class Solution(object):
	def __init__(self):
		self.cached = None
	def minTransfers(self, transactions):
		"""
		:type transactions: List[List[int]]
		:rtype: int
		"""
		if transactions==[]: return []
		infoDic = {}
		for eachTrans in transactions:
			if infoDic.get(eachTrans[0]) is None: infoDic[eachTrans[0]] = 0
			if infoDic.get(eachTrans[1]) is None: infoDic[eachTrans[1]] = 0
			infoDic[eachTrans[0]] -= eachTrans[2]
			infoDic[eachTrans[1]] += eachTrans[2]
			# print infoDic, eachTrans
		# print infoDic
		deltas = [ i for i in infoDic.values() if i!=0]
		# precounting +val == -val2
		deltas.sort()
		count=0
		leftInd=0; rightInd = len(deltas)-1;
		while leftInd < rightInd:
			if deltas[leftInd] == -deltas[rightInd]:
				count+=1
				deltas.pop(leftInd)
				deltas.pop(rightInd-1)
				rightInd-=2
			elif abs(deltas[leftInd]) > abs(deltas[rightInd]):
				leftInd+=1
			else:
				rightInd-=1
		# print deltas, count
		self.cached = [ [{}] for j in xrange(len(deltas))]
		# count+self.rec(deltas, 0)
		# print self.cached
		return count + self.rec(deltas, 0)
	def rec(self, deltas, start):
		# print start
		while start<len(deltas) and deltas[start]==0:
			start+=1
		if start==len(deltas): return 0
		cac = self.cached[start]
		if cac[0].get(tuple(deltas)) is not None:
			# print 'oh', tuple(deltas), start
			return cac[0].get(tuple(deltas))[0]
		ret = float('inf')
		for i in xrange(start+1, len(deltas)):
			if deltas[i] * deltas[start] < 0:
				deltas[i] += deltas[start]
				nextRet = 1+self.rec(deltas, start+1)
				if ret > nextRet:
					ret = nextRet
					optimalI = i
				deltas[i] -= deltas[start]
		cac[0][tuple(deltas)] = (ret, optimalI)
		return ret

transactions = [[0,1,10], [2,0,5]]
transactions = [[0,1,10], [1,0,1], [1,2,5], [2,0,5]]
transactions = [[10,11,6],[12,13,7],[14,15,2],[14,16,2],[14,17,2],[14,18,2]]
transactions = [[0,3,2],[1,4,3],[2,3,2],[2,4,2]]
transactions = [[0,3,9],[1,4,2],[2,5,5],[3,4,6],[4,5,2]]
# transactions = [[8,23,20],[3,24,78],[4,20,37],[0,29,66],[2,29,2],[0,20,23],[0,22,65],[5,24,34],[0,27,6],[6,21,16],[1,26,2],[4,21,73],[8,27,64],[6,27,39],[5,25,15],[5,23,28],[8,25,53],[6,27,98],[0,25,92],[5,28,91],[8,21,75],[1,25,39],[1,22,55],[1,25,14],[4,26,70],[6,29,30],[6,26,11],[1,28,68],[1,26,13],[7,21,4],[3,29,77],[0,26,93],[7,20,39],[5,22,91],[9,27,80],[1,23,71],[6,29,27],[8,26,95],[8,29,24],[7,25,70],[1,29,17],[9,29,98],[6,22,26],[1,24,74],[0,25,33],[0,24,68],[8,25,91],[8,23,36],[1,29,25],[8,27,82],[4,24,14]]

sol = Solution()
print len(transactions)
print sol.minTransfers(transactions)
