class Solution(object):
	def __init__(self):
		self.dic = {'2':['a','b','c'],
                '3':['d','e','f'],
                '4':['g','h','i'],
                '5':['j','k','l'],
                '6':['m','n','o'],
                '7':['p','q','r','s'],
                '8':['t','u','v'],
                '9':['w','x','y','z']
                }
	def letterCombinations(self, digits):
		"""
		:type digits: str
		:rtype: List[str]
		"""
		if not digits:
			return []
		res = []
		line = []
		self.recursive(digits, 0, res, line)
		return res
	def recursive(self, digits, cur, res, line):
		if len(line) == len(digits):
			res.append(''.join([x for x in line]))
			return
		for l in self.dic[digits[cur]]:
			line.append(l)
			self.recursive(digits, cur+1, res, line)
			line.pop()

sol = Solution()
x = sol.letterCombinations("23")
print x
