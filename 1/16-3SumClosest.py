class Solution(object):
	def threeSumClosest(self, nums, target):
		"""
		:type nums: List[int]
		:type target: int
		:rtype: int
		"""
		sortedNums = sorted(nums)
		closestDiff = (float('inf'), [])
		for i in range(len(nums)-2):
			firstP = i
			secondP = firstP + 1
			thirdP = len(nums)-1
			while secondP < thirdP:
				f = sortedNums[firstP]
				s = sortedNums[secondP]
				t = sortedNums[thirdP]
				hereSum = f+s+t
				if abs(hereSum - target)<closestDiff[0]:
					closestDiff = (abs(hereSum-target), [f, s, t])
					cand=hereSum
				if hereSum == target:
					return hereSum
				elif hereSum - target > 0:
					thirdP-=1
				elif hereSum - target < 0:
					secondP+=1
		return cand
        
sol = Solution()
x = sol.threeSumClosest([0,0,0], 1)
print x
