class Solution(object):
	def longestCommonPrefix(self, strs):
		"""
		:type strs: List[str]
		:rtype: str
		"""
		if strs==[]:
			return ""
		nStrs = len(strs)
		nStrList = [len(each) for each in strs]
		minNStr = min(nStrList)
		minStr = [i for i in strs if len(i)==minNStr][0]
		sucIndex=0
		for i in range(minNStr+1):
			if i==minNStr:
				return minStr[:sucIndex]
			if not all(each[i] == strs[0][i] for each in strs):
				return minStr[:sucIndex]
			else:
				sucIndex+=1

sol = Solution()
print sol.longestCommonPrefix(['flower', 'flow','fleet'])
