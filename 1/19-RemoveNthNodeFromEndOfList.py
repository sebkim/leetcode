# Definition for singly-linked list.
class ListNode(object):
	def __init__(self, x):
		self.val = x
		self.next = None
class LinkedList(object):
	def __init__(self, head=None):
		self.head=head
	def insert(self, data):
		new_node = ListNode(data)
		new_node.next = self.head
		self.head = new_node
	def seeAll(self):
		cur=self.head
		print '-'*10
		while cur:	
			print cur.val
			cur = cur.next
		print '-'*10

class Solution(object):
	def removeNthFromEnd(self, head, n):
		"""
		:type head: ListNode
		:type n: int
		:rtype: ListNode
		"""
		cur=head
		size=0
		while cur:
			size+=1
			cur = cur.next
		cur=head
		if n==size:
			head = cur.next
		else:
			for i in range(size-n-1):
				cur=cur.next
			cur.next = cur.next.next
		return head

sol = Solution()
linkedList = LinkedList(ListNode(2))
linkedList.insert(5)
linkedList.insert(8)

x = sol.removeNthFromEnd(linkedList.head, 2)
linkedList.seeAll()
