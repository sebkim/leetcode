class Solution(object):
	def maxArea(self, height):
		"""
		:type height: List[int]
		:rtype: int
		"""
		left, right = 0, len(height)-1
		hereMaxArea = self.__getArea(left, right, height)
		while True:
			if right-left == 1:
				break
			if height[left] < height[right]:
				left+=1
				hereArea = self.__getArea(left, right, height)
				if hereArea > hereMaxArea:
					hereMaxArea = hereArea
			else:
				right-=1
				hereArea = self.__getArea(left, right, height)
				if hereArea > hereMaxArea:
					hereMaxArea = hereArea
		return hereMaxArea
	def __getArea(self, left, right, height):
		return (right - left) * min(height[left], height[right])

sol = Solution()
print (sol.maxArea([1,2,4,3]))
