class Ref(object):
	def __init__(self, value): self.value = value
class Solution(object):
	def __init__(self):
		self.cached = None
		self.pattern = None
		self.match = None
	def isMatch(self, s, p):
		"""
		:type s: str
		:type p: str
		:rtype: bool
		"""
		# for passing by reference (further list container inside)
		self.cached = [[ [None] for i in range(len(s)+1)] for j in range(len(p)+1)]
		self.pattern = p
		self.match = s
		return self.regularExp(0, 0)

	def regularExp(self, patternPos, matchPos):
		if patternPos == len(self.pattern):
			if matchPos == len(self.match):
				return True
			else:
				return False
		cac = self.cached[patternPos][matchPos]
		if cac[0] is not None: return cac[0]
		ret = False
		nextStar = False
		if patternPos < len(self.pattern)-1:
			if self.pattern[patternPos+1] == '*':
				nextStar= True
		if not nextStar and patternPos < len(self.pattern) and matchPos < len(self.match) and \
		(self.pattern[patternPos] == self.match[matchPos] or self.pattern[patternPos] == '.'):
			ret = self.regularExp(patternPos+1, matchPos+1)
			cac[0] = ret
			return ret
		if nextStar:
			precedingElem = self.pattern[patternPos]
			if self.regularExp(patternPos+2, matchPos) or \
			(
				matchPos<len(self.match) and \
				(precedingElem =='.' or self.match[matchPos] == precedingElem) and \
				self.regularExp(patternPos, matchPos+1)
			):
				ret = True
		cac[0] = ret
		return ret

sol = Solution()
print sol.isMatch('aab', 'c*a*b')
