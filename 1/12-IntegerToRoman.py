class Solution(object):
	def __init__(self):
		self.convTab = {
		1000: "M",
		900: "CM",
		500: "D",
		400: "CD",
		100: "C",
		90: "XC",
		50: "L",
		40: "XL",
		10: "X",
		9: "IX",
		5: "V",
		4: "IV",
		1: "I"
	}
	def intToRoman(self, num):
		"""
		:type num: int
		:rtype: str
		"""
		MSP = self.get_floor_key(num)
		if MSP == num:
			return self.convTab[MSP]
		return self.convTab[MSP] + self.intToRoman(num - MSP)
		
	def get_floor_key(self, num):
		# if num in self.convTab:
		# 	return num
		return max(i for i in self.convTab if i <= num)


sol = Solution()
print sol.intToRoman(18)
