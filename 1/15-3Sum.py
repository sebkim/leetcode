class Solution(object):
	def threeSum(self, nums):
		"""
		:type nums: List[int]
		:rtype: List[List[int]]
		"""
		sortedNums = sorted(nums)
		cand = {}
		for i in range(len(nums)-2):
			firstP = i
			secondP = firstP + 1
			thirdP = len(nums)-1
			while secondP < thirdP:
				f = sortedNums[firstP]
				s = sortedNums[secondP]
				t = sortedNums[thirdP]
				if f + s + t == 0:
					hereSet = [f, s, t]
					hereSet.sort()
					hereSet = tuple(hereSet)
					if hereSet not in cand:
						cand[hereSet] = True
					thirdP -= 1
				elif f + s + t > 0:
					thirdP-=1
				elif f + s + t < 0:
					secondP+=1
		return cand.keys()

sol = Solution()
x=sol.threeSum([-1, 0, 1, 2, -1, -4])
print x
