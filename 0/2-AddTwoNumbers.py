# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        first=0
        second=0
        firstCur=l1
        i=0
        while firstCur:
           first+=firstCur.val*pow(10,i)
           i+=1
           firstCur=firstCur.next
        i=0
        secondCur=l2
        while secondCur:
            second+=secondCur.val*pow(10,i)
            i+=1
            secondCur=secondCur.next
        final=first+second
        # creating ret linked list
        eachDigitList=[int(i) for i in str(final)]
        outList=ListNode(-1)
        curOut=outList
        for rawI,i in enumerate(reversed(eachDigitList)):
            curOut.val=i
            if rawI<len(eachDigitList)-1:
                curOut.next=ListNode(-1)
                curOut=curOut.next
        return outList
