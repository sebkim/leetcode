class Solution(object):
    # dynamic programming O(n^2)
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        table=[[True if i==j else None for i in range(len(s)) ] for j in range(len(s))]
        leftIndMax=0
        rightIndMax=0
        maxLen=1
        # check two char subs
        for rawI in range(len(s)-1):
            if s[rawI]==s[rawI+1]:
                table[rawI][rawI+1]=True
                if maxLen<2:
                    leftIndMax=rawI
                    rightIndMax=rawI+1
                    maxLen=2
        # check char whose length is more than 2
        for subStrLen in range(3,len(s)+1):
            for rawI in range(len(s)):
                rawJ = rawI + subStrLen - 1
                if rawJ<len(s) and s[rawI]==s[rawJ] and table[rawI+1][rawJ-1]==True:
                    table[rawI][rawJ]=True
                    if maxLen<subStrLen:
                        leftIndMax=rawI
                        rightIndMax=rawJ
                        maxLen=subStrLen
        return s[leftIndMax:rightIndMax+1]

s="lcnvoknqgejxbfhijmxglisfzjwbtvhodwummdqeggzfczmetrdnoetmcydwddmtubcqmdjwnpzdqcdhplxtezctvgnpobnnscrmeqkwgiedhzsvskrxwfyklynkplbgefjbyhlgmkkfpwngdkvwmbdskvagkcfsidrdgwgmnqjtdbtltzwxaokrvbxqqqhljszmefsyewwggylpugmdmemvcnlugipqdjnriythsanfdxpvbatsnatmlusspqizgknabhnqayeuzflkuysqyhfxojhfponsndytvjpbzlbfzjhmwoxcbwvhnvnzwmkhjxvuszgtqhctbqsxnasnhrusodeqmzrlcsrafghbqjpyklaaqximcjmpsxpzbyxqvpexytrhwhmrkuybtvqhwxdqhsnbecpfiudaqpzsvfaywvkhargputojdxonvlprzwvrjlmvqmrlftzbytqdusgeupuofhgonqoyffhmartpcbgybshllnjaapaixdbbljvjomdrrgfeqhwffcknmcqbhvulwiwmsxntropqzefwboozphjectnudtvzzlcmeruszqxvjgikcpfclnrayokxsqxpicfkvaerljmxchwcmxhtbwitsexfqowsflgzzeynuzhtzdaixhjtnielbablmckqzcccalpuyahwowqpcskjencokprybrpmpdnswslpunohafvminfolekdleusuaeiatdqsoatputmymqvxjqpikumgmxaxidlrlfmrhpkzmnxjtvdnopcgsiedvtfkltvplfcfflmwyqffktsmpezbxlnjegdlrcubwqvhxdammpkwkycrqtegepyxtohspeasrdtinjhbesilsvffnzznltsspjwuogdyzvanalohmzrywdwqqcukjceothydlgtocukc"
sol=Solution()
print(sol.longestPalindrome(s))
