class Solution(object):

    def findMedianSortedArrays(self, nums1, nums2):
        class lORg:
            n, l,g = range(3) # less is 1, grater is 2.
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: float
        """
        # assume that nums1 has the smaller (or equal) number of elements to nums2.
        if len(nums1) > len(nums2):
            nums1, nums2 = nums2, nums1
        if len(nums1)<=2 or len(nums2)<=2:
            return self.__getMedianByMerging(nums1,nums2)
        else:
            first=self.__getMedian(nums1)
            second=self.__getMedian(nums2)
            # when first median is less, right part is sliced, and left part of nums2 is sliced.
            if first < second:
                firstInd=self.__getMedianInd(nums1,lORg.l)
                subNums1=nums1[firstInd:]
                removedLengthFromFirst=len(nums1)-len(subNums1)
                subNums2=nums2[:len(nums2)-removedLengthFromFirst]
            elif first == second:
                return first
            # when first median is greater, left part is sliced, and right part of nums2 is sliced.
            else:
                firstInd=self.__getMedianInd(nums1,lORg.g)
                subNums1=nums1[:firstInd+1]
                removedLengthFromFirst=len(nums1)-len(subNums1)
                subNums2=nums2[removedLengthFromFirst:]
            # print(subNums1)
            # print(subNums2)
            # print
            return self.findMedianSortedArrays(subNums1,subNums2)
    def __getMedian(self, nums):
        numOfNums=len(nums)
        if numOfNums%2==0:
            halfIndLeft = int(numOfNums/2)-1
            return (nums[halfIndLeft]+nums[halfIndLeft+1])/2.0
        else:
            halfInd=int(numOfNums/2)
            return nums[halfInd]
    def __getMedianInd(self, nums, lORg):
        numOfNums=len(nums)
        if numOfNums%2==0:
            halfIndLeft = int(numOfNums/2)-1
            # it has to cover both elements, thus
            # when less, left ind return.
            # when greater, right ind return.
            if lORg==1:
                return halfIndLeft
            elif lORg==2:
                return halfIndLeft+1
        else:
            halfInd=int(numOfNums/2)
            return halfInd
    def __getMedianByMerging(self,nums1, nums2):
        from bisect import insort
        for eachSmall in nums1:
            insort(nums2,eachSmall)
        return self.__getMedian(nums2)

nums1=[1,2,6,7,15,20]
nums2=[3,4,5,8,17,31,35]
sol=Solution()
print( sol.findMedianSortedArrays(nums1, nums2) )
