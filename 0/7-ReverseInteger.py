class Solution(object):
    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        minusBool=False
        if x<0:
            minusBool=True
            x*=-1
        strX=str(x)
        retStr=''
        for i in reversed(strX):
            retStr+=i
        retNum=int(retStr)
        if minusBool:
            if retNum>pow(2,31):
                retNum=0
        else:
            if retNum>pow(2,31)-1:
                retNum=0
        if minusBool:
            return -1*retNum
        else:
            return retNum

x=5123
sol=Solution()
print (sol.reverse(x))
