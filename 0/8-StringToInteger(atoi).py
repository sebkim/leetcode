class Solution(object):
    def myAtoi(self, stri):
        """
        :type str: str
        :rtype: int
        """
        stri=stri.strip()
        errorPointer=None
        for rawI,eachChar in enumerate(stri):
            if eachChar!='+' and eachChar!='-' and  not (eachChar.isdigit()):
                errorPointer=rawI
                break
        if errorPointer!=None:
            stri=stri[:errorPointer]
        try:
            numX=int(stri)
            if numX>0:
                if numX>pow(2,31)-1:
                    numX=pow(2,31)-1
            else:
                if numX<-pow(2,31):
                    numX=-1*(pow(2,31))
        except (TypeError, ValueError):
            numX=0
        return numX

stri=' + 413'
sol=Solution()
print (sol.myAtoi(stri))
