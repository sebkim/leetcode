class Solution(object):
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        checkCharDict={}
        curStr=''
        maxStr=''
        leftp=-1 # points to next to first repeating char
        for rawI, eachChar in enumerate(s):
            if eachChar in checkCharDict:
                # even if the char is already in dict, it can be appended to curStr if that char is placed in the left to first repeating char
                if leftp > checkCharDict[eachChar]:
                    checkCharDict[eachChar]=rawI
                    curStr+=eachChar
                    if len(curStr)>len(maxStr):
                        maxStr=curStr
                # when it happens to find the repeating char, init the state.
                else:
                    leftp=checkCharDict[eachChar]+1
                    checkCharDict[eachChar]=rawI
                    curStr=s[leftp:rawI+1]
            else:
                checkCharDict[eachChar]=rawI
                curStr+=eachChar
                if len(curStr)>len(maxStr):
                    maxStr=curStr
            # print checkCharDict, eachChar, curStr, maxStr
        return len(maxStr)

# s='kadvcgdkzx'
s = "tmmzuxt"
# s = "abcabcbb"
sol=Solution()
print( sol.lengthOfLongestSubstring(s) )
