class Solution(object):
    def __init__(self):
        self.cached = None
        self.s = None
    # dynamic programming O(n^2)
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        maxLeftInd = -1
        maxRightInd = -1
        maxLen = 0
        self.s = s
        self.cached = [[[None] for i in xrange(len(s))] for j in xrange(len(s))]
        for i in xrange(len(s)):
            for j in xrange(i, len(s)):
                if self.helper(i, j):
                    hLen = j-i+1
                    if hLen>maxLen:
                        maxLen = hLen
                        maxLeftInd=i
                        maxRightInd=j
        return s[maxLeftInd:maxRightInd+1]
    def helper(self, left, right):
        if left==right:
            return True
        if left==right-1:
            if self.s[left] == self.s[right]:
                return True
            else:
                return False
        ret = self.cached[left][right]
        if ret[0] is not None: return ret[0]
        if self.helper(left+1, right-1) and self.s[left] == self.s[right]:
            ret[0] = True
        else:
            ret[0] = False
        return ret[0]

s="lcnvoknqgejxbfhijmxglisfzjwbtvhodwummdqeggzfczmetrdnoetmcydwddmtubcqmdjwnpzdqcdhplxtezctvgnpobnnscrmeqkwgiedhzsvskrxwfyklynkplbgefjbyhlgmkkfpwngdkvwmbdskvagkcfsidrdgwgmnqjtdbtltzwxaokrvbxqqqhljszmefsyewwggylpugmdmemvcnlugipqdjnriythsanfdxpvbatsnatmlusspqizgknabhnqayeuzflkuysqyhfxojhfponsndytvjpbzlbfzjhmwoxcbwvhnvnzwmkhjxvuszgtqhctbqsxnasnhrusodeqmzrlcsrafghbqjpyklaaqximcjmpsxpzbyxqvpexytrhwhmrkuybtvqhwxdqhsnbecpfiudaqpzsvfaywvkhargputojdxonvlprzwvrjlmvqmrlftzbytqdusgeupuofhgonqoyffhmartpcbgybshllnjaapaixdbbljvjomdrrgfeqhwffcknmcqbhvulwiwmsxntropqzefwboozphjectnudtvzzlcmeruszqxvjgikcpfclnrayokxsqxpicfkvaerljmxchwcmxhtbwitsexfqowsflgzzeynuzhtzdaixhjtnielbablmckqzcccalpuyahwowqpcskjencokprybrpmpdnswslpunohafvminfolekdleusuaeiatdqsoatputmymqvxjqpikumgmxaxidlrlfmrhpkzmnxjtvdnopcgsiedvtfkltvplfcfflmwyqffktsmpezbxlnjegdlrcubwqvhxdammpkwkycrqtegepyxtohspeasrdtinjhbesilsvffnzznltsspjwuogdyzvanalohmzrywdwqqcukjceothydlgtocukc"
# s="abcddcba"
sol=Solution()
print(sol.longestPalindrome(s))
