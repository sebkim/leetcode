class Solution(object):
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        import sys
        print sys.version
        sortedNumsWI = sorted(zip(nums,range(len(nums))),key=lambda t:t[0])
        left = 0; right = len(nums)-1;
        while True:
            if sortedNumsWI[left][0]+sortedNumsWI[right][0] == target:
                oriLeft = sortedNumsWI[left][1]
                oriRight = sortedNumsWI[right][1]
                if oriLeft > oriRight:
                    oriLeft, oriRight = oriRight, oriLeft
                return [oriLeft, oriRight]
            elif sortedNumsWI[left][0]+sortedNumsWI[right][0] < target:
                left += 1
            elif sortedNumsWI[left][0]+sortedNumsWI[right][0] > target:
                right -= 1

nums = [1,2,3,4,5]
target = 7
sol = Solution()
print(sol.twoSum(nums, target))
