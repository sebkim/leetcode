import itertools
class Solution(object):
    def convert(self, s, numRows):
        """
        :type s: str
        :type numRows: int
        :rtype: str
        """
        if numRows==1:
            return s
        tablePointer=self.__createTablePointerGen(numRows,len(s))
        listTablePointer=list(tablePointer)
        # print listTablePointer
        colList=[j for i,j in listTablePointer]
        colNum=max(colList)+1
        table=[['' for i in range(colNum)] for j in range(numRows)]
        for eachChar,order in zip(s,listTablePointer):
            rawI,rawJ=order
            table[rawI][rawJ]=eachChar
        flattend=(something for i in table for something in i if something!='')
        from functools import reduce
        import operator
        retStr=reduce(operator.concat,flattend)
        return retStr

    # create generator for ziazag order
    def __createTablePointerGen(self,numRows,strlen):
        countedChar=0
        fullCol=True
        for c in itertools.count():
            for r in range(numRows):
                if numRows==2 or fullCol:
                    yield (r,c)
                    countedChar+=1
                    if r==numRows-1:
                        fullCol=False
                else:
                    row = numRows-(c%(numRows-1))-1
                    yield (row,c)
                    countedChar+=1
                    if row==1:
                        fullCol=True
                    break
            if strlen<=countedChar:
                raise StopIteration


s='PAYPALISHIRING'
# s="oxjpkcpdekyazevyzxudsirvddoxmptaodryfhdltcmuijsigolaxevcimbwduwrzqrhxvssxgmhpgpxvdyujvwrhzpktmdvcvcbquvpbhwsposktsecncwxbljxznsdiugaqbprknmabekwwrzltxixiuwihonrkutaviuixgibkuxinythvcgewcofsbycxrctbydyelzqhzyvxsetwkzuonbgqziosmjvnmtrzvkiuidrcjkavlwjaxrrybhsqsndghwhegpyrvrvgcwcpsnqsfjqgqjykwbqfyzjeojxlbtsfpwujjkbqtuzldxxbznjxmuddedqhwioneiwqvygqufezdbacrlbfggkmjbvfjjsqtrgormhlulkxombfyengkxuwypdkyyarpiiiwptqcdnsrqypunxfkrdlggvggxaxhifdzyuddjvvcvkwikdvbggkpbqvyqvfaakzzgecsazuxmqgwwbxchhtkarkqmrrmbsnixsczrwwdoebkfzpoikyibkbpbuedmrnllpkfnjkbnmovnfjxpkitwjiydmdrgqdthpywyjzmvnhksshkepdbylbdaexiwabfrabqlaegqnskhzumpzpplqvnwsvsuwxlyabjchruujhclbqcbhtozobviypcwmoxoriqbanvluzyxpaawwovkrsvrhxotnnjhvcivpfjjfjgwkhtgxqsrjpiqnymclvlhxveobpxgzgclnxtmqndzdmrsmduybifadlpebomaurljoewerzfwnxoacjydrfeuqvbtjnteegnvmjbgljcygraicamvfspynsrwgnamvqjiblomuqlcjnkloygvsytfqneycglxwwfyhtkdmxhvlujbspwlnqsefwchdpezmmzvfkbtjirwcaxxpukfmcltznaefgdtsdqaprvacmxemubeoljcquvpjxvqeajwfcyutuvvgscv"
s="twckwuyvbihajbmhmodminftgpdcbquupwflqfiunpuwtigfwjtgzzcfofjpydjnzqysvgmiyifrrlwpwpyvqadefmvfshsrxsltbxbziiqbvosufqpwsucyjyfbhauesgzvfdwnloojejdkzugsrksakzbrzxwudxpjaoyocpxhycrxwzrpllpwlsnkqlevjwejkfxmuwvsyopxpjmbuexfwksoywkhsqqevqtpoohpd"
sol=Solution()
# print (sol.convert(s,918))
print (sol.convert(s,4))
