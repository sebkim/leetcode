import math
class Solution(object):
	def __init__(self):
		self.isPrime = None
	def countPrimes(self, n):
		"""
		:type n: int
		:rtype: int
		"""
		if n<2: return 0
		self.isPrime = [True for i in range(n)]
		self.eratosthenes(n)
		# return len(filter(lambda x:x==True, self.isPrime))
		return sum(self.isPrime)
	def eratosthenes(self, n):
		sqrtN = int(math.sqrt(n))
		self.isPrime[0] = self.isPrime[1] = False
		for i in xrange(sqrtN+1):
			if self.isPrime[i]:
				for j in xrange(i*i, n, i):
					self.isPrime[j] = False
					