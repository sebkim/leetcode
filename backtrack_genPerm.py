from __future__ import print_function
import pdb

def backtrack(a, k, inputParam):
	"""
	a: list[int], solution vector
	k: int, depth or index
	inputParam: int
	ret: print all subsets of range(1, input+1)
	"""
	c = [] # candidates for next position
	if is_a_solution(a, k, inputParam):
		process_solution(a, k, inputParam)
	else:
		k+=1
		construct_candidates(a, k, inputParam, c)
		for i in range(len(c)):
			a.append(c[i])
			backtrack(a, k, inputParam)
			a.pop()

def is_a_solution(a, k, inputParam):
	return k==inputParam

def process_solution(a, k, inputParam):
	for i in range(k):
		print(" {}".format(a[i]), end="")
	print ()

def construct_candidates(a, k, inputParam, c):
	in_perm = [False for i in range(inputParam)]
	for i in range(len(a)):
		in_perm[a[i]] = True
	for i in range(inputParam):
		if in_perm[i]==False:
			c.append(i)

def generate_perm(n):
	a=[]
	# pdb.set_trace()
	backtrack(a, 0, n)

generate_perm(3)
