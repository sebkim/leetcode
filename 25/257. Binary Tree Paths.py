import copy

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def binaryTreePaths(self, root):
        """
        :type root: TreeNode
        :rtype: List[str]
        """
        if root is None: return []
        result = []
        self.backtrack(root, 0, [], result)
        return result
    def backtrack(self, root, k, partial, res):
        partial.append(root.val)
        if root.left is None and root.right is None:
            tmp = copy.copy(partial)
            res.append("->".join([str(i) for i in tmp]))
        if root.left:
            self.backtrack(root.left, k+1, partial, res)
        if root.right:
            self.backtrack(root.right, k+1, partial, res)
        partial.pop()
