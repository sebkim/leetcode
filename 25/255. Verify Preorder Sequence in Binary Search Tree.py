class Solution(object):
    def __init__(self):
        self.preorder = None
    def verifyPreorder(self, preorder):
        """
        :type preorder: List[int]
        :rtype: bool
        """
        if len(preorder)<=1: return True
        self.preorder = preorder
        return self.verify(0, len(preorder)-1)
    def verify(self, start, end):
        if start>=end: return True
        pivot = self.preorder[start]
        bigger = None
        for i in xrange(start+1, end+1):
            if bigger is None and self.preorder[i] > pivot:
                bigger = i
            if bigger is not None and self.preorder[i] < pivot:
                return False
        if bigger is None:
            return self.verify(start+1, end)
        if bigger is not None:
            return self.verify(start+1, bigger-1) and self.verify(bigger, end)
