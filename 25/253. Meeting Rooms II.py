# Definition for an interval.
# class Interval(object):
#     def __init__(self, s=0, e=0):
#         self.start = s
#         self.end = e
import heapq
class Solution(object):
	def minMeetingRooms(self, intervals):
		"""
		:type intervals: List[Interval]
		:rtype: int
		"""
		if len(intervals)==0: return 0
		if len(intervals)==1: return 1
		pq = []
		sortedIntervals = sorted(intervals, key=lambda x:x.start)
		# push first meeting
		heapq.heappush(pq, (sortedIntervals[0].end, sortedIntervals[0].start))
		for eachInterval in sortedIntervals[1:]:
			# pop earliestEndMeeting
			earliestEndMeeting = heapq.heappop(pq)
			# if new room is required
			if eachInterval.start < earliestEndMeeting[0]:
				heapq.heappush(pq, (eachInterval.end, eachInterval.start))
			# if new room is not required, modify that room info
			else:
				earliestEndMeeting = (eachInterval.end, earliestEndMeeting[1])
			heapq.heappush(pq, earliestEndMeeting)
		return len(pq)
