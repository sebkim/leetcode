# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def countUnivalSubtrees(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        return self.rec_count(root)[1]
    # return (IsItUnisub, NumOfSubtrees)
    def rec_count(self, root):
        if root is None: return (False, 0)
        if root.left is None and root.right is None: return (True, 1)
        left_bool, left_count = self.rec_count(root.left) if root.left else (False, 0)
        right_bool, right_count = self.rec_count(root.right) if root.right else (False, 0)
        res = left_count + right_count
        here_bool = True
        if root.left and (not left_bool or (left_bool and root.val != root.left.val)):
            here_bool = False
        if root.right and (not right_bool or (right_bool and root.val != root.right.val)):
            here_bool = False
        return (here_bool, res + (1 if here_bool else 0))
