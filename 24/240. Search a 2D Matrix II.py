class Solution(object):
	def searchMatrix(self, matrix, target):
		"""
		:type matrix: List[List[int]]
		:type target: int
		:rtype: bool
		"""
		if matrix == []:
			return False
		m = len(matrix)
		n = len(matrix[0])
		rowPointer = 0
		colPointer = n-1
		while 0<=rowPointer<m and 0<=colPointer<n:
			val = matrix[rowPointer][colPointer]
			if val > target:
				colPointer-=1
			elif val < target:
				rowPointer+=1
			else:
				return True
		return False