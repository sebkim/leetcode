import copy
from collections import deque
class Solution(object):
	def __init__(self):
		pass
	def findStrobogrammatic(self, n):
		"""
		:type n: int
		:rtype: List[str]
		"""
		res = []
		self.rec(deque(), n, n, res)
		res = [''.join(i) for i in res]
		res = [int(i) for i in res]
		res = [str(i) for i in res if len(str(i))==n]
		return res
	def rec(self, partial, k, n, res):
		if k == 0:
			res.append(copy.copy(partial))
			return
		elif k==1:
			mids = ['0', '1', '8']
			for mid in mids:
				tmpPartial = copy.copy(partial)
				tmpPartial = list(tmpPartial)
				tmpPartial.insert(n//2, mid)
				res.append(copy.copy(tmpPartial))
			return
		cands = ['00','11', '69', '88', '96']
		for c in cands:
			partial.appendleft(c[0])
			partial.append(c[1])
			self.rec(partial, k-2, n, res)
			partial.popleft()
			partial.pop()
n = 5
sol = Solution()
res = sol.findStrobogrammatic(n)
print len(res), res
