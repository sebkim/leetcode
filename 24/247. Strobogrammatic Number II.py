import copy
class Solution(object):
	def __init__(self):
		pass
	def findStrobogrammatic(self, n):
		"""
		:type n: int
		:rtype: List[str]
		"""
		res = []
		self.rec([], 0, n, res)
		res = [''.join(i) for i in res]
		res = [int(i) for i in res]
		res = [str(i) for i in res if len(str(i))==n]
		return res
	def rec(self, partial, k, n, res):
		if n == k:
			if self.isStrobogrammatic(partial):
				res.append(copy.copy(partial))
			return
		cands = ['0', '1', '6', '8', '9']
		for c in cands:
			partial.append(c)
			self.rec(partial, k+1, n, res)
			partial.pop()
	def isStrobogrammatic(self, num):
		"""
		:type num: str
		:rtype: bool
		"""
		dic = {'0': '0', '1':'1', '6':'9', '8':'8', '9':'6'}
		converted = ''
		for i in xrange(len(num)):
			if dic.get(num[i]) is None: return False
			converted += dic[num[i]]
		if converted == ''.join(list(reversed(num))):
			return True
		return False
n = 5
sol = Solution()
res = sol.findStrobogrammatic(n)
print len(res), res
