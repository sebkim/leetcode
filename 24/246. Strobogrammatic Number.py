class Solution(object):
    def isStrobogrammatic(self, num):
        """
        :type num: str
        :rtype: bool
        """
        dic = {'0': '0', '1':'1', '6':'9', '8':'8', '9':'6'}
        converted = ''
        for i in xrange(len(num)):
            if dic.get(num[i]) is None: return False
            converted += dic[num[i]]
        if converted == ''.join(list(reversed(num))):
        	return True
        return False

num = '2'
sol = Solution()
print sol.isStrobogrammatic(num)
