class Solution(object):
    def diffWaysToCompute(self, input):
        """
        :type input: str
        :rtype: List[int]
        """
        # preprocess input
        nums = input.replace('+',' ').replace('-', ' ').replace('*', ' ').split()
        operators = [i for i in input if i=='+' or i=='-' or i=='*']
        pos = 1
        for each_op in operators:
            nums.insert(pos, each_op)
            pos += 2
        return self.DiffRec(nums)
    def DiffRec(self, nums):
        if len(nums)==1 and nums[0].isdigit():
            return [int(nums[0])]
        ret = []
        for each_ind, each in enumerate(nums):
            if not each.isdigit():
                left = nums[:each_ind]
                right = nums[each_ind+1:]
                for each_left in self.DiffRec(left):
                    for each_right in self.DiffRec(right):
                        if each == '+':
                            tmp = each_left + each_right
                        elif each == '-':
                            tmp = each_left - each_right
                        else:
                            tmp = each_left * each_right
                        ret.append(tmp)
        return ret

INPUT = '12+23*123'
SOL = Solution()
print SOL.diffWaysToCompute(INPUT)
