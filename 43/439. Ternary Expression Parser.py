class Solution(object):
    def parseTernary(self, expression):
        """
        :type expression: str
        :rtype: str
        """
        stack = []
        qmode = False
        for e in reversed(expression):
            if qmode:
                qmode = False
                if e=='T':
                    top = stack.pop()
                    stack.pop()
                    stack.append(top)
                elif e=='F':
                    stack.pop()
            else:
                if e=='?':
                    qmode = True
                elif e==':':
                    pass
                else:
                    stack.append(e)
        return stack[-1]
