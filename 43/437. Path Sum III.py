# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def pathSum(self, root, sum):
        """
        :type root: TreeNode
        :type sum: int
        :rtype: int
        """
        if root is None: return 0
        return self.actual(root, sum, 0) + self.pathSum(root.left, sum) + self.pathSum(root.right, sum)
    def actual(self, root, sum, cum):
        if root is None: return 0
        res = 0
        cum += root.val
        if sum == cum:
            res += 1
        if root.left:
            res += self.actual(root.left, sum, cum)
        if root.right:
            res += self.actual(root.right, sum, cum)
        return res
