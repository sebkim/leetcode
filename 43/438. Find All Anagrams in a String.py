from collections import Counter
class Solution(object):
	def findAnagrams(self, s, p):
		"""
		:type s: str
		:type p: str
		:rtype: List[int]
		"""
		if len(s)==0 or len(s)<len(p): return []
		res = []
		myDic = Counter()
		pCounter = Counter(p)
		# initial p chars Counter
		for i in xrange(len(p)):
			myDic[s[i]] +=1
		if myDic == pCounter: res.append(0)
		for i in xrange(1, len(s)-len(p)+1):
			myDic[s[i+len(p)-1]]+=1
			myDic[s[i-1]]-=1
			if myDic[s[i-1]] ==0: myDic.pop(s[i-1])
			if myDic == pCounter: res.append(i)
		return res

s= "cbaebabacd"
p = 'abc'
sol = Solution()
print sol.findAnagrams(s, p)