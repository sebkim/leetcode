import heapq
class Solution(object):
	def thirdMax(self, nums):
		"""
		:type nums: List[int]
		:rtype: int
		"""
		nums = list(set(nums))
		if len(nums)<3: return max(nums)
		nums = [-i for i in nums]
		heapq.heapify(nums)
		ret = None
		for i in xrange(3):
			ret = heapq.heappop(nums)
		return -ret

nums = [1,2]
sol = Solution()
print sol.thirdMax(nums)