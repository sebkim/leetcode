class Solution(object):
	def wordsTyping(self, sentence, rows, cols):
		"""
		:type sentence: List[str]
		:type rows: int
		:type cols: int
		:rtype: int
		"""
		firstWordInLine = True
		sumLen =0
		nowRow = 0
		eachWordInd = 0
		while True:
			if nowRow>=rows:
				return (eachWordInd) // len(sentence)
			if firstWordInLine:
				lenSpace = 0
				firstWordInLine = False
			else:
				lenSpace = 1
			hereLen = len(sentence[eachWordInd%len(sentence)])
			if sumLen + hereLen + lenSpace <= cols:
				sumLen += hereLen + lenSpace
			else:
				firstWordInLine = True
				sumLen = 0
				nowRow+=1
				eachWordInd-=1
			eachWordInd+=1

sentence = ["try","to","be","better"]
rows=10000
cols=9001
sol = Solution()
print sol.wordsTyping(sentence, rows, cols)
