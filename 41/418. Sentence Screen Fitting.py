class Solution(object):
	def wordsTyping(self, sentence, rows, cols):
		"""
		:type sentence: List[str]
		:type rows: int
		:type cols: int
		:rtype: int
		"""
		howManyCovered = self.preprocess(sentence, cols)
		ret = 0
		wordInd = 0
		for _ in xrange(rows):
			ret += howManyCovered[wordInd]
			wordInd += howManyCovered[wordInd]
			wordInd %= len(sentence)
		return ret//len(sentence)
	# howManyCovered specifies how many words can be put when it starts with 'i' index word on one column.
	def preprocess(self, sentence, cols):
		howManyCovered = [0] * len(sentence)
		for eachWordInd, eachWord in enumerate(sentence):
			lenSum=0
			firstWordInLine = True
			word_ptr = 0
			while True:
				if firstWordInLine:
					lenSpace = 0
					firstWordInLine = False
				else:
					lenSpace = 1
				hereLen = len(sentence[(eachWordInd + word_ptr) % len(sentence)])
				if lenSum + lenSpace + hereLen <= cols:
					word_ptr+=1
					lenSum += hereLen + lenSpace
				else:
					break
			howManyCovered[eachWordInd] = word_ptr
		return howManyCovered

sentence = ["try","to","be","better"]
# sentence = ["hello", "world"]
rows=10000
cols=9001
# rows=2
# cols = 8
sol = Solution()
print sol.wordsTyping(sentence, rows, cols)
