import copy
class Solution(object):
	def __init__(self):
		self.bits = None
		self.cands = None
		self.uptoK = None
	def minAbbreviation(self, target, dictionary):
		"""
		:type target: str
		:type dictionary: List[str]
		:rtype: str
		"""
		m = len(target)
		# only consider same length word in dictionary
		dictionary = [i for i in dictionary if len(i)==m]
		if len(dictionary)==0: return str(m)
		bits = []
		for each in dictionary:
			val = 0
			for iInd, (a,b) in enumerate(zip(target, each)):
				if a!=b:
					val += 2**(m-iInd-1)
			bits.append(val)
		self.bits = bits
		# print [bin(i) for i in bits]
		valAllOr = 0
		for i in bits:
			valAllOr |= i
		valAllOr = bin(valAllOr)[2:]
		# print valAllOr
		self.cands = [iInd for iInd, i in enumerate(list(reversed(valAllOr))) if i=='1']
		self.uptoK = len(self.cands)+1
		print self.cands, self.uptoK
		res = []
		self.dfs([], 0, res)
		print res
		resSet = set()
		for i in res:
			resSet.add(frozenset(i))
		res = [list(i) for i in resSet]
		print res
		res2 = []
		for eachRes in res:
			hereStr = []
			for cInd, c in enumerate(list(reversed(target))):
				if cInd in eachRes:
					hereStr.append(c)
				else:
					hereStr.append(1)
			hereStr = list(reversed(hereStr))
			for i in xrange(len(hereStr)-1, 0, -1):
				if type(hereStr[i]) == type(hereStr[i-1]) == type(1):
					hereStr[i-1]+= hereStr.pop(i)
			hereStr = [str(i) for i in hereStr]
			res2.append(hereStr)
			print res2
		return ''.join(min([i for i in res2], key=lambda x:len(x)))
	def dfs(self, partial, k, res):
		if k==self.uptoK: return
		if len(partial)!=0:
			valFromPartial = sum(2**i for i in partial)
			if all((i & valFromPartial) >0 for i in self.bits):
				self.uptoK = k+1
				res.append(copy.copy(partial))
				return
		hereCands = list(set(self.cands) - set(partial))
		for c in hereCands:
			partial.append(c)
			self.dfs(partial, k+1, res)
			partial.pop()

target = 'apple'
dictionary = ['apply', 'tuple']
# target="internationalize"
# dictionary=["xnternationalize"
# ,"ixternationalize"
# ,"inxernationalize"
# ,"intxrnationalize"
# ,"intexnationalize"
# ,"interxationalize"
# ,"internxtionalize"
# ,"internaxionalize"
# ,"internatxonalize"]
# ,"internatixnalize"
# ,"internatioxalize"
# ,"internationxlize"
# ,"internationaxize"
# ,"internationalxze"
# ,"internationalixe"
# ,"internationalizx"]
sol = Solution()
print sol.minAbbreviation(target, dictionary)
