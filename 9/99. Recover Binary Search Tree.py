# Definition for a binary tree node.
class TreeNode(object):
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None
		

class Solution(object):
	def __init__(self):
		self.current = None
		self.before = None
		self.errNode1 = None
		self.errNode2 = None
		self.candErrNode2 = None
		self.breakBool = False
	def recoverTree(self, root):
		"""
		:type root: TreeNode
		:rtype: void Do not return anything, modify root in-place instead.
		"""
		self.preOrder(root)
		if self.errNode2 is None:
			self.errNode2 = self.candErrNode2
		self.errNode1.val, self.errNode2.val = self.errNode2.val, self.errNode1.val
	def preOrder(self, node):
		if not self.breakBool:
			if node.left:
				self.preOrder(node.left)
			self.before = self.current
			self.current = node
			if self.before:
				if self.current.val - self.before.val <= 0:
					if self.errNode1 is None:
						self.errNode1 = self.before
						self.candErrNode2 = self.current
					else:
						self.errNode2 = self.current
						self.breakBool = True
			if node.right:
				self.preOrder(node.right)
