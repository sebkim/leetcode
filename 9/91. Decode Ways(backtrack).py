class Solution(object):
	def __init__(self):
		self.res = 0
	def numDecodings(self, s):
		"""
		:type s: str
		:rtype: int
		"""
		if len(s)==0:
			return 0
		a=[]
		self.backtrack(a, 0, s)
		return self.res
	def backtrack(self, a, k, s):
		c=[]
		if self.is_a_solution(a, k, s):
			self.process_solution(a, k, s)
		else:
			k+=1
			self.constructCandidates(a, k, c, s)
			for eachC in c:
				a.append(eachC)
				self.backtrack(a, k, s)
				a.pop()
	def is_a_solution(self, a, k, s):
		if sum(len(i) for i in a) == len(s):
			return True
		else:
			return False
	def process_solution(self, a, k, s):
		print a
		self.res+=1
	def constructCandidates(self, a, k, c, s):
		usedInd = -1
		if len(a)!=0:
			usedInd = sum(len(i) for i in a) - 1
		if usedInd >= len(s)-1:
			return
		if s[usedInd+1:usedInd+2]=='0':
			return
		c.append(s[usedInd+1:usedInd+2])
		if usedInd >= len(s)-2:
			return
		if int(s[usedInd+1:usedInd+3]) <= 26:
			c.append(s[usedInd+1:usedInd+3])
		
sol = Solution()
# s = "47575625458446174945557745813412115112968167865867877552577411"
s = "12025"
res = sol.numDecodings(s)
print res
