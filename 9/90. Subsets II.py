from collections import Counter
class Solution(object):
	def __init__(self):
		self.res = set()
	def subsetsWithDup(self, nums):
		"""
		:type nums: List[int]
		:rtype: List[List[int]]
		"""
		a=[]
		self.backtrack(a,0, nums)
		retList = [ list(Counter(dict(i)).elements()) for i in self.res]
		return retList

	def backtrack(self, a, k, nums):
		c=[]
		if self.is_a_solution(a, k, nums):
			self.process_solution(a, k, nums)
		else:
			k+=1
			self.constructCandidates(a, k, c, nums)
			for eachC in c:
				a.append(eachC)
				self.backtrack(a, k, nums)
				a.pop()
	def is_a_solution(self, a, k, nums):
		if k==len(nums):
			return True
		else:
			return False
	def process_solution(self, a, k, nums):
		oneSub = [j for i,j in zip(a, nums) if i==True]
		oneSub = Counter(oneSub).items()
		oneSub = frozenset(oneSub)
		self.res.add(oneSub)
		# print oneSub
		# print filter(lambda x:a[nums.index(x)], nums)
	def constructCandidates(self, a, k, c, nums):
		c.append(False)
		c.append(True)

sol = Solution()
nums = [1,2,2]
res = sol.subsetsWithDup(nums)
print res
