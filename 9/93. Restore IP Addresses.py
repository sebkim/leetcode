import copy
class Solution(object):
	def __init__(self):
		pass
	def restoreIpAddresses(self, s):
		"""
		:type s: str
		:rtype: List[str]
		"""
		partial = []
		res = []
		self.restoreRec(partial, 0, s, res)
		return ['.'.join(i) for i in res]
	def restoreRec(self, partial, k, s, res):
		if k==4:
			if len(s)==0: 
				res.append(copy.copy(partial))
			else: return
		cand = []
		remainSCand = []
		for i in range(min(3, len(s))):
			cand.append(s[:i+1])
			remainSCand.append(s[i+1:])
		nextRemainStage = 4-k-1
		for eachC, eachRemainS in zip(cand, remainSCand):
			if 1*nextRemainStage <= len(eachRemainS) <= 3*nextRemainStage:
				if self.validCandCheck(eachC):
					partial.append(eachC)
					self.restoreRec(partial, k+1, eachRemainS, res)
					partial.pop()
	def validCandCheck(self, hereStr):
		if len(hereStr) == 1: return True
		if hereStr[0]=='0' and len(hereStr[1:])>=1:
			return False
		val = int(hereStr)
		if 0<=val<=255: return True
		else: return False
				
sol = Solution()
s = "010010"
# s= '12311'
res = sol.restoreIpAddresses(s)
print res
