# Definition for a binary tree node.
class TreeNode(object):
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None

class Solution(object):
	def __init__(self):
		self.res = []
	def inorderTraversal(self, root):
		"""
		:type root: TreeNode
		:rtype: List[int]
		"""
		stack = []
		current = root
		while True:
			while current:
				stack.append(current)
				current = current.left
			if current is None and len(stack)!=0:
				popped = stack.pop()
				self.res.append(popped.val)
				current = popped.right
			else:
				break
		return self.res
