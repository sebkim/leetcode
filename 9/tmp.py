# Definition for a binary tree node.
class TreeNode(object):
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None
class Solution(object):
	def generateTrees(self, n):
		"""
		:type n: int
		:rtype: List[TreeNode]
		"""
		return self.rec(range(1, n+1))
	def rec(self, nums):
		if len(nums)==0: return [None]
		if len(nums)==1: return [ TreeNode(nums[0]) ]
		ns = []
		for iInd, i in enumerate(nums):
			lefts = self.rec(nums[:iInd])
			rights = self.rec(nums[iInd+1:])
			for eachLeft in lefts:
				for eachRight in rights:
					node = TreeNode(i)
					node.left = eachLeft
					node.right = eachRight
					ns.append(node)
		return ns

sol = Solution()
x=sol.generateTrees(0)
print x
