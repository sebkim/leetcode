class Solution(object):
	def numDecodings(self, s):
		"""
		:type s: str
		:rtype: int
		"""
		if len(s)==0:
			return 0
		if s[0]=='0':
			return 0
		count = [0 for i in range(len(s)+1)]
		count[0] = 1
		count[1] = 1
		for i in range(2, len(s)+1):
			count[i] = 0
			if s[i-1] > '0':
				count[i] += count[i-1]
			if 10<= int(s[i-2:i]) <= 26 :
				count[i] += count[i-2]
		# print count
		return count[-1]
		
sol = Solution()
# s = "47575625458446174945557745813412115112968167865867877552577411"
# s="126"
s="101"
res = sol.numDecodings(s)
print res
