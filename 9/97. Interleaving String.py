class Solution(object):
	def __init__(self):
		self.s1 = None
		self.s2= None
		self.s3=None
		self.cached = None
	def isInterleave(self, s1, s2, s3):
		"""
		:type s1: str
		:type s2: str
		:type s3: str
		:rtype: bool
		"""
		from collections import Counter
		if Counter(s1) + Counter(s2) != Counter(s3):
			return False
		self.s1=s1; self.s2 = s2; self.s3=s3;
		self.cached = [[[None] for i in range(len(s2)+1)] for j in range(len(s1)+1)]
		return self.isInterRec(len(s1), len(s2))
	def isInterRec(self, lenS1, lenS2):
		if lenS1<0 or lenS2<0: return False
		if lenS1==0 and lenS2==0: return True
		ret = self.cached[lenS1][lenS2]
		if ret[0] is not None: return ret[0]
		if lenS1>0 and lenS2>0 and self.s3[lenS1+lenS2-1] == self.s1[lenS1-1] == self.s2[lenS2-1]:
			ret[0] = self.isInterRec(lenS1-1, lenS2) or self.isInterRec(lenS1, lenS2-1)
			return ret[0]
		elif lenS1>0 and self.s3[lenS1+lenS2-1] == self.s1[lenS1-1]:
			ret[0] = self.isInterRec(lenS1-1, lenS2)
			return ret[0]
		elif lenS2>0 and self.s3[lenS1+lenS2-1] == self.s2[lenS2-1]:
			ret[0] = self.isInterRec(lenS1, lenS2-1)
			return ret[0]
		ret[0] = False
		return ret[0]
		
sol = Solution()
s1 = 'aabcc'; s2 = 'dbbca';
s3='aadbbcbcac'
# s3= 'aadbbbaccc'

# s1="abbbbbbcabbacaacccababaabcccabcacbcaabbbacccaaaaaababbbacbb"
# s2="ccaacabbacaccacababbbbabbcacccacccccaabaababacbbacabbbbabc"
# s3="cacbabbacbbbabcbaacbbaccacaacaacccabababbbababcccbabcabbaccabcccacccaabbcbcaccccaaaaabaaaaababbbbacbbabacbbacabbbbabc"

s1="bbbbbabbbbabaababaaaabbababbaaabbabbaaabaaaaababbbababbbbbabbbbababbabaabababbbaabababababbbaaababaa"
s2="babaaaabbababbbabbbbaabaabbaabbbbaabaaabaababaaaabaaabbaaabaaaabaabaabbbbbbbbbbbabaaabbababbabbabaab"
s3="babbbabbbaaabbababbbbababaabbabaabaaabbbbabbbaaabbbaaaaabbbbaabbaaabababbaaaaaabababbababaababbababbbababbbbaaaabaabbabbaaaaabbabbaaaabbbaabaaabaababaababbaaabbbbbabbbbaabbabaabbbbabaaabbababbabbabbab"

res = sol.isInterleave(s1, s2, s3)
print res
