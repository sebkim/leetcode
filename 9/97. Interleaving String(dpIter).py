from collections import Counter
class Solution(object):
	def isInterleave(self, s1, s2, s3):
		"""
		:type s1: str
		:type s2: str
		:type s3: str
		:rtype: bool
		"""
		if Counter(s1)+Counter(s2) != Counter(s3):
			return False
		table = [[False for i in range(len(s2)+1)] for j in range(len(s1)+1)]
		for i in range(len(s1)+1):
			for j in range(len(s2)+1):
				s1Val = s1[i-1] if len(s1)>0 else None
				s2Val = s2[j-1] if len(s2)>0 else None
				if i==0 and j==0:
					table[i][j] = True
				# s1 is empty
				elif i==0 and s2[j-1] == s3[j-1]:
					table[i][j] = table[i][j-1]
				# s2 is empty
				elif j==0 and s1[i-1] == s3[i-1]:
					table[i][j] = table[i-1][j]
				# s3 char is both equal to s1 and s2
				elif s1Val == s3[i+j-1] and s2Val == s3[i+j-1]:
					table[i][j] = table[i-1][j] or table[i][j-1]
				# s3 char is equal to s1 but not to s2
				elif s1Val == s3[i+j-1]:
					table[i][j] = table[i-1][j]
				# s3 char is equal to s2 but not to s1
				elif s2Val == s3[i+j-1]:
					table[i][j] = table[i][j-1]
		return table[len(s1)][len(s2)]


sol = Solution()

# s1 = 'aabcc'; s2 = 'dbbca';
# s3='aadbbcbcac'
# # s3= 'aadbbbaccc'

# s1="abbbbbbcabbacaacccababaabcccabcacbcaabbbacccaaaaaababbbacbb"
# s2="ccaacabbacaccacababbbbabbcacccacccccaabaababacbbacabbbbabc"
# s3="cacbabbacbbbabcbaacbbaccacaacaacccabababbbababcccbabcabbaccabcccacccaabbcbcaccccaaaaabaaaaababbbbacbbabacbbacabbbbabc"

s1="bbbbbabbbbabaababaaaabbababbaaabbabbaaabaaaaababbbababbbbbabbbbababbabaabababbbaabababababbbaaababaa"
s2="babaaaabbababbbabbbbaabaabbaabbbbaabaaabaababaaaabaaabbaaabaaaabaabaabbbbbbbbbbbabaaabbababbabbabaab"
s3="babbbabbbaaabbababbbbababaabbabaabaaabbbbabbbaaabbbaaaaabbbbaabbaaabababbaaaaaabababbababaababbababbbababbbbaaaabaabbabbaaaaabbabbaaaabbbaabaaabaababaababbaaabbbbbabbbbaabbabaabbbbabaaabbababbabbabbab"

# s1='a'
# s2=''
# s3='a'

res = sol.isInterleave(s1, s2, s3)
print res
