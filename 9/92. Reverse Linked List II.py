# Definition for singly-linked list.
class ListNode(object):
	def __init__(self, x):
		self.val = x
		self.next = None

class Solution(object):
	def reverseBetween(self, head, m, n):
		"""
		:type head: ListNode
		:type m: int
		:type n: int
		:rtype: ListNode
		"""
		cur = head
		count=0
		nodeBeforeReverseStart = None
		while cur and count<m-1:
			nodeBeforeReverseStart = cur
			cur = cur.next
			count+=1
		reverseStartNode = cur
		prev = None; next = None;
		count=0
		while cur is not None and count<n-m+1:
			next = cur.next
			cur.next = prev
			prev = cur
			cur = next
			count+=1
		reverseStartNode.next = next
		if nodeBeforeReverseStart:
			nodeBeforeReverseStart.next = prev
		else:
			head = prev
		return head


