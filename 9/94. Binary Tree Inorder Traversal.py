# Definition for a binary tree node.
class TreeNode(object):
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None

class Solution(object):
	def __init__(self):
		self.res = []
	def inorderTraversal(self, root):
		"""
		:type root: TreeNode
		:rtype: List[int]
		"""
		self.rec(root)
		return self.res
	def rec(self, node):
		if node is not None:
			self.rec(node.left)
			# print node.val
			self.res.append(node.val)
			self.rec(node.right)