class Solution(object):
	def __init__(self):
		self.cached = None

	def numTrees(self, n):
		"""
		:type n: int
		:rtype: int
		"""
		self.cached = [[None] for i in range(n+1)]
		return self.numTreesRec(n)
	def numTreesRec(self, n):
		if n==0: return 1
		elif n==1: return 1
		ret = self.cached[n]
		if ret[0] is not None: return ret[0]
		ret[0] = 0
		for i in range(n):
			ret[0] += self.numTreesRec(i) * self.numTreesRec(n-i-1)
		return ret[0]

sol = Solution()
n=3
res = sol.numTrees(n)
print res
