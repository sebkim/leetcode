# Definition for a binary tree node.
class TreeNode(object):
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None

class Solution(object):
	def isValidBST(self, root):
		"""
		:type root: TreeNode
		:rtype: bool
		"""
		return self.isBSTHelper(root, -float('inf'), float('inf'))
	def isBSTHelper(self, node, minCons, maxCons):
		if node is None:
			return True
		if node.val < minCons or node.val > maxCons:
			return False
		return self.isBSTHelper(node.left, minCons, node.val-1) and self.isBSTHelper(node.right, node.val+1, maxCons)
		
			

	
	