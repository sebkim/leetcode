# Definition for singly-linked list.
class ListNode(object):
	def __init__(self, x):
		self.val = x
		self.next = None

class Solution(object):
	def partition(self, head, x):
		"""
		:type head: ListNode
		:type x: int
		:rtype: ListNode
		"""
		cur = head
		prevSmallList = None; prevBigList = None;
		headSmallList = None; headBigList = None;
		while cur:
			if cur.val < x:
				smallList = ListNode(cur.val)
				if prevSmallList:
					prevSmallList.next = smallList
				else:
					headSmallList = smallList
				prevSmallList = smallList
			else:
				bigList = ListNode(cur.val)
				if prevBigList:
					prevBigList.next = bigList
				else:
					headBigList = bigList
				prevBigList = bigList
			cur = cur.next
		if headBigList and headSmallList is None:
			return headBigList
		if prevSmallList:
			prevSmallList.next = headBigList
		return headSmallList