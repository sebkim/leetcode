class Solution(object):
	def grayCode(self, n):
		"""
		:type n: int
		:rtype: List[int]
		"""
		grayRawList = self.grayRec(n)
		return [int(i, 2) for i in grayRawList]
	def grayRec(self, n):
		if n==0: return ['0']
		if n==1: return ['0', '1']
		return ['0'+i for i in self.grayRec(n-1)] + ['1'+i for i in reversed(self.grayRec(n-1))]

sol = Solution()
n = 3
res = sol.grayCode(n)
print res
