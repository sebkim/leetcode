class Solution(object):
	def largestRectangleArea(self, heights):
		"""
		:type heights: List[int]
		:rtype: int
		"""
		stack = []
		maxArea = 0
		i = 0
		while i < len(heights):
			if len(stack)==0:
				stack.append(i)
			else:
				if heights[stack[-1]]<= heights[i]:
					stack.append(i)
				else:
					while len(stack)>=1 and heights[i] < heights[stack[-1]]:
						popped = stack.pop()
						if len(stack)==0:
							area = heights[popped] * i
						else:
							area = heights[popped] * (i - stack[-1] -1)
						if maxArea < area:
							maxArea = area
					stack.append(i)
			i+=1
		while len(stack)!=0:
			popped = stack.pop()
			if len(stack)==0:
				area = heights[popped] * i
			else:
				area = heights[popped] * (i - stack[-1] -1)
			if maxArea < area:
				maxArea = area
		# print i
		return maxArea
        
sol = Solution()
nums =  [2,1,5,6,2,3]
res = sol.largestRectangleArea(nums)
print res
