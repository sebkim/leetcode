class TreeNode(object):
    def __init__(self):
        self.left = None
        self.right = None

def GetLenNonLeaf(tn, char):
    if len(char)==1:
        return 0
    leftStr = char[:len(char)//2]
    rightStr = char[len(char)//2:]
    ret = 1
    if len(leftStr)>=1:
        leftNode = TreeNode()
        tn.left = leftNode
        ret += GetLenNonLeaf(leftNode, leftStr)
    if len(rightStr)>=1:
        rightNode = TreeNode()
        tn.right=rightNode
        ret += GetLenNonLeaf(rightNode, rightStr)
    return ret

def GetOneResult(partial):
    root = TreeNode()
    return ConstructTree(root, source, partial)

def ConstructTree(tn, char, partial):
    if len(char)==1: return char
    left_str = char[:len(char)//2]
    right_str = char[len(char)//2:]
    new_left_str = left_str; new_right_str = right_str
    if len(left_str)>=1:
        left_node = TreeNode()
        tn.left = left_node
        new_left_str = ConstructTree(left_node, left_str, partial)
    if len(right_str)>=1:
        right_node = TreeNode()
        tn.right = right_node
        new_right_str = ConstructTree(right_node, right_str, partial)
    if tn.left is not None and tn.right is not None:
        is_swap = partial.pop()
        if is_swap:
            return new_right_str + new_left_str
        else:
            return new_left_str + new_right_str

def backtrack(k, partial, res):
    if k == len_nonleaf:
        res.append(GetOneResult(list(partial)))
        return
    for eachC in [False, True]:
        partial.append(eachC)
        backtrack(k+1, partial, res)
        partial.pop()

source = "great"
len_nonleaf = GetLenNonLeaf(TreeNode(), source)
res = []
backtrack(0, [], res)
print res, len(res)
