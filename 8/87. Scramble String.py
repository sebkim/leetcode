from collections import Counter
class Solution(object):
	def isScramble(self, s1, s2):
		"""
		:type s1: str
		:type s2: str
		:rtype: bool
		"""
		if len(s1)!=len(s2): return False
		c1 = Counter(s1)
		c2 = Counter(s2)
		if c1!=c2:
			return False
		if s1==s2:
			return True
		for i in range(1, len(s1)):
			if self.isScramble(s1[:i], s2[:i]) and self.isScramble(s1[i:], s2[i:]):
				return True
			if self.isScramble(s1[:i], s2[len(s1)-i:]) and self.isScramble(s1[i:], s2[:len(s1)-i]):
				return True
		return False

sol = Solution()
s1 = "great"
s2 = "etarg"
res = sol.isScramble(s1, s2)
print res
