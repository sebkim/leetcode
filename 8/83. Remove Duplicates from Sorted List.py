# Definition for singly-linked list.
class ListNode(object):
	def __init__(self, x):
		self.val = x
		self.next = None

class Solution(object):
	def deleteDuplicates(self, head):
		"""
		:type head: ListNode
		:rtype: ListNode
		"""
		cur = head
		base = cur
		while cur:
			if base.val == cur.val:
				pass
			else:
				base.next = cur
				base = cur
			cur = cur.next
		if base:
			base.next = None
		return head