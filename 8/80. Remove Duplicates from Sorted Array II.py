class Solution(object):
	def removeDuplicates(self, nums):
		"""
		:type nums: List[int]
		:rtype: int
		"""
		if len(nums) <= 1:
			return len(nums)
		replaceInd = 2
		for i in range(2, len(nums)):
			if nums[replaceInd-2] == nums[i]:
				pass
			else:
				nums[replaceInd] = nums[i]
				replaceInd+=1
		return replaceInd

sol = Solution()
nums = [1,1,1,2,2,3]
# nums = [2]
# nums = [1,2,3]
res = sol.removeDuplicates(nums)
print res
