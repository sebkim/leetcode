class Solution(object):
	def search(self, nums, target):
		"""
		:type nums: List[int]
		:type target: int
		:rtype: int
		"""
		return self.searchRec(nums, target, 0, len(nums)-1)
	def searchRec(self, nums, target, lo, hi):
		while lo < hi-1 and nums[lo] == nums[lo+1]:
			lo+=1
		while hi > lo+1 and nums[hi] == nums[hi-1]:
			hi-=1
		if hi-lo <= 1:
			for ind in range(lo, hi+1):
				if nums[ind] == target:
					return True
			return False
		elif lo<hi:
			mid = lo + (hi - lo + 1 ) // 2
			right, left = False, False
			# decide which direction is right.
			if (nums[hi] - nums[mid]) > 0:
				right = True
			elif (nums[hi] - nums[mid]) <0:
				left = True
			###
			if nums[mid] == target:
				return True
			elif right:
				if nums[mid] <= target <= nums[hi]:
					return self.searchRec(nums, target, mid+1, hi)
				else:
					return self.searchRec(nums, target, lo, mid-1)
			elif left:
				if nums[lo] <= target <= nums[mid]:
					return self.searchRec(nums, target, lo, mid-1)
				else:
					return self.searchRec(nums, target, mid+1, hi)	

sol = Solution()
x=sol.search([6,7,1,2,3,4,5], 7)
x=sol.search([5,5,5, 1, 2, 3, 5,5,5], 3)
# x=sol.search([1], 1)
print x

