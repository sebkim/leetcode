# Definition for singly-linked list.
class ListNode(object):
	def __init__(self, x):
		self.val = x
		self.next = None

class Solution(object):
	def deleteDuplicates(self, head):
		"""
		:type head: ListNode
		:rtype: ListNode
		"""
		# base1 is surely not duplicated element
		# base2 is a candidate.
		if head is None or head.next is None:
			return head
		cur = head
		base1 = None
		base2 = cur
		cur = cur.next
		while cur:
			if base2.val != cur.val:
				base1 = base2
				base2 = cur
			else:
				while cur and cur.val==base2.val:
					cur = cur.next
				base2 = cur
				if base1:
					base1.next = base2
				else:
					head = base2
			if cur:
				cur = cur.next
		return head
