from collections import defaultdict
import heapq
import copy
class Solution(object):
    def __init_(self):
        self.distance = None
        self.maze = None
        self.hole = None
        self.parent = None
    def findShortestWay(self, maze, ball, hole):
        """
        :type maze: List[List[int]]
        :type ball: List[int]
        :type hole: List[int]
        :rtype: str
        """
        self.maze = maze
        self.distance = {}
        self.hole= tuple(hole)
        self.parent = defaultdict(list)
        self.shortest(ball)
        for k in self.parent:
            maxdist = max(self.parent[k], key=lambda x:x[1])[1]
            self.parent[k] = [i[0] for i in self.parent[k] if i[1]==maxdist]
        recon_res = []
        self.recon(tuple(hole), [], recon_res)
        overall_res = self.transform(recon_res)
        overall_res.sort()
        ret = self.distance.get(tuple(hole), -1)
        if ret==-1: return 'impossible'
        return overall_res[0]

    def shortest(self, start):
        maze = self.maze
        pq = []
        self.distance[tuple(start)] = 0
        self.parent[tuple(start)].append((None, 0))
        heapq.heappush(pq, (0, tuple(start)))
        while len(pq)!=0:
            cost, nextpos = heapq.heappop(pq)
            if self.distance.get(nextpos, float('inf')) < cost:
                continue
            leftj = rightj = nextpos[1]
            upi = downi = nextpos[0]
            while leftj-1>=0 and self.maze[nextpos[0]][leftj-1]==0:
                leftj-=1
                if (nextpos[0], leftj) == self.hole:
                    break
            while rightj+1<len(self.maze[0]) and self.maze[nextpos[0]][rightj+1]==0:
                rightj+=1
                if (nextpos[0], rightj) == self.hole:
                    break
            while upi-1>=0 and self.maze[upi-1][nextpos[1]]==0:
                upi-=1
                if (upi, nextpos[1]) == self.hole:
                    break
            while downi+1<len(self.maze) and self.maze[downi+1][nextpos[1]]==0:
                downi+=1
                if (downi, nextpos[1]) == self.hole:
                    break
            for nei, newcost in zip([(nextpos[0], leftj), (nextpos[0], rightj), (upi, nextpos[1]), (downi, nextpos[1])], \
            [cost+nextpos[1]-leftj, cost+rightj-nextpos[1], cost+nextpos[0]-upi, cost+downi-nextpos[0]]):
                if self.distance.get(nei, float('inf')) >= newcost and nei!=nextpos:
                    self.parent[nei].append((nextpos, newcost))
                    self.distance[nei] = newcost
                    heapq.heappush(pq, (newcost, nei))
    def recon(self, here, partial, res):
        if self.parent[here] == [None]:
            res.append(list(reversed(copy.copy([self.hole] + partial))))
            return
        parent_list = self.parent[here]
        for each_parent in parent_list:
            partial.append(each_parent)
            self.recon(each_parent, partial, res)
            partial.pop()
    def transform(self, recon_res):
        overall_res = []
        for each_res in recon_res:
            res = []
            for left, right in zip([None] + each_res, each_res)[1:]:
                if left[1] == right[1] and left[0]>right[0]:
                    res.append('u')
                elif left[1] == right[1] and left[0]<right[0]:
                    res.append('d')
                elif left[0] == right[0] and left[1]>right[1]:
                    res.append('l')
                elif left[0] == right[0] and left[1]<right[1]:
                    res.append('r')
            overall_res.append(res)
        overall_res = [''.join(i) for i in overall_res]
        return overall_res
MAZE = [[0,0,0,0,0],[1,1,0,0,1],[0,0,0,0,0],[0,1,0,0,1],[0,1,0,0,0]]
BALL = [4,3]
HOLE = [0,1]
from pprint import pprint
SOL = Solution()
pprint(SOL.findShortestWay(MAZE, BALL, HOLE))
