class Solution(object):
    def __init__(self):
        self.nums = None
        self.res = None
    def findSubsequences(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        if len(nums)<=1: return []
        self.nums = nums
        self.res = set()
        self.backtrack(0, [], len(nums))
        return list(self.res)
    def backtrack(self, index, partial, length):
        if index>=2:
            self.res.add(tuple(i[1] for i in partial))
        for cand_ind, cand in enumerate(self.nums):
            topnum_ind, topnum = partial[-1] if len(partial)!=0 else (-1, -float('inf'))
            if cand>=topnum and cand_ind>topnum_ind:
                partial.append((cand_ind, cand))
                self.backtrack(index+1, partial, length)
                partial.pop()

NUMS = [4,6,7,7]
NUMS = [10,10,10,10,10,10,10,10,10,10,10,10,10,10,10]
from pprint import pprint
SOL = Solution()
pprint (SOL.findSubsequences(NUMS))
