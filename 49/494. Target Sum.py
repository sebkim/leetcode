class Solution(object):
    def __init__(self):
        self.cached = None
        self.nums = None
        self.S = None
    def findTargetSumWays(self, nums, S):
        """
        :type nums: List[int]
        :type S: int
        :rtype: int
        """
        self.nums = nums
        self.S = S
        self.cached = [[[None] for _ in xrange(1001)] for _ in xrange(len(nums))]
        return self.dp(0, 0)
    def dp(self, ind, cum):
        if ind == len(self.nums):
            if cum == self.S:
                return 1
            else:
                return 0
        ret = self.cached[ind][cum]
        if ret[0] is not None:
            return ret[0]
        curval = self.nums[ind]
        minus = self.dp(ind+1, cum-curval)
        plus = self.dp(ind+1, cum+curval)
        ret[0] = minus + plus
        return ret[0]

SOL = Solution()
NUMS = [1,1,1,1,1]
S = 3
print SOL.findTargetSumWays(NUMS, S)
