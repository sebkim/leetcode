from collections import deque
class MovingAverage(object):
	def __init__(self, size):
		"""
		Initialize your data structure here.
		:type size: int
		"""
		self.size = size
		self.data = deque()
		self.curSize = 0
		self.nowSum = 0
	def next(self, val):
		"""
		:type val: int
		:rtype: float
		"""
		if self.curSize < self.size:
			pass
			self.curSize+=1
		else:
			self.nowSum-=self.data.popleft()
		self.data.append(val)
		self.nowSum+=val
		return self.nowSum/float(self.curSize)
        
# Your MovingAverage object will be instantiated and called as such:
# obj = MovingAverage(size)
# param_1 = obj.next(val)