class Solution(object):
	def lengthOfLongestSubstringTwoDistinct(self, s):
		"""
		:type s: str
		:rtype: int
		"""
		k=2
		leftp = 0; rightp = 0;
		dic = {}
		ret = 0
		while rightp<len(s):
			if s[rightp] not in dic:
				dic[s[rightp]] = 1
			else:
				dic[s[rightp]] +=1
			while len(dic.keys())>k:
				if dic[s[leftp]] == 1:
					dic.pop(s[leftp])
				else:
					dic[s[leftp]]-=1
				leftp+=1
			ret = max(ret, rightp-leftp+1)
			rightp+=1
		return ret