# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

from collections import defaultdict
class Solution(object):
    def findFrequentTreeSum(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        if root is None: return []
        self.dic = defaultdict(int)
        self.preorder(root)
        maxFreq = max(self.dic.values())
        return filter(lambda x:self.dic[x]==maxFreq, self.dic)
    def preorder(self, node):
        sval = 0
        sval += node.val
        if node.left:
            sval += self.preorder(node.left)
        if node.right:
            sval += self.preorder(node.right)
        self.dic[sval]+=1
        return sval
