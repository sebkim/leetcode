# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
from collections import Counter
from itertools import chain
class Solution(object):
    def __init__(self):
        self.c = None
    def findMode(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        self.c = Counter()
        self.dfs(root)
        max_val = max(chain([None], self.c.itervalues()))
        ret = [k for k, v in self.c.iteritems() if self.c[k] == max_val]
        return ret
    def dfs(self, root):
        if root:
            self.c[root.val]+=1
            self.dfs(root.left)
            self.dfs(root.right)
