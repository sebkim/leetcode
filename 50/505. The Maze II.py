import heapq
class Solution(object):
    def __init_(self):
        self.distance = None
        self.maze = None
        self.destination = None
    def shortestDistance(self, maze, start, destination):
        """
        :type maze: List[List[int]]
        :type start: List[int]
        :type destination: List[int]
        :rtype: int
        """
        self.maze = maze
        self.distance = {}
        self.destination = destination
        self.shortest(start)
        return self.distance.get(tuple(destination), -1)
    def shortest(self, start):
        maze = self.maze
        pq = []
        heapq.heappush(pq, (0, tuple(start)))
        while len(pq)!=0:
            cost, nextpos = heapq.heappop(pq)
            if self.distance.get(nextpos, float('inf')) < cost:
                continue
            leftj = rightj = nextpos[1]
            upi = downi = nextpos[0]
            while leftj-1>=0 and self.maze[nextpos[0]][leftj-1]==0:
                leftj-=1
            while rightj+1<len(self.maze[0]) and self.maze[nextpos[0]][rightj+1]==0:
                rightj+=1
            while upi-1>=0 and self.maze[upi-1][nextpos[1]]==0:
                upi-=1
            while downi+1<len(self.maze) and self.maze[downi+1][nextpos[1]]==0:
                downi+=1
            for nei, newcost in zip([(nextpos[0], leftj), (nextpos[0], rightj), (upi, nextpos[1]), (downi, nextpos[1])], \
            [cost+nextpos[1]-leftj, cost+rightj-nextpos[1], cost+nextpos[0]-upi, cost+downi-nextpos[0]]):
                if self.distance.get(nei, float('inf')) > newcost:
                    self.distance[nei] = newcost
                    heapq.heappush(pq, (newcost, nei))


SOL = Solution()
MAZE = [[0,0,1,0,0],[0,0,0,0,0],[0,0,0,1,0],[1,1,0,1,1],[0,0,0,0,0]]
START = [0,4]
DESTINATION = [4,4]
print SOL.shortestDistance(MAZE, START, DESTINATION)
