class Solution(object):
    def __init__(self):
        self.res = []
        self.target = None
    def addOperators(self, num, target):
        """
        :type num: str
        :type target: int
        :rtype: List[str]
        """
        self.target = target
        self.backtrack("", num, True)
        return self.res
    def backtrack(self, partial, remaining, first_try):
        if len(remaining)==0:
            if eval(partial) == self.target:
                self.res.append(partial[2:])
            return
        for op in ['+', '-', '*']:
            if first_try:
                if op != '+': continue
                f = '0'
            else:
                f = ''
            for i in xrange(len(remaining)):
                cand = remaining[:i+1]
                if cand[0] == '0' and len(cand[1:]) !=0:
                    continue
                each = f + (op + cand)
                self.backtrack(partial + each, remaining[i+1:], False)

NUM = "123456789"
TARGET = 45
SOL = Solution()
print SOL.addOperators(NUM, TARGET)
