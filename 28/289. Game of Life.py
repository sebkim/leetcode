class Solution(object):
	def gameOfLife(self, board):
		"""
		:type board: List[List[int]]
		:rtype: void Do not return anything, modify board in-place instead.
		"""
		if board==[]: return
		dyTab = [-1, -1, -1, 0, 1, 1, 1, 0]
		dxTab = [-1, 0, 1, 1, 1, 0, -1, -1]
		newBoard = [[None for i in xrange(len(board[0]))] for j in xrange(len(board))]
		for i in xrange(len(board)):
			for j in xrange(len(board[0])):
				count=0
				for dy, dx in zip(dyTab, dxTab):
					y = i+dy
					x = j+dx
					if y<0 or x<0 or y>=len(board) or x>=len(board[0]): continue
					if board[y][x] == 1: count+=1
				if board[i][j] == 1:
					if count<2: newBoard[i][j] = 0
					elif 2<=count<=3: newBoard[i][j] = 1
					elif count>3: newBoard[i][j] = 0
				elif board[i][j] == 0:
					if count==3: newBoard[i][j] = 1
					else: newBoard[i][j] = 0
		for i in xrange(len(board)):
			for j in xrange(len(board[0])):
				board[i][j] = newBoard[i][j]
		# print board

board = [[1]]
board = [[1,1],[1,0]]
sol = Solution()
sol.gameOfLife(board)
