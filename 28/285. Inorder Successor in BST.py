# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def inorderSuccessor(self, root, p):
        """
        :type root: TreeNode
        :type p: TreeNode
        :rtype: TreeNode
        """
        cand = None
        cur = root
        while cur is not None:
            if cur.val > p.val:
                cand = cur
                cur = cur.left
            else:
                cur = cur.right
        return cand.val if cand else None
