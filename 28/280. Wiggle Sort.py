import copy
from collections import deque
class Solution(object):
	# def wiggleSort(self, nums):
	# 	"""
	# 	:type nums: List[int]
	# 	:rtype: void Do not return anything, modify nums in-place instead.
	# 	"""
	# 	revNums = copy.deepcopy(nums)
	# 	nums.sort()
	# 	revNums.sort(reverse=True)
	# 	q1 = deque(nums)
	# 	q2 = deque(revNums)
	# 	whichTurn = q1
	# 	res = []
	# 	for i in xrange(len(nums)):
	# 		res.append(whichTurn.popleft())
	# 		if whichTurn == q1:
	# 			whichTurn = q2
	# 		elif whichTurn == q2:
	# 			whichTurn = q1
	# 	for i in xrange(len(nums)):
	# 		nums[i] = res[i]
	# 	# print nums
	def wiggleSort(self, nums):
		for i in xrange(len(nums)-1):
			nums[i:i+2] = sorted(nums[i:i+2], reverse=i%2)

nums = [3,5,2,1,6,4]
sol = Solution()
sol.wiggleSort(nums)
