class ZigzagIterator(object):
	def __init__(self, v1, v2):
		"""
		Initialize your data structure here.
		:type v1: List[int]
		:type v2: List[int]
		"""
		self.v1 = v1
		self.v2 = v2
		self.v1_ptr = 0
		self.v2_ptr = 0
		self.whose_turn = 0
	def next(self):
		"""
		:rtype: int
		"""
		# when v1 or v2's items are all used
		if self.v1_ptr==len(self.v1):
			self.whose_turn=1
		if self.v2_ptr==len(self.v2):
			self.whose_turn=0
		##
		if self.whose_turn==0:
			self.whose_turn=1
			self.v1_ptr+=1
			return self.v1[self.v1_ptr-1]
		elif self.whose_turn==1:
			self.whose_turn=0
			self.v2_ptr+=1
			return self.v2[self.v2_ptr-1]
	def hasNext(self):
		"""
		:rtype: bool
		"""
		if self.v1_ptr >= len(self.v1) and self.v2_ptr >= len(self.v2): return False
		return True
# Your ZigzagIterator object will be instantiated and called as such:
v1=[1,2,3,10,100,1000]
v2=[4,5,6]
i, v = ZigzagIterator(v1, v2), []
while i.hasNext(): v.append(i.next())
print v