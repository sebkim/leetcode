class ValidWordAbbr(object):
	def __init__(self, dictionary):
		"""
		initialize your data structure here.
		:type dictionary: List[str]
		"""
		self.dictionary = set(dictionary)
		self.myDic = {}
		for eachWord in self.dictionary:
			if len(eachWord)>2:
				compressed = eachWord[0]
				compressed += str(len(eachWord[1:-1]))
				compressed += eachWord[-1]
			else:
				compressed = eachWord
			if self.myDic.get(compressed) is None:
				self.myDic[compressed] = []
			self.myDic[compressed].append(eachWord)
	def isUnique(self, word):
		"""
		check if a word is unique.
		:type word: str
		:rtype: bool
		"""
		if len(word)>2:
			compressed = word[0]
			compressed += str(len(word[1:-1]))
			compressed += word[-1]
		else:
			compressed = word
		if self.myDic.get(compressed) is not None:
			if len(self.myDic.get(compressed))>1:
				return False
			elif len(self.myDic.get(compressed))==1:
				if self.myDic.get(compressed)[0] != word:
					return False
		return True

# Your ValidWordAbbr object will be instantiated and called as such:
dictionary = [ "deer", "door", "cake", "card" ]
dictionary = ["a", "a"]
vwa = ValidWordAbbr(dictionary)
print vwa.isUnique("dear")
print vwa.isUnique("anotherWord")
print vwa.isUnique('a')
