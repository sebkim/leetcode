class Solution(object):
	def moveZeroes(self, nums):
		"""
		:type nums: List[int]
		:rtype: void Do not return anything, modify nums in-place instead.
		"""
		leftWall = -1
		for i in xrange(len(nums)):
			if nums[i] != 0:
				leftWall +=1
				nums[i], nums[leftWall] = nums[leftWall], nums[i]
