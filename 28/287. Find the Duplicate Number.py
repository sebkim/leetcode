class Solution(object):
    def findDuplicate(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        slow = 0
        fast = 0
        entry = 0
        while True:
            slow = nums[slow]
            fast = nums[nums[fast]]
            if slow == fast:
                break
        while True:
            slow = nums[slow]
            entry = nums[entry]
            if slow == entry:
                return slow

NUMS = [1,2,3,4,4]
SOL = Solution()
print SOL.findDuplicate(NUMS)
