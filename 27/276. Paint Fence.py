class Solution(object):
	def __init__(self):
		self.cached = None
	def numWays(self, n, k):
		"""
		:type n: int
		:type k: int
		:rtype: int
		"""
		self.cached = [[None] for _ in xrange(n+1)]
		return self.dpRec(n, k)[0] + self.dpRec(n, k)[1]
	# it returns tuple(A, B). A is same, and B is diff.
	def dpRec(self, n, k):
		if n==0:
			return (0, 0)
		if n==1:
			return (0, k)
		cac = self.cached[n]
		if cac[0] is not None: return cac[0]
		# nowSame comes from beforeStage diff. Just put same color right before current fence.
		same = self.dpRec(n-1, k)[1]
		# nowDiff comes from both same and diff of beforeStage.
		diff = self.dpRec(n-1, k)[0] * (k-1) + self.dpRec(n-1, k)[1] * (k-1)
		cac[0] = (same, diff)
		return (same, diff)
		