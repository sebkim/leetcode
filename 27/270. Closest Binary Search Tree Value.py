# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def closestValue(self, root, target):
        """
        :type root: TreeNode
        :type target: float
        :rtype: int
        """
        if root is None: return None
        return self.closest(root, target)
    def closest(self, root, target):
        if root.left is None and root.right is None:
            return root.val
        a = None
        if target == root.val:
            return root.val
        elif target < root.val:
            if root.left:
                a = self.closest(root.left, target)
        elif target > root.val:
            if root.right:
                a = self.closest(root.right, target)
        if a is None:
            return root.val
        else:
            return min((a, root.val), key=lambda x: abs(x - target))
