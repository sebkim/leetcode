# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def closestKValues(self, root, target, k):
        """
        :type root: TreeNode
        :type target: float
        :type k: int
        :rtype: List[int]
        """
        s1 = []
        s2 = []
        self.inorder(True, root, target, s1)
        self.inorder(False, root, target, s2)
        res = []
        i = 0
        while i<k:
            i+=1
            if len(s1)==0:
                res.append(s2.pop())
            elif len(s2)==0:
                res.append(s1.pop())
            elif abs(s1[-1] - target) < abs(s2[-1] - target):
                res.append(s1.pop())
            else:
                res.append(s2.pop())
        return res

    def inorder(self, reverse, root, target, stack):
        if reverse:
            if root.right:
                self.inorder(reverse, root.right, target, stack)
            if root.val <= target: return
            stack.append(root.val)
            if root.left:
                self.inorder(reverse, root.left, target, stack)
        else:
            if root.left:
                self.inorder(reverse, root.left, target, stack)
            if root.val > target: return
            stack.append(root.val)
            if root.right:
                self.inorder(reverse, root.right, target, stack)
