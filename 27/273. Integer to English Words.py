class Solution(object):
	def __init__(self):
		self.convTab = {
			0: '',
			1: 'One',
			2: 'Two',
			3: 'Three',
			4: 'Four',
			5: 'Five',
			6: 'Six',
			7: 'Seven',
			8: 'Eight',
			9: 'Nine',
			10: 'Ten',
			11: 'Eleven',
			12: 'Twelve',
			13: 'Thirteen',
			14: 'Fourteen',
			15: 'Fifteen',
			16: 'Sixteen',
			17: 'Seventeen',
			18: 'Eighteen',
			19: 'Nineteen',
			20: 'Twenty',
			30: 'Thirty',
			40: 'Forty',
			50: 'Fifty',
			60: 'Sixty',
			70: 'Seventy',
			80: 'Eighty',
			90: 'Ninety'
		}
		for i in xrange(1, 10):
			self.convTab[i*100] = self.convTab[i] + ' Hundred'
	def numberToWords(self, num):
		"""
		:type num: int
		:rtype: str
		"""
		if num==0: return 'Zero'
		numStr = "{:,d}".format(num)
		fourParts = [0 for i in range(4)]
		for iInd, i in enumerate(numStr.split(',')[::-1]):
			fourParts[4-iInd-1] = int(i)
		ret = ''
		for eachPart, eachSuffix in zip(fourParts, ['Billion', 'Million', 'Thousand', '']):
			if eachPart ==0: continue
			ret += self.numberToWordsUPTOHundreds(eachPart) + ' ' + eachSuffix
			if eachSuffix!='': ret += ' '
		return ret.rstrip()
	def numberToWordsUPTOHundreds(self, num):
		MSP = self.getFloorKey(num)
		if MSP==num:
			return self.convTab[MSP]
		return self.convTab[MSP] + ' '+self.numberToWordsUPTOHundreds(num-MSP)
	def getFloorKey(self, num):
		return max(i for i in self.convTab if i<=num)

num=123
sol = Solution()
print sol.numberToWords(num)
