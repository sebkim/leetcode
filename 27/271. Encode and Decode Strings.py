class Codec:
	def encode(self, strs):
		"""Encodes a list of strings to a single string.

		:type strs: List[str]
		:rtype: str
		"""
		return ''.join(['$'.join([str(len(s)), s]) for s in strs])
	def decode(self, s):
		"""Decodes a single string to a list of strings.
		:type s: str
		:rtype: List[str]
		"""
		if s=='': return []
		res = []
		ind=0
		mode = 0
		oneStr = ''
		howManyRead = -1
		while ind<len(s):
			if howManyRead==0:
				mode=0
				howManyRead = -1
				continue
			if mode==0:
				if ind!=0:
					res.append(oneStr)
				howManyRead = ''
				while s[ind]!='$':
					howManyRead+=s[ind]
					ind+=1
				howManyRead = int(howManyRead)
				oneStr = ""
				mode=1
			elif mode==1:
				oneStr += s[ind]
				howManyRead-=1
				if howManyRead==0:
					mode=0
			ind+=1	
		res.append(oneStr)
		return res

# Your Codec object will be instantiated and called as such:
strs = ['abc', 'def']
strs = ["63/Rc","h","BmI3FS~J9#vmk","7uBZ?7*/","24h+X","O "]
strs = []
codec = Codec()
print codec.encode(strs)
print codec.decode(codec.encode(strs))
