class Solution(object):
	def __init__(self):
		self.cached = None
	def numSquares(self, n):
		"""
		:type n: int
		:rtype: int
		"""
		self.cached = [[None] for i in xrange(n+1)]
		return self.dpRec(n)
	def dpRec(self, n):
		if n == 0: return 0
		cac = self.cached[n]
		if cac[0] is not None: return cac[0]
		ret = float('inf')
		i = 1
		while i*i<=n:
			ret = min(ret, 1 + self.dpRec(n-i*i))
			i+=1
		cac[0] = ret
		return ret

