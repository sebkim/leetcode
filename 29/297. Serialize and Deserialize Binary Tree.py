# Definition for a binary tree node.
class TreeNode(object):
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None

class Codec:
	def __init__(self):
		self.data = []
		self.MARKER = float('inf')
	def serialize(self, root):
		"""Encodes a tree to a single string.
		:type root: TreeNode
		:rtype: str
		"""
		if root is None:
			self.data.append(self.MARKER)
			return self.data
		self.data.append(root.val)
		self.serialize(root.left)
		self.serialize(root.right)
		return self.data
	def deserialize(self, data):
		"""Decodes your encoded data to tree.

		:type data: str
		:rtype: TreeNode
		"""
		data = iter(data)
		return self.deserializeHelper(data)
	def deserializeHelper(self, data):
		val = data.next()
		if val == self.MARKER: return
		root = TreeNode(val)
		root.left = self.deserializeHelper(data)
		root.right = self.deserializeHelper(data)
		return root

# Your Codec object will be instantiated and called as such:
root = TreeNode(1)
root.left = TreeNode(2)
root.right = TreeNode(3)
# root = None
codec = Codec()
print codec.deserialize(codec.serialize(root))
