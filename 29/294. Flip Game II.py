import copy
class Solution(object):
	def __init__(self):
		self.cached = {}
	def canWin(self, s):
		"""
		:type s: str
		:rtype: bool
		"""
		s = list(s)
		return self.rec(s)
	def rec(self, s):
		if not self.isPossibleSol(s):
			return False
		if self.cached.get(tuple(s)) is not None: return self.cached.get(tuple(s))
		cands = [(i, i+1) for i in xrange(len(s)-1) if s[i:i+2] == ['+', '+']]
		# if self.isSymmetric(s):
		# 	cands = cands[:(len(cands)+1)//2]
		ret = False
		for c in cands:
			modiS = self.putTwoMinus(s, c[0])
			if self.rec(modiS): continue
			else: return True
		self.cached[tuple(s)] = ret
		return ret
	def isSymmetric(self, s):
		for i in xrange((len(s)+1)//2):
			if s[i] == s[len(s)-i-1]: pass
			else: return False
		return True
	def isPossibleSol(self, s):
			ret = False
			for i in xrange(len(s)-1):
				if s[i:i+2]==['+', '+']: return True
				else: pass
			return ret
	def putTwoMinus(self, s, where):
		s2 = copy.copy(s)
		s2[where:where+2] = ['-', '-']
		return s2

s = "++++"
# s = "+++++++++++++++++++++"
s = "++++-++++++++"
sol = Solution()
print sol.canWin(s)
