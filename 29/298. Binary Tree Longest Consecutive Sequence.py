# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

import sys
class Solution(object):
	def __init__(self):
		self.maxAns = 0
		sys.setrecursionlimit(10000)
	def longestConsecutive(self, root):
		"""
		:type root: TreeNode
		:rtype: int
		"""
		self.rec(root)
		return self.maxAns
	def rec(self, root):
		if root is None: return 0
		if root.left:
			lRes = self.longestConsecutive(root.left)
		if root.right:
			rRes = self.longestConsecutive(root.right)
		ret = 1
		if root.left:
			if root.val+1 == root.left.val:
				ret = max(ret, lRes+1)
		if root.right:
			if root.val+1 == root.right.val:
				ret = max(ret, rRes +1)
		self.maxAns = max(self.maxAns, ret)
		return ret
