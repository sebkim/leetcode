import heapq
class MedianFinder:
	def __init__(self):
		"""
		Initialize your data structure here.
		"""
		self.minHeap = []
		self.maxHeap = []
		self.maxTurn = True
	def addNum(self, num):
		"""
		Adds a num into the data structure.
		:type num: int
		:rtype: void
		"""
		# should maintain invariant that maxHeap top number should be equal or less than minHeap top number!
		# and, numbers put into maxHeap and minHeap alternatively.
		if self.maxTurn:
			self.maxTurn = False
			heapq.heappush(self.maxHeap, -num)
		else:
			self.maxTurn = True
			heapq.heappush(self.minHeap, num)
		if len(self.maxHeap)>0 and len(self.minHeap)>0 and -self.maxHeap[0] > self.minHeap[0]:
			a, b= self.maxHeap[0], self.minHeap[0]
			heapq.heappop(self.maxHeap)
			heapq.heappop(self.minHeap)
			heapq.heappush(self.maxHeap, -b)
			heapq.heappush(self.minHeap, -a)
   	def findMedian(self):
		"""
		Returns the median of current data stream
		:rtype: float
		"""
		totalSize = len(self.maxHeap) + len(self.minHeap)
		if totalSize % 2 ==0:
			return (-self.maxHeap[0] + self.minHeap[0]) / 2.0
		return -self.maxHeap[0]
# Your MedianFinder object will be instantiated and called as such:
mf = MedianFinder()
mf.addNum(1)
mf.addNum(5)
print mf.findMedian()
