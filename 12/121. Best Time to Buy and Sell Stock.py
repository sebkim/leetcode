class Solution(object):
	def maxProfit(self, prices):
		"""
		:type prices: List[int]
		:rtype: int
		"""
		minPos = 0
		minPrice = float('inf')
		ret = 0
		for eachPriceInd, eachPrice in enumerate(prices):
			if minPrice > eachPrice:
				minPrice = eachPrice
				minPos = eachPriceInd
			ret = max(ret, eachPrice - prices[minPos])
		return ret