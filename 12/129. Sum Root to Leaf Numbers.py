import copy
# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
	def sumNumbers(self, root):
		"""
		:type root: TreeNode
		:rtype: int
		"""
		if root is None: return 0
		res = []
		self.rec([], root, res)
		res = [[str(i) for i in j] for j in res]
		ret = [''.join(i) for i in res]
		ret = [int(i) for i in ret]
		return sum(ret)
	def rec(self, partial, root, res):
		if root.left is None and root.right is None:
			res.append(partial + [root.val])
			return
		partial.append(root.val)
		if root.left:
			self.rec(partial, root.left, res)
		if root.right:
			self.rec(partial, root.right, res)
		partial.pop()

