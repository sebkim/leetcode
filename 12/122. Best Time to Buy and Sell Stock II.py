class Solution(object):
	def maxProfit(self, prices):
		"""
		:type prices: List[int]
		:rtype: int
		"""
		ret = 0
		for eachPriceInd, eachPrice in enumerate(prices[:-1]):
			diff = prices[eachPriceInd+1] - prices[eachPriceInd]
			if diff>0:
				ret += diff
		return ret
		