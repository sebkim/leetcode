class Solution(object):
	def maxProfit(self, prices):
		"""
		:type prices: List[int]
		:rtype: int
		"""
		if prices == []: return 0
		minPosForLeft = 0
		maxPosForRight = len(prices)-1
		minPriceForLeft = prices[0]
		maxPriceForRight = prices[-1]
		# lefts maintains maximum profit that can be possible using elems before 'i'.
		lefts = [0 for _ in range(len(prices))]
		rights = [0 for _ in range(len(prices))]
		for eachPriceInd, eachPrice in enumerate(prices):
			if eachPriceInd==0: continue
			if minPriceForLeft > eachPrice:
				minPosForLeft = eachPriceInd
				minPriceForLeft = eachPrice
			possibleMax = eachPrice - prices[minPosForLeft]
			lefts[eachPriceInd] = max(lefts[eachPriceInd-1], possibleMax)
		for eachPriceRevInd, eachPrice in enumerate(reversed(prices)):
			eachPriceInd = len(prices) - eachPriceRevInd - 1
			if eachPriceInd==len(prices)-1: continue
			if maxPriceForRight < eachPrice:
				maxPosForRight = eachPriceInd
				maxPriceForRight = eachPrice
			possibleMax = prices[maxPosForRight] - eachPrice
			rights[eachPriceInd] = max(rights[eachPriceInd+1], possibleMax)
		sums = [i+j for i,j in zip(lefts, rights)]
		return max(sums)
