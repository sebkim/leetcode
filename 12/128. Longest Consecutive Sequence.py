class Solution(object):
	def longestConsecutive(self, nums):
		"""
		:type nums: List[int]
		:rtype: int
		"""
		bag = set(nums)
		longestInfo = {each: None for each in bag}
		while bag:
			each = bag.pop()
			if longestInfo[each]==None: longestInfo[each]=1
			upNum=each
			while True:
				upNum = upNum+1
				if upNum in bag:
					bag.remove(upNum)
					longestInfo[each]+=1
				else: break
			downNum = each
			while True:
				downNum = downNum-1
				if downNum in bag:
					bag.remove(downNum)
					longestInfo[each]+=1
				else: break
		# print longestInfo
		return max(longestInfo.values())

sol = Solution()
nums = [100, 4, 200, 1, 3, 2]
print sol.longestConsecutive(nums)
