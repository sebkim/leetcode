class Solution(object):
	def __init__(self):
		self.cached = None
	def minimumTotal(self, triangle):
		"""
		:type triangle: List[List[int]]
		:rtype: int
		"""
		self.cached = [ [ [None] for i in range(len(triangle))] for j in range(len(triangle))]
		self.triangle = triangle
		return self.rec(0, 0)
	def rec(self, row, col):
		if row == len(self.triangle):
			return 0
		cac = self.cached[row][col]
		if cac[0] is not None: return cac[0]
		ret = float('inf')
		for i in range(2):
			ret = min(ret, self.rec(row+1, col+i) + self.triangle[row][col])
		cac[0] = ret
		return ret
