# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
	def __init__(self):
		self.maxRet = -float('inf')
	def maxPathSum(self, root):
		"""
		:type root: TreeNode
		:rtype: int
		"""
		self.rec(root)
		return self.maxRet
	def rec(self, root):
		ls, rs = 0, 0
		if root.left:
			ls = self.rec(root.left)
		if root.right:
			rs = self.rec(root.right)
		ret = max(ls+root.val, rs+root.val, root.val)
		self.maxRet = max(self.maxRet, ret, ls+rs+root.val)
		return ret
		