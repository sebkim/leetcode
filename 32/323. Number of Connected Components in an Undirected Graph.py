class Solution(object):
    def __init__(self):
        self.visited = None
        self.adj_list = None
    def countComponents(self, n, edges):
        """
        :type n: int
        :type edges: List[List[int]]
        :rtype: int
        """
        self.visited = [False for _ in xrange(n)]
        self.adj_list = [list() for _ in xrange(n)]
        for edge in edges:
            self.adj_list[edge[0]].append(edge[1])
            self.adj_list[edge[1]].append(edge[0])
        n_component = 0
        for i in xrange(n):
            if self.visited[i] is False:
                n_component += 1
                self.dfs(i)
        return n_component
    def dfs(self, here):
        self.visited[here] = True
        for there in self.adj_list[here]:
            if self.visited[there] is False:
                self.dfs(there)
