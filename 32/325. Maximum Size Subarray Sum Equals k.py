class Solution(object):
	def maxSubArrayLen(self, nums, k):
		"""
		:type nums: List[int]
		:type k: int
		:rtype: int
		"""
		if len(nums)==0: return 0
		cums = []
		hereSum=0
		for i in nums:
			hereSum+=i
			cums.append(hereSum)
		# sumDic stores eachCum as key, eachCumInd as value
		# for the case first element in num is target
		sumDic = {0:-1}
		maxRet = 0
		for eachCumInd, eachCum in enumerate(cums):
			if sumDic.get(eachCum - k) is not None:
				maxRet = max(maxRet, eachCumInd - sumDic[eachCum-k])
			# keep only first duplicate because we want the index as left as possible.
			if sumDic.get(eachCum) is None:
				sumDic[eachCum] = eachCumInd
		return maxRet
