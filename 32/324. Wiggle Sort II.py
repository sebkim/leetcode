from heapq import nsmallest
import copy
class Solution(object):
	def wiggleSort(self, nums):
		"""
		:type nums: List[int]
		:rtype: void Do not return anything, modify nums in-place instead.
		"""
		if len(nums)<=1: return
		median = self.findMedian(nums)
		L, R, medians = [], [], []
		for i in xrange(len(nums)):
			if nums[i] < median:
				L.append(nums[i])
			elif nums[i] > median:
				R.append(nums[i])
			else:
				medians.append(nums[i])
		res = [None for i in xrange(len(nums))]
		print L, R, medians
		L = iter(L)
		R = iter(R)
		medians = iter(medians)
		if len(nums)%2==0:
			startIndForSmallVal = len(nums)-2
		else:
			startIndForSmallVal = len(nums)-1
		for i in xrange(startIndForSmallVal, -1, -2):
			try:
				smallVal = L.next()
				res[i] = smallVal
			except StopIteration:
				break
		for i in xrange(1, len(nums), 2):
			try:
				bigVal = R.next()
				res[i] = bigVal
			except StopIteration:
				break
		for i in xrange(len(nums)):
			if res[i] is None:
				try:
					medVal = medians.next()
					res[i] = medVal
				except StopIteration:
					break
		for i in xrange(len(nums)):
			nums[i] = res[i]
		print nums
	def findMedian(self, nums):
		return nsmallest( (len(nums)+1) //2, nums)[-1]

nums = [1, 5, 1, 1, 6, 4]
# nums = [1, 3, 2, 2, 3, 1]
# nums = [1,3,2,2,2,1,1,3,1,1,2]
sol = Solution()
print sol.wiggleSort(nums)
