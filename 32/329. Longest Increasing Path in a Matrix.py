class Solution(object):
	def __init__(self):
		self.cached = None
		self.matrix = None
		self.m = None
		self.n = None
	def longestIncreasingPath(self, matrix):
		"""
		:type matrix: List[List[int]]
		:rtype: int
		"""
		if len(matrix)==0: return 0
		self.matrix = matrix
		self.m = len(self.matrix)
		self.n = len(self.matrix[0])
		self.cached = [[[None] for i in xrange(self.n)] for j in xrange(self.m)]
		res = 0
		for i in xrange(self.m):
			for j in xrange(self.n):
				res = max(res, self.dpRec(i, j))
		return res
	def dpRec(self, row, col):
		cac = self.cached[row][col]
		if cac[0] is not None: return cac[0]
		dyL = [0, -1, 0, +1]
		dxL = [-1, 0, +1, 0]
		ret = 1
		for dy, dx in zip(dyL, dxL):
			y = dy+row
			x = dx+col
			if y<0 or x<0 or y>=self.m or x>=self.n: continue
			if self.matrix[y][x] > self.matrix[row][col]:
				nextRet = self.dpRec(y,x)
				ret = max(ret, 1 + nextRet)
		cac[0] = ret
		return ret
nums = [
  [9,9,4],
  [6,6,8],
  [2,1,1]
]
sol = Solution()
print sol.longestIncreasingPath(nums)