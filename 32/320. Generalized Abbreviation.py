import copy
class Solution(object):
	def generateAbbreviations(self, word):
		"""
		:type word: str
		:rtype: List[str]
		"""
		res = []
		self.rec([], 0, word, res)
		#preprocess res so that consecutive number do not appear
		for eachThing in res:
			for i in xrange(len(eachThing)-1, 0, -1):
				if type(eachThing[i]) == type(eachThing[i-1]) == type(1):
					eachThing[i-1]+=eachThing.pop(i)
		res = [[str(i) for i in j] for j in res]
		res = [''.join(i) for i in res]
		return res
	def rec(self, partial, k, word, res):
		if k==len(word):
			res.append(copy.copy(partial))
			return
		cands = [word[k], 1]
		for c in cands:
			partial.append(c)
			self.rec(partial, k+1, word, res)
			partial.pop()

word = 'word'
sol = Solution()
print sol.generateAbbreviations(word)
