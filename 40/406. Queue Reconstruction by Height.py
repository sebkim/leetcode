from itertools import groupby
class Solution(object):
	def reconstructQueue(self, people):
		"""
		:type people: List[List[int]]
		:rtype: List[List[int]]
		"""
		if people == []: return []
		people = sorted(sorted(people, key=lambda x:x[1]), key=lambda x:x[0], reverse=True)
		res = []
		for k, g in groupby(people, key=lambda x:x[0]):
			for v in g:
				res.insert(v[1], v)
		return res

people = [[7,0], [4,4], [7,1], [5,0], [6,1], [5,2]]
sol = Solution()
print sol.reconstructQueue(people)