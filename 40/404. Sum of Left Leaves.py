# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def sumOfLeftLeaves(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        return self.rec(root)
    def rec(self, root):
        if root is None: return 0
        hsum = 0
        if root.left:
            if root.left.left is None and root.left.right is None:
                hsum += root.left.val
            else:
                hsum += self.rec(root.left)
        if root.right:
            hsum += self.rec(root.right)
        return hsum
        
