class Solution(object):
	def findNthDigit(self, n):
		"""
		:type n: int
		:rtype: int
		"""
		length = 1
		start = 1
		count = 9
		while n>length * count:
			n -= length*count
			length+=1
			start*=10
			count*=10
		actualNum = start + (n-1) // length
		actualNum = str(actualNum)
		print length, actualNum, n
		return int(actualNum[(n-1) % length])

n = 1500
sol = Solution()
print sol.findNthDigit(n)