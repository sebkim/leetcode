import heapq
class Solution(object):
    def trapRainWater(self, heightMap):
		"""
		:type heightMap: List[List[int]]
		:rtype: int
		"""
		if len(heightMap)==0: return 0
		pq  = []
		visited = [[False for i in xrange(len(heightMap[0]))] for j in xrange(len(heightMap))]
		m = len(heightMap)
		n = len(heightMap[0])
		# put boundary cells into PQ.
		for i in xrange(m):
			visited[i][0] = True
			visited[i][n-1] = True
			heapq.heappush(pq, (heightMap[i][0], i, 0))
			heapq.heappush(pq, (heightMap[i][n-1], i, n-1))
		for j in xrange(n):
			visited[0][j] = True
			visited[m-1][j] = True
			heapq.heappush(pq, (heightMap[0][j], 0, j))
			heapq.heappush(pq, (heightMap[m-1][j], m-1, j))
		##
		res = 0
		dxL = [-1, 0, 1, 0]
		dyL = [0, -1, 0, +1]
		while len(pq)!=0:
			here = heapq.heappop(pq)
			hereY = here[1]
			hereX = here[2]
			hereHeight = here[0]
			for dy, dx in zip(dyL, dxL):
				y = hereY + dy
				x = hereX + dx
				if y<0 or x<0 or y>=m or x>=n: continue
				if not visited[y][x]:
					visited[y][x] = True
					res += max(0, hereHeight - heightMap[y][x])
					heapq.heappush(pq, (max(hereHeight, heightMap[y][x]), y, x))
		return res

heightMap = [[9,9,9,9,9],[9,2,1,2,9], [9,2,8,2,9], [9,2,3,2,9], [9,9,9,9,9]]
heightMap = [[1,4,3,1,3,2],[3,2,1,3,2,4],[2,3,3,2,3,1]]
heightMap = [[12,13,1,12],[13,4,13,12],[13,8,10,12],[12,13,12,12],[13,13,13,13]]
sol = Solution()
print sol.trapRainWater(heightMap)
