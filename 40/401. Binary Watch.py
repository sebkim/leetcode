import copy
class Solution(object):
	def __init__(self):
		self.cands = ['h1', 'h2', 'h4', 'h8', 'm1', 'm2', 'm4', 'm8', 'm16', 'm32']
		self.num = None
		self.cached = {}
	def readBinaryWatch(self, num):
		"""
		:type num: int
		:rtype: List[str]
		"""
		self.num = num
		res = []
		self.rec([], 0, res)
		res2 = set()
		for i in res:
			res2.add(frozenset(i))
		res2 = [list(i) for i in res2]
		ans = []
		for eachRes in res2:
			hourStr = 0
			minStr = 0
			for eachLed in eachRes:
				if eachLed[0] == 'h':
					hourStr+=int(eachLed[1:])
				if eachLed[0] == 'm':
					minStr += int(eachLed[1:])
			if hourStr>12 or minStr>59: continue
			ans.append(str(hourStr) + ":" + str(minStr).zfill(2))
		ans.sort()
		return ans
	def rec(self, partial, k, res):
		if k==self.num:
			res.append(copy.copy(partial))
			return
		cands = list(set(self.cands) - set(partial))
		for c in cands:
			partial.append(c)
			self.rec(partial, k+1, res)
			partial.pop()
num = 2
sol = Solution()
print sol.readBinaryWatch(num)
