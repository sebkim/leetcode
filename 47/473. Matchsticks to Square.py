class Solution(object):
    def __init__(self):
        self.nums = None
        self.target = None
    def makesquare(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        if nums == []: return False
        nums.sort(reverse=True)
        self.nums = nums
        allsum = sum(nums)
        if allsum % 4 != 0: return False
        self.target = allsum / 4
        sums = [0 for _ in xrange(4)]
        print self.target
        print self.nums
        return self.backtrack(0, sums)
    def backtrack(self, index, sums):
        if index == len(self.nums):
            if all(i==self.target for i in sums):
                return True
            else:
                return False
        for i in xrange(4):
            if self.nums[index] + sums[i] > self.target: continue
            sums[i] += self.nums[index]
            if self.backtrack(index+1, sums): return True
            sums[i] -= self.nums[index]
        return False

NUMS = [5969561,8742425,2513572,3352059,9084275,2194427,1017540,2324577,6810719,8936380,7868365,2755770,9954463,9912280,4713511]
NUMS = [5,5,5,5,16,4,4,4,4,4,3,3,3,3,4]
SOL = Solution()
print SOL.makesquare(NUMS)
