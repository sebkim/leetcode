import copy
class Solution(object):
	def __init__(self):
		self.words = None
		self.prefixDic = {}
	def wordSquares(self, words):
		"""
		:type words: List[str]
		:rtype: List[List[str]]
		"""
		self.words = words
		self.preprocess()
		res = []
		self.rec([], 0, res)
		return res
	def preprocess(self):
		for eachWord in self.words:
			for i in xrange(1, len(eachWord)+1):
				herePrefix = eachWord[:i]
				if self.prefixDic.get(herePrefix) is None:
					self.prefixDic[herePrefix] = []
				self.prefixDic[herePrefix].append(eachWord)
	def rec(self, partial, k, res):
		if k==len(self.words[0]):
			res.append(copy.copy(partial))
			return
		if k==0:
			cands = []
			for each in self.words:
				cands.append(each)
		else:
			prefixToFind = ''.join([i[k] for i in partial])
			cands = self.prefixDic.get(prefixToFind)
			if cands is None: cands = []
		for cand in cands:
			partial.append(cand)
			self.rec(partial, k+1, res)
			partial.pop()

words = ["ball","area","lead","lady"]
sol = Solution()
print sol.wordSquares(words)

