class Solution(object):
    def complexNumberMultiply(self, a, b):
        """
        :type a: str
        :type b: str
        :rtype: str
        """
        splitted = a.split('+')
        ra = splitted[0]; ra = float(ra);
        ia = splitted[1]; ia = float(ia[:-1]);
        splitted = b.split('+')
        rb = splitted[0]; rb = float(rb);
        ib = splitted[1]; ib = float(ib[:-1]);
        retR = 0; retI = 0;
        retR = ra * rb - (ia * ib)
        retI = ra * ib + rb * ia
        return "{}+{}i".format(int(retR), int(retI))
