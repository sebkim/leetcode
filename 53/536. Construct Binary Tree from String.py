# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def str2tree(self, s):
        """
        :type s: str
        :rtype: TreeNode
        """
        def t(val, left=None, right=None):
            node, node.left, node.right = TreeNode(val), left, right
            return node
        return eval('t(' + s.replace('(', ',t(') + ')') if s else None

S = '1(2,3)'
SOL = Solution()
print SOL.str2tree(S)
