# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def __init__(self):
        self.sum_val = 0
    def convertBST(self, root):
        """
        :type root: TreeNode
        :rtype: TreeNode
        """
        if root is None: return None
        return self.helper(root)
    def helper(self, root):
        if root.right:
            self.helper(root.right)
        root.val += self.sum_val
        self.sum_val = root.val
        if root.left:
            self.helper(root.left)
        return root
