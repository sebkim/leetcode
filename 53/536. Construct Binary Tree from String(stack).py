# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def str2tree(self, s):
        """
        :type s: str
        :rtype: TreeNode
        """
        stack = []
        i=0
        while i<len(s):
            if s[i] == ')':
                stack.pop()
                i += 1
            elif '0' <= s[i] <= '9' or s[i] == '-':
                j=i
                while j<len(s) and ('0' <= s[j] <= '9' or s[j] == '-'):
                    j += 1
                one_num = s[i:j]
                cur_node = TreeNode(int(one_num))
                i = j
                if len(stack)!=0:
                    parent = stack[-1]
                    if parent.left is not None:
                        parent.right = cur_node
                    else:
                        parent.left = cur_node
                stack.append(cur_node)
            else:
                i += 1
        return stack[-1] if len(stack)!=0 else None

S = '12(2(1)(2))(3(1)(2))'
SOL = Solution()
print SOL.str2tree(S)
