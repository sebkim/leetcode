from __future__ import print_function
import pdb

def backtrack(a, k, inputParam):
	"""
	a: list[int], solution vector
	k: int, depth or index
	inputParam: int
	ret: print all subsets of range(1, input+1)
	"""
	c = [] # candidates for next position
	if is_a_solution(a, k, inputParam):
		process_solution(a, k, inputParam)
	else:
		k+=1
		construct_candidates(a, k, inputParam, c)
		for i in range(len(c)):
			a.append(c[i])
			backtrack(a, k, inputParam)
			a.pop()

def is_a_solution(a, k, inputParam):
	return k==inputParam

def process_solution(a, k, inputParam):
	print("{", end="")
	for i in range(k):
		if a[i]==True:
			print(" {}".format(i), end="")
	print("}")

def construct_candidates(a, k, inputParam, c):
	c.append(True)
	c.append(False)

def generate_subsets(n):
	a=[]
	# pdb.set_trace()
	backtrack(a, 0, n)

generate_subsets(5)
