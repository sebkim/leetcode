class Solution(object):
    def __init__(self):
        self.board = None
        self.dydx=[[-1, -1], [-1, 0], [-1, 1], [0, 1], [1, 1], [1,0], [1, -1], [0, -1]]
        self.visited = None
        self.neighbor_check_map = None
    def updateBoard(self, board, click):
        """
        :type board: List[List[str]]
        :type click: List[int]
        :rtype: List[List[str]]
        """
        self.board = board
        self.visited = [[False for _ in xrange(len(board[0]))] for _2 in xrange(len(board))]
        self.neighbor_check_map = [[0 for _ in xrange(len(board[0]))] for _2 in xrange(len(board))]
        self.neighbor_check()
        if self.board[click[0]][click[1]] == 'M':
            board[click[0]][click[1]] = 'X'
            return board
        self.helper_rec(click[0], click[1])
        for y in xrange(len(board)):
            for x in xrange(len(board[0])):
                if self.visited[y][x] is True:
                    if self.neighbor_check_map[y][x] == 0:
                        board[y][x] = 'B'
                    else:
                        board[y][x] = str(self.neighbor_check_map[y][x])
        return board
    def helper_rec(self, y, x):
        self.visited[y][x] = True
        if self.neighbor_check_map[y][x] == 0:
            for dydx in self.dydx:
                dy = dydx[0]
                dx = dydx[1]
                newy = y+dy
                newx = x+dx
                if 0 <= newy < len(self.board) and 0 <= newx < len(self.board[0]) and self.visited[newy][newx] is False:
                    self.helper_rec(newy, newx)
        else:
            pass

    def neighbor_check(self):
        for y in xrange(len(self.board)):
            for x in xrange(len(self.board[0])):
                r_sum = 0
                for dydx in self.dydx:
                    dy = dydx[0]
                    dx = dydx[1]
                    newy = y+dy
                    newx = x+dx
                    if 0 <= newy < len(self.board) and 0 <= newx < len(self.board[0]):
                        if self.board[newy][newx] == 'M':
                            r_sum += 1
                self.neighbor_check_map[y][x] = r_sum

BOARD = [['E' for i in xrange(5)] for j in xrange(4)]
BOARD[1][2] = 'M'
CLICK = [3, 0]
SOL = Solution()
print SOL.updateBoard(BOARD, CLICK)
