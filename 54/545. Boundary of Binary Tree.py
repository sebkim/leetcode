# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def boundaryOfBinaryTree(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        left_res = []
        right_res = []
        if root is None: return []
        elif root.left is None and root.right is None:
            return [root.val]
        if root.left is not None:
            self.TraverseBoundary(root, left_res, True)
        if root.right is not None:
            self.TraverseBoundary(root, right_res, False)
        leaf_res = []
        self.TraverseLeaves(root, leaf_res)
        return [root.val] + (left_res[1:-1] if left_res != [] else []) +  leaf_res + (list(reversed(right_res[1:-1])) if right_res != [] else [])
    def TraverseLeaves(self, root, res):
        if root.left:
            self.TraverseLeaves(root.left, res)
        if root.left is None and root.right is None:
            res.append(root.val)
        if root.right:
            self.TraverseLeaves(root.right, res)
    def TraverseBoundary(self, root, res, bool_left):
        if root is not None:
            res.append(root.val)
            if bool_left:
                if root.left:
                    self.TraverseBoundary(root.left, res, bool_left)
                    return
                if root.right:
                    self.TraverseBoundary(root.right, res, bool_left)
            else:
                if root.right:
                    self.TraverseBoundary(root.right, res, bool_left)
                    return
                if root.left:
                    self.TraverseBoundary(root.left, res, bool_left)
