# Definition for singly-linked list.
class ListNode(object):
	def __init__(self, x):
		self.val = x
		self.next = None

class Solution(object):
	def mergeTwoLists(self, l1, l2):
		"""
		:type l1: ListNode
		:type l2: ListNode
		:rtype: ListNode
		"""
		# merged = ListNode()
		# from collections import deque
		curL1 = l1 if l1 is not None else None
		curL2 = l2 if l2 is not None else None
		res = []
		while not (curL1==None or curL2==None):
			if curL1.val <= curL2.val:
				res.append(curL1.val)
				curL1 = curL1.next
			else:
				res.append(curL2.val)
				curL2 = curL2.next
		while curL1!=None:
			res.append(curL1.val)
			curL1 = curL1.next
		while curL2!=None:
			res.append(curL2.val)
			curL2 = curL2.next
		if len(res)>0:
			head = ListNode(res[-1])
			for i in range(len(res)-2, -1, -1):
				head = self.insertNode(head, res[i])
			return head
		else:
			return None
	def insertNode(self, head, val):
		x=ListNode(val)
		x.next=head
		head = x
		return head

sol = Solution()
l1 = ListNode(3)
# tmp=ListNode(1)
# tmp.next=l1
# l1 = tmp
l2 =ListNode(4)
# tmp=ListNode(2)
# tmp.next=l2
# l2=tmp
x = sol.mergeTwoLists(l1, l2)
print x.val
