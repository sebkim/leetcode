class Solution(object):
	def isValid(self, s):
		"""
		:type s: str
		:rtype: bool
		"""
		matchingDic = {'{': '}', '(': ')', '[': ']'}
		stack = []
		for i in s:
			if i=='{' or i=='(' or i=='[':
				stack.append(i)
			elif i=='}' or i==')'or i==']':
				try:
					popped = stack.pop()
				except IndexError:
					return False
				if i==matchingDic[popped]:
					pass
				else:
					return False
			else:
				return False
		if len(stack)!=0:
			return False
		return True

sol = Solution()
x = sol.isValid('()')
print x
