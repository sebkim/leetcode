class Solution(object):
	def strStr(self, haystack, needle):
		"""
		:type haystack: str
		:type needle: str
		:rtype: int
		"""
		lenNeedle = len(needle)
		for startInd in range(len(haystack) - lenNeedle + 1):
			if haystack[startInd:startInd+lenNeedle] == needle:
				return startInd
		return -1

sol = Solution()
x=sol.strStr('zababab','ab')
print x
