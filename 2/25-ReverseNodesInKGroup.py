# Definition for singly-linked list.
class ListNode(object):
	def __init__(self, x):
		self.val = x
		self.next = None
class LinkedList(object):
	def __init__(self, head=None):
		self.head=head
	def insertNode(self, val):
		x=ListNode(val)
		x.next=self.head
		self.head=x
	def seeAll(self):
		cur=self.head
		while cur!=None:
			print cur.val
			cur=cur.next

class Solution(object):
	def reverseKGroup(self, head, k):
		"""
		:type head: ListNode
		:type k: int
		:rtype: ListNode
		"""
		prev=None
		next=None
		current = head
		count = 0
		if self.checkEnoughNum(head, k):
			while current is not None and count<k:
				next = current.next
				current.next = prev
				prev = current
				current = next
				count+=1
			if next is not None:
				head.next = self.reverseKGroup(next, k)
			return prev
		else:
			return head

	def checkEnoughNum(self, head, k):
		current = head
		count=0
		while current is not None and count<k:
			count+=1
			current = current.next
		if count == k:
			return True
		else:
			return False

sol = Solution()
myList = LinkedList()
myList.insertNode(5)
myList.insertNode(4)
myList.insertNode(3)
myList.insertNode(2)
myList.insertNode(1)
myList2 = LinkedList()
myList2.head=sol.reverseKGroup(myList.head, 2)
myList2.seeAll()
