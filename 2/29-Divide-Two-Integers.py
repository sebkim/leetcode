class Solution(object):
	def divide(self, dividend, divisor):
		"""
		:type dividend: int
		:type divisor: int
		:rtype: int
		"""
		def divHelper(dividend, divisor):
			res=0
			remainingVal=True
			while dividend>=divisor:
				counter=0
				modiDivisor=divisor
				while modiDivisor<=dividend:
					modiDivisor = modiDivisor << 1
					counter+=1
				dividend -= modiDivisor >> 1
				counter-=1
				res+=pow(2,counter)
				if dividend == 0:
					remainingVal=False
			return (res, remainingVal)
		if dividend==0:
			return 0
		dividendWOS = -dividend if dividend<0 else dividend
		divisorWOS = -divisor if divisor<0 else divisor
		if dividend>0:
			if divisor>0:
				return divHelper(dividendWOS, divisorWOS)[0]
			else:
				return -divHelper(dividendWOS, divisorWOS)[0]
		else:
			if divisor>0:
				if divHelper(dividendWOS, divisorWOS)[1]==True:
					return -(divHelper(dividendWOS, divisorWOS)[0]+1)
				else:
					return -(divHelper(dividendWOS, divisorWOS)[0])
			else:
				if divHelper(dividendWOS, divisorWOS)[1]==True:
					return (divHelper(dividendWOS, divisorWOS)[0])+1
				else:
					return (divHelper(dividendWOS, divisorWOS)[0])
			
sol = Solution()
x=sol.divide(9,3)
print x
