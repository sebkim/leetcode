class Solution(object):
	def generateParenthesis(self, n):
		"""
		:type n: int
		:rtype: List[str]
		"""
		resList = []
		self.recursiveParen(n, n, resList, "")
		return resList
	def recursiveParen(self, openN, closeN, resList, res):
		if openN==0 and closeN==0:
			resList.append(res)
			return
		if openN>closeN:
			return
		if openN>0:
			self.recursiveParen(openN-1, closeN, resList, res+"(")
		if closeN>0:
			self.recursiveParen(openN, closeN-1, resList, res+")")

sol = Solution()
x = sol.generateParenthesis(2)
print x
