# Definition for singly-linked list.
class ListNode(object):
	def __init__(self, x):
		self.val = x
		self.next = None
class LinkedList(object):
	def __init__(self, head=None):
		self.head=head
	def insertNode(self, val):
		x=ListNode(val)
		x.next=self.head
		self.head=x
	def seeAll(self):
		cur=self.head
		while cur!=None:
			print cur.val
			cur=cur.next

# O(nklog(k))
class Solution(object):
	def mergeKLists(self, lists):
		"""
		:type lists: List[ListNode]
		:rtype: ListNode
		"""
		if len(lists)==0:
			return []

		head=None
		listsAsArray = []
		for i in lists:
			cur=i
			hereList = []
			while cur!=None:
				hereList.append(cur.val)
				cur=cur.next
			listsAsArray.append(hereList)
		merged = self.mergedKRec(listsAsArray)
		
		for i in range(len(merged[0])-1,-1,-1):
			x=ListNode(merged[0][i])
			x.next=head
			head=x
		return head
		
	def mergedKRec(self, lists):
		if len(lists)==1:
			return lists
		nLists = len(lists)
		merged = []
		for i in range(0, nLists-1, 2):
			merged.append(self.merge2Lists(lists[i], lists[i+1]))
		if nLists%2==1:
			merged.append(lists[-1])
		return self.mergedKRec(merged)

	def merge2Lists(self, l1, l2):
		from collections import deque
		buffer1 = deque(l1)
		buffer2 = deque(l2)
		res = []
		while not (len(buffer1)==0 or len(buffer2)==0):
			if buffer1[0] <= buffer2[0]:
				res.append(buffer1.popleft())
			else:
				res.append(buffer2.popleft())
		while not len(buffer1)==0:
			res.append(buffer1.popleft())
		while not len(buffer2)==0:
			res.append(buffer2.popleft())
		return res
