class Solution(object):
	def uniquePathsWithObstacles(self, obstacleGrid):
		"""
		:type obstacleGrid: List[List[int]]
		:rtype: int
		"""
		if obstacleGrid==[]:
			return 0
		m = len(obstacleGrid)
		n = len(obstacleGrid[0])
		dpTable = [[None for i in range(n)] for j in range(m)]
		dpTable[0][0] = 1 if obstacleGrid[0][0]!=1 else 0
		for i in range(1, n):
			dpTable[0][i] = dpTable[0][i-1] if obstacleGrid[0][i]!=1 else 0
		for i in range(1, m):
			dpTable[i][0] = dpTable[i-1][0] if obstacleGrid[i][0]!=1 else 0
		for i in range(1, m):
			for j in range(1, n):
				dpTable[i][j] = dpTable[i-1][j] + dpTable[i][j-1] if obstacleGrid[i][j]!=1 else 0
		return dpTable[m-1][n-1]

# mat = [
#   [0,0,0],
#   [0,1,0],
#   [0,0,0]
# ]
mat = [[0]]
sol = Solution()
print sol.uniquePathsWithObstacles(mat)