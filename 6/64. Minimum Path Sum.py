class Solution(object):
	def minPathSum(self, grid):
		"""
		:type grid: List[List[int]]
		:rtype: int
		"""
		if grid==[]:
			return 0
		m = len(grid)
		n = len(grid[0])
		dpTable = [[None for i in range(n)] for j in range(m)]
		parentTab = [[None for i in range(n)] for j in range(m)]
		dpTable[0][0] = grid[0][0]
		parentTab[0][0] = -1
		for i in range(1, n):
			dpTable[0][i] = dpTable[0][i-1] + grid[0][i]
			parentTab[0][i] = 'R'
		for i in range(1, m):
			dpTable[i][0] = dpTable[i-1][0] + grid[i][0]
			parentTab[i][0] = 'D'
		for i in range(1, m):
			for j in range(1, n):
				dpTable[i][j] = min(dpTable[i-1][j], dpTable[i][j-1])+ grid[i][j]
				parentTab[i][j] = 'D' if dpTable[i-1][j] < dpTable[i][j-1] else 'R'
		return dpTable[m-1][n-1]
        
sol = Solution()
grid = [[1,2,3],[4,5,6],[7,8,9]]
print sol.minPathSum(grid)