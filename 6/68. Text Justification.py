import operator
class Solution(object):
	def fullJustify(self, words, maxWidth):
		"""
		:type words: List[str]
		:type maxWidth: int
		:rtype: List[str]
		"""
		res = []
		oneLine = []
		sumLen = 0
		firstWordInLine = True
		i=0
		while i<len(words):
			if firstWordInLine:
				lenSpace = 0
			else:
				lenSpace = 1
			hereLen = len(words[i])
			if sumLen + hereLen + lenSpace<= maxWidth:
				sumLen += hereLen + lenSpace
				oneLine.append(words[i])
				firstWordInLine = False
			else:
				res.append(oneLine)
				oneLine = []
				sumLen = 0
				firstWordInLine = True
				i-=1
			i+=1
		res.append(oneLine)
		res2 = []
		for eachSetInd, eachSet in enumerate(res):
			numGap = len(eachSet)-1
			lenStrsInEachSet = reduce(operator.add, map(len, eachSet))
			leftGap = maxWidth - numGap - lenStrsInEachSet
			gapList = [1 for i in range(numGap)]
			breakBool = False
			if gapList and leftGap>0:
				if eachSetInd!=len(res)-1:
					while True:
						for eachGapInd in range(len(gapList)):
							gapList[eachGapInd]+=1
							leftGap-=1
							if leftGap<=0:
								breakBool = True
								break
						if breakBool:
							break
			hereStr = ""
			for eachWordInd, eachWord in enumerate(eachSet):
				hereStr += eachWord
				if eachWordInd < numGap:
					hereStr += " "*gapList[eachWordInd]
			if len(hereStr) < maxWidth:
				while len(hereStr) < maxWidth:
					hereStr += " "
			res2.append(hereStr)
		return res2

sol = Solution()
# words = ["This", "is", "an", "example", "of", "text", "justification."]
# maxWidth = 16
words = ["What","must","be","shall","be."]
maxWidth = 12
print sol.fullJustify(words, maxWidth)