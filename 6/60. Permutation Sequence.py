class Solution(object):
	def __init__(self):
		self.fact = []
	def getPermutation(self, n, k):
		"""
		:type n: int
		:type k: int
		:rtype: str
		"""
		self.getFact(n)
		resPerm = []
		_, k = divmod(k, self.fact[n]+1)
		k-=1
		tryN = n-1
		candidates = range(1, n+1)
		while len(resPerm) < n:
			whichPos, k = divmod(k , self.fact[tryN])
			tryN-=1
			resPerm.append(candidates[whichPos])
			candidates.pop(whichPos)
		return ''.join([str(i) for i in resPerm])
	def getFact(self, n):
		self.fact.append(1)
		for i in xrange(1, n+1):
			val = self.fact[-1] * i
			self.fact.append(val)

sol = Solution()
num = 3
k = 2
print sol.getPermutation(num, k)
