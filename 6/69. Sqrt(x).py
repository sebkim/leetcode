class Solution(object):
	def mySqrt(self, x):
		"""
		:type x: int
		:rtype: int
		"""
		return self.binSqrt(0, x, x)

	def binSqrt(self, start, end, target):
		if (end-start)<=1:
			if target==1:
				return end
			return start
		mid = (end+start) // 2
		if pow(mid, 2) > target:
			return self.binSqrt(start, mid, target)
		elif pow(mid, 2) < target:
			return self.binSqrt(mid, end, target)
		else:
			return mid

sol = Solution()
x =36
print sol.mySqrt(x)