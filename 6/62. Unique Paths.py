class Solution(object):
	def uniquePaths(self, m, n):
		"""
		:type m: int
		:type n: int
		:rtype: int
		"""
		def facto(n):
			res = 1
			for i in range(1, n+1):
				res*=i
			return res
		return facto(m+n-2) / (facto(m-1) * facto(n-1) )

sol = Solution()
m=1
n=2
print sol.uniquePaths(m, n)