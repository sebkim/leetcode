import re
class Solution(object):
	def isNumber(self, s):
		"""
		:type s: str
		:rtype: bool
		"""
		s = s.strip()
		if s=='':
			return False
		# 12.32
		# 12. 
		# .32
		# 3
		num_format = re.compile("^[\+-]?[0-9]*\.?[0-9]*$") 
		isnumber = re.match(num_format,s)
		# +.
		# +
		# -
		# -.
		num_not_format = re.compile("^[\+-]?\.?$")
		isnotnumber = re.match(num_not_format,s)
		if 'e' in s:
			beforeE = s[:s.index('e')]
			afterE = s[s.index('e')+1:]
			beforeE = beforeE.lstrip()
			afterE = afterE.rstrip()
			if beforeE=="":
				return False
			if re.match(num_format, beforeE) and not re.match(num_not_format, beforeE) and re.match(r"^[\+-]?[0-9]+$", afterE):
				return True
			else:
				return False
		else:
			if (isnumber and not isnotnumber):
				return True
			else:
				return False
		
sol = Solution()
s = "-e58"
print sol.isNumber(s)


