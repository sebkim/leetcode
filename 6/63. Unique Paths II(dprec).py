class Solution(object):
	def __init__(self):
		self.cached = None
		self.obstacleGrid = None
		self.m = None
		self.n = None
	def uniquePathsWithObstacles(self, obstacleGrid):
		"""
		:type obstacleGrid: List[List[int]]
		:rtype: int
		"""
		if obstacleGrid==[]:
			return 0
		self.m = len(obstacleGrid)
		self.n = len(obstacleGrid[0])
		self.obstacleGrid = obstacleGrid
		self.cached = [[[None] for _ in xrange(self.n)] for _ in xrange(self.m)]
		if self.obstacleGrid[0][0]==1: return 0
		return self.dp(0, 0)
	def dp(self, i, j):
		if i==self.m-1 and j==self.n-1:
			return 1
		ret = self.cached[i][j]
		if ret[0] is not None: return ret[0]
		ret[0] = 0
		if i<self.m-1 and self.obstacleGrid[i+1][j]!=1:
			ret[0] += self.dp(i+1, j)
		if j<self.n-1 and self.obstacleGrid[i][j+1]!=1:
			ret[0] += self.dp(i, j+1)
		return ret[0]

mat = [
  [0,0,0],
  [0,1,0],
  [0,0,0]
]

# mat = [[0]]
sol = Solution()
print sol.uniquePathsWithObstacles(mat)
