from collections import Counter
class Solution(object):
	def __init__(self):
		self.res = 0
	def uniquePaths(self, m, n):
		"""
		:type m: int
		:type n: int
		:rtype: int
		"""
		a=[]
		self.backtrack(a, 0, (m, n))
		return self.res

	def backtrack(self, a, k, inputParam):
		c=[]
		if self.is_a_solution(a, k, inputParam):
			self.process_solution(a, k, inputParam)
		else:
			k+=1
			self.construct_candidates(a, k, inputParam, c)
			for i in c:
				a.append(i)
				self.backtrack(a, k, inputParam)
				a.pop()
	def is_a_solution(self, a, k, inputParam):
		hereCounter = Counter(a)
		if hereCounter['D'] == inputParam[0]-1 and hereCounter['R'] == inputParam[1]-1:
			return True
		else:
			return False
	def process_solution(self, a, k, inputParam):
		print a
		self.res+=1
	def construct_candidates(self, a, k, inputParam, c):
		hereCounter = Counter(a)
		if hereCounter['D']<inputParam[0]-1:
			c.append('D')
		if hereCounter['R']<inputParam[1]-1:
			c.append('R')

sol = Solution()
m=4
n=4
print sol.uniquePaths(m, n)