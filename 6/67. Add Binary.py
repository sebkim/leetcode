class Solution(object):
	## recursive version
	# def addBinary(self, a, b):
	# 	"""
	# 	:type a: str
	# 	:type b: str
	# 	:rtype: str
	# 	"""
	# 	if a=='': return b
	# 	if b=='': return a
	# 	if a[-1]=='1' and b[-1]=='1':
	# 		# '1' is carry
	# 		return self.addBinary(self.addBinary(a[:-1], b[:-1]), '1') + '0'
	# 	elif a[-1]=='0' and b[-1]=='0':
	# 		return self.addBinary(a[:-1], b[:-1]) + '0'
	# 	else:
	# 		return self.addBinary(a[:-1], b[:-1]) + '1'
	## 
	## iterative version
	def addBinary(self, a, b):
		i = len(a)-1; j = len(b)-1;
		carry = 0
		res = ''
		while i>=0 or j>=0 or carry:
			hereSum = 0
			if i>=0:
				hereSum += ord(a[i]) - ord('0')
				i-=1
			if j>=0:
				hereSum += ord(b[j]) - ord('0')
				j-=1
			hereSum += carry
			res += str(hereSum & 1)
			carry = (hereSum>>1) & 1
		return ''.join(reversed(res))

a='1011'
b='11'
sol = Solution()
print sol.addBinary(a,b)
