# Definition for singly-linked list.
class ListNode(object):
	def __init__(self, x):
		self.val = x
		self.next = None

class Solution(object):
	def rotateRight(self, head, k):
		"""
		:type head: ListNode
		:type k: int
		:rtype: ListNode
		"""
		if head is None:
			return None
		if k==0:
			return head
		lenList = 0
		cur = head
		while cur:
			cur = cur.next
			lenList+=1
		k = k % lenList

		from collections import deque
		cur = head
		nums = deque([])
		while cur:
			nums.append(cur.val)
			cur = cur.next
		nums.rotate(k)

		newHead = ListNode(nums.popleft())
		cur = newHead
		for i in range(len(nums)):
			cur.next = ListNode(nums.popleft())
			cur = cur.next
		return newHead
        
