
# Definition for a  binary tree node
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class BSTIterator(object):
    def __init__(self, root):
        """
        :type root: TreeNode
        """
        self.stack = []
        self.push_all(root)
    def push_all(self, root):
        cur = root
        while cur is not None:
            self.stack.append(cur)
            cur = cur.left
    def hasNext(self):
        """
        :rtype: bool
        """
        if len(self.stack)!=0:
            return True
        else:
            return False
    def next(self):
        """
        :rtype: int
        """
        tmp = self.stack.pop()
        if tmp.right:
            self.push_all(tmp.right)
        return tmp.val

# Your BSTIterator will be called like this:
# i, v = BSTIterator(root), []
# while i.hasNext(): v.append(i.next())
