# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def rob(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        notrob, rob = self.rec_rob(root)
        return max(notrob, rob)
    def rec_rob(self, root):
        if root is None: return 0, 0
        notrob_left, rob_left = self.rec_rob(root.left)
        notrob_right, rob_right = self.rec_rob(root.right)
        return max(notrob_left, rob_left) + max(notrob_right, rob_right), \
        root.val + notrob_left + notrob_right
