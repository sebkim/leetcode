# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def __init__(self):
        self.mymap = None
    def rob(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        self.mymap = {}
        return self.rec_rob(root)
    def rec_rob(self, root):
        if root is None: return 0
        if root in self.mymap: return self.mymap[root]
        # not rob root case
        l_result = self.rec_rob(root.left)
        r_result = self.rec_rob(root.right)
        # rob case
        result = root.val
        if root.left is not None:
            result += self.rec_rob(root.left.left) + self.rec_rob(root.left.right)
        if root.right is not None:
            result += self.rec_rob(root.right.left) + self.rec_rob(root.right.right)
        ans = max(result, l_result+r_result)
        self.mymap[root] = ans
        return ans
