# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def __init__(self):
        self.largest = 0
    def largestBSTSubtree(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        self.check_bst(root)
        return self.largest
    # is_it_bst, num_nodes, min, max
    def check_bst(self, root):
        if root is None: return True, 0, float('inf'), -float('inf')
        left_result = self.check_bst(root.left)
        right_result = self.check_bst(root.right)
        num_nodes = left_result[1] + right_result[1] + 1
        min_val = min(left_result[2], right_result[2], root.val)
        max_val = max(left_result[3], right_result[3], root.val)
        if left_result[0] and right_result[0] and root.val>left_result[3] and root.val < right_result[2]:
            if self.largest < num_nodes:
                self.largest = num_nodes
            return True, num_nodes, min_val, max_val
        else:
            return False, num_nodes, min_val, max_val
