from collections import defaultdict
class Solution(object):
    def __init__(self):
        self.adj_list = defaultdict(list)
    def findItinerary(self, tickets):
        """
        :type tickets: List[List[str]]
        :rtype: List[str]
        """
        self.make_graph(tickets)
        circuit = []
        self.GetEulerTrail('JFK', circuit)
        return list(reversed(circuit))
    def GetEulerTrail(self, here, circuit):
        cur = self.adj_list[here]
        while len(cur)>0:
            there = cur.pop()
            self.GetEulerTrail(there, circuit)
        circuit.append(here)
    def make_graph(self, tickets):
        for ticket in tickets:
            self.adj_list[ticket[0]].append(ticket[1])
        for key in self.adj_list:
            self.adj_list[key].sort(reverse=True)
