class Solution(object):
    def __init__(self):
        self.nums = None
        self.cached = None
    def optimalDivision(self, nums):
        """
        :type nums: List[int]
        :rtype: str
        """
        self.cached = [[[None] for i in xrange(len(nums))] for j in xrange(len(nums))]
        self.nums = nums
        _,_,_,ret = self.helper(0, len(nums)-1)
        return ret
    def helper(self, i1, i2):
        if i1<0 or i2<0 or i1>=len(self.nums) or i2>=len(self.nums): assert False
        if i1==i2:
            ans = self.nums[i1]
            return ans, ans, str(self.nums[i1]), str(self.nums[i1])
        elif i2-i1==1:
            ans = self.nums[i1]/float(self.nums[i2])
            tmppartial = str(self.nums[i1])+'/'+str(self.nums[i2])
            return ans, ans, tmppartial, tmppartial
        ret = self.cached[i1][i2]
        if ret[0] is not None:
            return ret[0]
        hmin = float('inf'); hmax = -1
        for i in xrange(i1, i2):
            leftmin, leftmax, leftminpartial, leftmaxpartial= self.helper(i1, i)
            rightmin, rightmax, rightminpartial, rightmaxpartial = self.helper(i+1, i2)
            loopmin = leftmin / float(rightmax)
            loopmax = leftmax / float(rightmin)
            if hmin>loopmin:
                hmin = loopmin
                minpartial = leftminpartial +"/" + ("" if rightmaxpartial.isdigit() else "(") + \
                rightmaxpartial + ("" if rightmaxpartial.isdigit() else ")")
            if hmax<loopmax:
                hmax = loopmax
                maxpartial = leftmaxpartial +"/" + ("" if rightminpartial.isdigit() else "(") + \
                rightminpartial + ("" if rightminpartial.isdigit() else ")")
        ret[0] = (hmin, hmax, minpartial, maxpartial)
        return ret[0]

NUMS = [1000, 100, 10, 2]
SOL = Solution()
print SOL.optimalDivision(NUMS)
