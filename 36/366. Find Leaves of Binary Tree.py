# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
from collections import defaultdict

class Solution(object):
    def __init__(self):
        self.res = defaultdict(list)
    def findLeaves(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        self.helper(root)
        return self.res.values()
    def helper(self, root):
        if root is None: return -1
        h = 1 + max(self.helper(root.left), self.helper(root.right))
        self.res[h].append(root.val)
        return h
