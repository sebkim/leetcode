# """
# This is the interface that allows for creating nested lists.
# You should not implement it, or speculate about its implementation
# """
#class NestedInteger(object):
#    def __init__(self, value=None):
#        """
#        If value is not specified, initializes an empty list.
#        Otherwise initializes a single integer equal to value.
#        """
#
#    def isInteger(self):
#        """
#        @return True if this NestedInteger holds a single integer, rather than a nested list.
#        :rtype bool
#        """
#
#    def add(self, elem):
#        """
#        Set this NestedInteger to hold a nested list and adds a nested integer elem to it.
#        :rtype void
#        """
#
#    def setInteger(self, value):
#        """
#        Set this NestedInteger to hold a single integer equal to value.
#        :rtype void
#        """
#
#    def getInteger(self):
#        """
#        @return the single integer that this NestedInteger holds, if it holds a single integer
#        Return None if this NestedInteger holds a nested list
#        :rtype int
#        """
#
#    def getList(self):
#        """
#        @return the nested list that this NestedInteger holds, if it holds a nested list
#        Return None if this NestedInteger holds a single integer
#        :rtype List[NestedInteger]
#        """

class Solution(object):
    def __init__(self):
        self.md = None
    def depthSumInverse(self, nestedList):
        """
        :type nestedList: List[NestedInteger]
        :rtype: int
        """
        self.md = self.MaxDepth(nestedList, 1)
        return self.helper(nestedList, 1)
    def helper(self, nestedList, depth):
        ret = 0
        for each in nestedList:
            if each.isInteger():
                ret += each.getInteger() * (self.md-depth+1)
            else:
                ret += self.helper(each.getList(), depth+1)
        return ret
    def MaxDepth(self, nestedList, depth):
        tmp = []
        for each in nestedList:
            if not each.isInteger():
                tmp.append(self.MaxDepth(each.getList(), depth+1))
        if len(tmp)==0: return depth
        else: return max(tmp)
