class Solution(object):
	def __init__(self):
		self.grid = None
		self.verticalKillMap = None
		self.horizontalKillMap = None
	def maxKilledEnemies(self, grid):
		"""
		:type grid: List[List[str]]
		:rtype: int
		"""
		if grid==[] or grid==[[]]: return 0
		self.grid = grid
		self.verticalKillMap = [[None for i in range(len(grid[0]))] for j in range(len(grid))]
		self.horizontalKillMap = [[None for i in range(len(grid[0]))] for j in range(len(grid))]
		for i in xrange(len(grid)):
			self.fillWhichRow(i)
		for i in xrange(len(grid[0])):
			self.fillWhichCol(i)
		sumMap = [[None for i in range(len(grid[0]))] for j in range(len(grid))]
		for i in xrange(len(grid)):
			for j in xrange(len(grid[0])):
				if self.verticalKillMap[i][j] is not None and self.horizontalKillMap[i][j] is not None:
					sumMap[i][j] = self.verticalKillMap[i][j] + self.horizontalKillMap[i][j]
		maxVal= max( [ max(j) for j in sumMap] )
		if maxVal is not None:
			return maxVal 
		return 0
	def fillWhichCol(self, col):
		numEnemy=0
		res=[]
		for row in xrange(len(self.grid)):
			cell = self.grid[row][col]
			if cell=='E':
				numEnemy+=1
			elif cell=='W':
				res.append(numEnemy)
				numEnemy=0
		res.append(numEnemy)
		res_ptr=0
		for row in xrange(len(self.grid)):
			cell = self.grid[row][col]
			if cell=='0':
				self.verticalKillMap[row][col] = res[res_ptr]
			elif cell=='W':
				res_ptr+=1
	def fillWhichRow(self, row):
		numEnemy=0
		res=[]
		for col in xrange(len(self.grid[0])):
			cell = self.grid[row][col]
			if cell=='E':
				numEnemy+=1
			elif cell=='W':
				res.append(numEnemy)
				numEnemy=0
		res.append(numEnemy)
		res_ptr=0
		for col in xrange(len(self.grid[0])):
			cell = self.grid[row][col]
			if cell=='0':
				self.horizontalKillMap[row][col] = res[res_ptr]
			elif cell=='W':
				res_ptr+=1

grid = ["0E00","E0WE","0E00"]
sol=Solution()
print sol.maxKilledEnemies(grid)
