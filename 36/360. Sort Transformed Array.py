class Solution(object):
	def sortTransformedArray(self, nums, a, b, c):
		"""
		:type nums: List[int]
		:type a: int
		:type b: int
		:type c: int
		:rtype: List[int]
		"""
		i=0; j=len(nums)-1;
		hereSorted = [None for _ in xrange(len(nums))]
		if a>=0:
			hereSortedInd = j
		else:
			hereSortedInd = 0
		while i<=j:
			leftVal = self.quad(nums[i], a, b, c)
			rightVal = self.quad(nums[j], a, b, c)
			if a>=0:
				if  leftVal>=rightVal:
					hereSorted[hereSortedInd] = leftVal
					i+=1
				else:
					hereSorted[hereSortedInd] = rightVal
					j-=1
				hereSortedInd-=1
			else:
				if leftVal<=rightVal:
					hereSorted[hereSortedInd] = leftVal
					i+=1
				else:
					hereSorted[hereSortedInd] = rightVal
					j-=1
				hereSortedInd+=1
		return hereSorted
	def quad(self, x, a, b, c):
		return a*x*x + b*x + c

nums = [-4, -2, 2, 4]
a=1
b=3
c=5
sol = Solution()
print sol.sortTransformedArray(nums, a, b, c)
