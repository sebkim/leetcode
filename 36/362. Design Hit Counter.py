class HitCounter(object):
	def __init__(self):
		"""
		Initialize your data structure here.
		"""
		self.times = [None for _ in xrange(301)]
		self.hits = [None for _ in xrange(301)]
	def hit(self, timestamp):
		"""
		Record a hit.
		@param timestamp - The current timestamp (in seconds granularity).
		:type timestamp: int
		:rtype: void
		"""
		ind = ((timestamp-1) % 300) +1
		if self.times[ind]!=timestamp:
			self.times[ind] = timestamp
			self.hits[ind] = 1
		else:
			self.hits[ind]+=1
	def getHits(self, timestamp):
		"""
		Return the number of hits in the past 5 minutes.
		@param timestamp - The current timestamp (in seconds granularity).
		:type timestamp: int
		:rtype: int
		"""
		ret = 0
		for eachTimeInd, eachTime in enumerate(self.times):
			if eachTime >= timestamp-300+1:
				ret += self.hits[eachTimeInd]
		return ret
			
# Your HitCounter object will be instantiated and called as such:
# obj = HitCounter()
# obj.hit(timestamp)
# param_2 = obj.getHits(timestamp)