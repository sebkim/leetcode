# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class TreeNode2(TreeNode):
	def __init__(self, x):
		super(TreeNode2, self).__init__(x)
		self.parent = None
class Solution(object):
	# ## recursive version
	# def lowestCommonAncestor(self, root, p, q):
	# 	"""
	# 	:type root: TreeNode
	# 	:type p: TreeNode
	# 	:type q: TreeNode
	# 	:rtype: TreeNode
	# 	"""
	# 	if root in (None, p, q): return root
	# 	left, right = [self.lowestCommonAncestor(i, p, q) for i in [root.left, root.right]]
	# 	return root if left and right else left or right
	# ##
	## only O(n) for first query and thereafter O(logn) version
	def __init__(self):
		self.p2 = None
		self.q2 = None
	def lowestCommonAncestor(self, root, p, q):
		root2 = self.createRoot2(root, p, q)
		def pathFind(fromNode, toNode, res):
			cur = fromNode
			while cur:
				res.append(cur)
				cur = cur.parent
			return res
		pPath = []
		pathFind(self.p2, root2, pPath)
		qPath = []
		pathFind(self.q2, root2, qPath)
		lca = [a for a,b in zip(reversed(pPath), reversed(qPath))[::-1] if a==b][0]
		return lca
	def createRoot2(self, root, p, q, parentParam = None):
		if root is None: return
		node = TreeNode2(root.val)
		node.parent = parentParam
		# track p and q in root2
		if root is p: self.p2 = node
		if root is q: self.q2 = node
		if root.left:
			node.left = self.createRoot2(root.left, p, q, node)
		if root.right:
			node.right = self.createRoot2(root.right, p, q, node)
