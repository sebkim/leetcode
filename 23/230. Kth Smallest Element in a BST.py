# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def __init__(self):
        self.size_cac = {}
    def kthSmallest(self, root, k):
        """
        :type root: TreeNode
        :type k: int
        :rtype: int
        """
        return self.kth_rec(root, k)
    def kth_rec(self, root, k):
        if root is None: return None
        ls = self.size(root.left) if root.left is not None else 0
        if ls+1 == k: return root.val
        elif ls+1>k: return self.kth_rec(root.left, k)
        else: return self.kth_rec(root.right, k-ls-1)
    def size(self, root):
        if root is None: return 0
        if self.size_cac.get(root) is not None: return self.size_cac.get(root)
        s = 1
        if root.left:
            s += self.size(root.left)
        if root.right:
            s += self.size(root.right)
        self.size_cac[root] = s
        return s
