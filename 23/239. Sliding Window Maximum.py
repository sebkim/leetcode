from collections import deque
class Solution(object):
	def maxSlidingWindow(self, nums, k):
		"""
		:type nums: List[int]
		:type k: int
		:rtype: List[int]
		"""
		if k==0: return []
		res = [None for _ in xrange(len(nums)-k+1)]
		resInd=0
		# deq contains candidate's index
		deq = deque()
		for i in xrange(len(nums)):
			# filter out of range thing (leftside out of range)
			if len(deq)!=0 and deq[0] < i-k+1:
				deq.popleft()
			# filter useless things. if deqLastThing < newlyAddedNum
			while len(deq)!=0 and nums[deq[-1]] < nums[i]:
				deq.pop()
			deq.append(i)
			if i>=k-1:
				res[resInd] = nums[deq[0]]
				resInd+=1
		return res
