class Solution(object):
	def productExceptSelf(self, nums):
		"""
		:type nums: List[int]
		:rtype: List[int]
		"""
		res = [None for _ in xrange(len(nums))]
		fromStart = [1]
		for i in xrange(1, len(nums)):
			fromStart.append(fromStart[-1] * nums[i-1])
		fromEnd = 1
		for i in xrange(len(nums)-1, -1 , -1):
			res[i] = fromStart[i] * fromEnd
			fromEnd *= nums[i]
		return res

nums = [1,2,3,4]
sol = Solution()
print sol.productExceptSelf(nums)
