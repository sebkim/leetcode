# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
	def isPalindrome(self, head):
		"""
		:type head: ListNode
		:rtype: bool
		"""
		if head is None: return True
		slow = head; fast = head
		while fast.next is not None and fast.next.next is not None:
			fast = fast.next.next
			slow = slow.next
		slow.next = Solution.reverseLL(slow.next)
		slow = slow.next
		while slow:
			if slow.val != head.val: return False
			slow = slow.next
			head = head.next
		return True
	@staticmethod
	def reverseLL(head):
		cur = head
		prev = None; next = None;
		while cur:
			next = cur.next
			cur.next = prev
			prev = cur
			cur = next
		return prev