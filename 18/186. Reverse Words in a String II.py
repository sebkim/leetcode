class Solution:
    # @param s, a list of 1 length strings, e.g., s = ['h','e','l','l','o']
    # @return nothing
    def reverseWords(self, s):
    	if len(s)==0: return
    	# reverse whole
    	Solution.reverse(s, 0, len(s)-1)
    	# reverse each word respectively
    	start = 0
    	for i in xrange(len(s)):
    		if s[i]==' ':
    			Solution.reverse(s, start, i-1)
    			start = i+1
    	# make sure reverse last word
    	Solution.reverse(s, start, len(s)-1)
    	print s
    @staticmethod
    def reverse(s, start, end ):
    	temp = s[start]
    	while start<end:
    		s[start] =s[end]
    		s[end] = temp
    		start+=1
    		end-=1
    		temp = s[start]
    	
s = ['a', 'b', 'c', ' ', 'd', 'e', 'f']
s = []
sol = Solution()
print sol.reverseWords(s)