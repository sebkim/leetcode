class Solution(object):
	def canJump(self, nums):
		"""
		:type nums: List[int]
		:rtype: bool
		"""
		if nums==[]:
			return True
		closestJump =len(nums)-1
		for i in range(len(nums)-2, -1, -1):
			if i + nums[i] >= closestJump:
				closestJump = i
		return closestJump==0

sol = Solution()
# nums =[2,3,1,1,4]
nums = [3,2,1,0,4]
print sol.canJump(nums)
