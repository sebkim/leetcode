class Solution(object):
	def generateMatrix(self, n):
		"""
		:type n: int
		:rtype: List[List[int]]
		"""
		def indCheck():
			if startRowInd>endRowInd or startColInd> endColInd:
				return False
			return True
		from collections import deque
		startRowInd = 0
		startColInd = 0
		endRowInd = n-1
		endColInd = n-1
		mat = [[None for i in range(n)] for j in range(n)]
		numsToPut = [i for i in range(1, n*n+1)]
		numsToPut = deque(numsToPut)
		while startRowInd <= endRowInd and startColInd <= endColInd:
			# first row
			for i in range(startColInd, endColInd+1):
				mat[startRowInd][i] = numsToPut.popleft()
			startRowInd+=1
			# print res, startRowInd
			if not indCheck():
				break
			# last column
			for i in range(startRowInd, endRowInd+1):
				mat[i][endColInd] = numsToPut.popleft()
			endColInd-=1
			# print res, endColInd
			if not indCheck():
				break
			# last row
			for i in range(endColInd, startColInd-1, -1):
				mat[endRowInd][i] = numsToPut.popleft()
			endRowInd-=1
			# print res, endRowInd
			if not indCheck():
				break
			# first column
			for i in range(endRowInd, startRowInd-1, -1):
				mat[i][startRowInd-1] = numsToPut.popleft()
			startColInd+=1
			# print res, startColInd
			if not indCheck():
				break
		return mat

sol = Solution()
nums = 4
print sol.generateMatrix(nums)
