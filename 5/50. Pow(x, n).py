class Solution(object):
	def myPow(self, x, n):
		"""
		:type x: float
		:type n: int
		:rtype: float
		"""
		if n==0:
			return 1.
		if n<0 and n%2==1:
			temp = self.myPow(x, n//2+1)
		else:
			temp = self.myPow(x, n//2)
		if n%2==0:
			return temp*temp
		else:
			if n>0:
				return x*temp*temp
			else:
				return temp*temp/x
sol = Solution()
x = 2
n = 3
print sol.myPow(x, n)
