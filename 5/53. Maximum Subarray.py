class Solution(object):
	def maxSubArray(self, nums):
		"""
		:type nums: List[int]
		:rtype: int
		"""
		curr_max = nums[0]
		max_so_far = curr_max
		for i in range(1, len(nums)):
			curr_max = max(nums[i], curr_max + nums[i])
			if max_so_far < curr_max:
				max_so_far = curr_max
		return max_so_far