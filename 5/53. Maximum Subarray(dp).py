import sys
sys.setrecursionlimit(10000)
class Solution(object):
	def __init__(self):
		self.cached = None
		self.nums = None
		self.ret = None
	def maxSubArray(self, nums):
		"""
		:type nums: List[int]
		:rtype: int
		"""
		self.nums = nums
		self.cached = [[None] for _ in xrange(len(nums))]
		self.ret = -1
		self.dp(len(nums)-1)
		return self.ret
	def dp(self, endind):
		if endind == 0:
			return self.nums[0]
		ret = self.cached[endind]
		if ret[0] is not None: return ret[0]
		prevret = self.dp(endind-1)
		ret[0] = self.nums[endind]
		ret[0] += prevret if prevret>0 else 0
		self.ret = max(self.ret, ret[0])
		return ret[0]
