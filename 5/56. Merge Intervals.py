# Definition for an interval.
class Interval(object):
	def __init__(self, s=0, e=0):
		self.start = s
		self.end = e

class Solution(object):
	def merge(self, intervals):
		"""
		:type intervals: List[Interval]
		:rtype: List[Interval]
		"""
		intervals = sorted(intervals, key=lambda x: x.start)
		stack = []
		for eachInterval in intervals:
			if len(stack)==0:
				stack.append(eachInterval)
			else:
				if self.isOverlap(stack[-1], eachInterval):
					if stack[-1].end < eachInterval.end:
						stack[-1].end = eachInterval.end
					else:
						pass
				else:
					stack.append(eachInterval)
		return stack

	def isOverlap(self, first, second):
		if first.start > second.start:
			first, second = second, first
		if first.end >= second.start:
			return True
		else:
			return False

	def getOverlap(self, first, second):
		if first.start > second.start:
			first, second = second, first
		newInterval = Interval(first.start, max(first.end, second.end))
		return newInterval

sol = Solution()
intervals = [[1,3],[2,6],[8,10],[15,18]]
intervalsObjs = [Interval(i[0], i[1]) for i in intervals]
print sol.merge(intervalsObjs)
