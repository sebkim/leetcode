class Solution(object):
	def spiralOrder(self, matrix):
		"""
		:type matrix: List[List[int]]
		:rtype: List[int]
		"""
		def indCheck():
			if startRowInd>endRowInd or startColInd> endColInd:
				return False
			return True
		if matrix==[]:
			return []
		startRowInd = 0
		startColInd = 0
		endRowInd = len(matrix)-1
		endColInd = len(matrix[0])-1
		res = []
		while startRowInd <= endRowInd and startColInd <= endColInd:
			# first row
			for i in range(startColInd, endColInd+1):
				res.append(matrix[startRowInd][i])
			startRowInd+=1
			# print res, startRowInd
			if not indCheck():
				break
			# last column
			for i in range(startRowInd, endRowInd+1):
				res.append(matrix[i][endColInd])
			endColInd-=1
			# print res, endColInd
			if not indCheck():
				break
			# last row
			for i in range(endColInd, startColInd-1, -1):
				res.append(matrix[endRowInd][i])
			endRowInd-=1
			# print res, endRowInd
			if not indCheck():
				break
			# first column
			for i in range(endRowInd, startRowInd-1, -1):
				res.append(matrix[i][startRowInd-1])
			startColInd+=1
			# print res, startColInd
			if not indCheck():
				break
		return res

sol = Solution()
# matrix = [[j*5+i+1 for i in range(5)] for j in range(5)]
matrix =[[2,3]]
print matrix
print sol.spiralOrder(matrix)
