# O(n^2)
class Solution(object):
	def repeatedSubstringPattern(self, str):
		"""
		:type str: str
		:rtype: bool
		"""
		nstr = len(str)
		nowLen = nstr
		divisor = 2
		while divisor<=nstr:
			if nowLen % divisor == 0:
				nowLen = nstr / divisor
				template = str[:nowLen]
				if str == template*divisor:
					return True
				# print nowLen, divisor, template
			else: pass
			divisor+=1
		return False
		
s = 'bb'
s = 'abcabc'
sol = Solution()
print sol.repeatedSubstringPattern(s)
