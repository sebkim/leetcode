# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class TreeNodeP(TreeNode):
    def __init__(self, x, parent=None):
        super(TreeNodeP, self).__init__(x)
        self.parent = parent

class Solution(object):
    def deleteNode(self, root, key):
        """
        :type root: TreeNode
        :type key: int
        :rtype: TreeNode
        """
        new_root = self.preorder(root)
        return self.delete(new_root, key)
    def delete(self, new_root, key):
        target = self.search(new_root, key)
        if target is None: return new_root
        cs = self.child_size(target);
        if cs == 0:
            if target.parent is None:
                return None
            else:
                if target.parent.left is target:
                    target.parent.left = None
                elif target.parent.right is target:
                    target.parent.right = None
        elif cs == 1:
            if target.left:
                target_bottom = target.left
            elif target.right:
                target_bottom = target.right
            if target.parent is None:
                new_root = target_bottom
            else:
                if target.parent.left is target:
                    target.parent.left = target_bottom
                elif  target.parent.right is target:
                    target.parent.right = target_bottom
                del target
        elif cs == 2:
            succ = self.successor(target)
            new_root = self.delete(new_root, succ.val)
            target.val = succ.val
            del succ
        return new_root
    def successor(self, root):
        if root.right is not None:
            succ = root.right
            while succ.left is not None:
                succ = succ.left
            return succ
        # up direction case
        else:
            cur = root
            if cur.parent is None: return None
            while cur.parent is not None and cur.parent.right is cur:
                cur = cur.parent
            if cur.parent is not Noen and cur.parent.left is cur:
                return cur.parent
            else:
                return None
    def search(self, root, x):
        if root is None: return None
        if root.val == x: return root
        elif root.left and root.val> x: return self.search(root.left, x)
        elif root.right and root.val < x: return self.search(root.right, x)
        else: return None
    def child_size(self, root):
        ret = 0
        if root.left: ret +=1
        if root.right: ret+=1
        return ret
    def preorder(self, root, parent=None):
        if root is None: return
        node = TreeNodeP(root.val, parent)
        node.left = self.preorder(root.left, node) if root.left else None
        node.right = self.preorder(root.right, node) if root.right else None
        return node
