import heapq
from collections import Counter
class Solution(object):
    def frequencySort(self, s):
        """
        :type s: str
        :rtype: str
        """
        sCounter = Counter(s)
        sCounterList = [(-j,i) for i,j in sCounter.items()]
        heapq.heapify(sCounterList)
        ret = ''
        while len(sCounterList)!=0:
            popped = heapq.heappop(sCounterList)
            ret += popped[1] * (-popped[0])
        return ret

s = 'tree'
sol = Solution()
print sol.frequencySort(s)
