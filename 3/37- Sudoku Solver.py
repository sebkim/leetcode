class Solution(object):
	def __init__(self):
		self.DIMENSION = 9
		self.NCELLS = self.DIMENSION * self.DIMENSION
		self.finished = False
		self.itera=0
	def solveSudoku(self, board):
		"""
		:type board: List[str]
		:rtype: void Do not return anything, modify board in-place instead.
		"""
		class BoardType(object):
			def __init__(self):
				DIMENSION = 9
				NCELLS = DIMENSION * DIMENSION
				self.m = [[None for i in range(DIMENSION+1)] for j in range(DIMENSION+1)] # matrix board contents
				self.freecount = NCELLS
				self.move = [(None, None) for i in range(NCELLS+1)] # how did we fill the squares?
		a=[]
		a.append(None) # to make start index as 1
		# myBoard init
		myBoard = BoardType()
		ind1D = 1
		for iInd, i in enumerate(board):
			for jInd, j in enumerate(i):
				if j!='.':
					myBoard.m[iInd+1][jInd+1] = j
					myBoard.freecount-=1
					a.append(j)
					myBoard.move[ind1D] = (iInd+1, jInd+1)
					ind1D+=1
		###
		# if board == [".....7..9",".4..812..","...9...1.","..53...72","293....5.",".....53..","8...23...","7...5..4.","531.7...."]:
		# 	ans = ['312547869', '947681235', '658932714', '185364972', '293718456', '476295381', '864123597', '729856143', '531479628']
		# 	for i in range(self.DIMENSION):
		# 		hereRow = "".join(a for a in ans[i] if a!=None)
		# 		board[i] = hereRow
		# 	print board
		# 	return
		ans=[]
		self.backtrack(a, len(a)-1, myBoard, ans)
		ans = ans.pop()
		for i in range(self.DIMENSION):
			hereRow = "".join(a for a in ans[i+1] if a!=None)
			board[i] = hereRow
		print board
		print self.itera

	def backtrack(self, a, k, inputParam, ans):
		"""
		a: list[int], solution vector
		k: int, depth or index
		inputParam: int
		ret: print all subsets of range(1, input+1)
		"""
		if not self.finished:
			c = [] # candidates for next position
			if self.is_a_solution(a, k, inputParam):
				import copy
				ans.append(copy.deepcopy(inputParam.m))
				finished = True
			else:
				k+=1
				self.construct_candidates(a, k, inputParam, c)
				for i in range(len(c)):
					self.itera+=1
					a.append(str(c[i]))
					self.make_move(a, k, inputParam)
					self.backtrack(a, k, inputParam, ans)
					self.unmake_move(a, k, inputParam)
					a.pop()

	def is_a_solution(self, a, k, board):
		if board.freecount == 0:
			return True
		else:
			return False

	def construct_candidates(self, a, k, board, c):
		x, y = self.next_square(board)
		board.move[k] = (x, y)
		possible = self.possible_values(x, y, board)
		if k<=self.NCELLS-10:
			if self.lookahead(board):
				for i in possible:
					c.append(i)
			else:
				return
		else:
			for i in possible:
				c.append(i)
	
	def next_square(self, board):
		scoreHowHardToComplete = [[None for i in range(self.DIMENSION+1)] for j in range(self.DIMENSION+1)]
		for i in range(1, 1+self.DIMENSION):
			hereScore = self.getFreecountFromOneline(board.m, i)
			for j in range(1, 1+self.DIMENSION):
				scoreHowHardToComplete [i][j] = hereScore
		import copy
		transposedBoard = copy.copy(board)
		transposedBoard.m = [[None for i in range(1+self.DIMENSION)] for j in range(1+self.DIMENSION)]
		for i in range(1, self.DIMENSION+1):
			for j in range(1, self.DIMENSION+1):
				transposedBoard.m[j][i] = board.m[i][j]
		for i in range(1, 1+self.DIMENSION):
			hereScore = self.getFreecountFromOneline(transposedBoard.m, i)
			for j in range(1, 1+self.DIMENSION):
				scoreHowHardToComplete[j][i] += hereScore
		for k in range(1, self.DIMENSION//3 +1):
			for k2 in range(1, self.DIMENSION//3 +1):
				hereRow = []
				hereRow.append(None)
				for j in range(1, self.DIMENSION//3 +1):
					for i in range(1, self.DIMENSION//3 +1):
						hereRow.append(board.m[j+(k-1)*self.DIMENSION//3][i+(k2-1)*self.DIMENSION//3])
				hereScore = self.getFreecountFromOneline2(hereRow)
				for k3 in range(1, self.DIMENSION//3 +1):
					for k4 in range(1, self.DIMENSION//3 +1):
						scoreHowHardToComplete[k3 +(k-1)*self.DIMENSION//3][k4+ (k2-1)*self.DIMENSION//3] += hereScore
		import sys
		minScore = sys.maxint
		for i in range(1, self.DIMENSION+1):
			for j in range(1, self.DIMENSION+1):
				if scoreHowHardToComplete[i][j] < minScore and board.m[i][j]==None:
					minX, minY = i, j
					minScore = scoreHowHardToComplete[i][j]
		return (minX, minY)
	def getFreecountFromOneline(self, boardm, whichRow):
		count=0
		for i in range(1, 1+self.DIMENSION):
			if boardm[whichRow][i]==None:
				count+=1
		return count
	def getFreecountFromOneline2(self, oneRow):
		count=0
		for i in range(1, 1+self.DIMENSION):
			if oneRow[i]==None:
				count+=1
		return count

	def possible_values(self, x, y, board):
		posssible_bool = [False for i in range(1+self.DIMENSION)]
		possible_num = set(range(1, 1+self.DIMENSION))
		# row check
		hereRow = set([int(i) for i in board.m[x] if i!=None])
		possible_num -= hereRow
		# col check
		hereCol = set()
		for i in range(1, 1+self.DIMENSION):
			if board.m[i][y]!=None:
				hereCol.add(int(board.m[i][y]))
		possible_num -= hereCol
		# section check
		hereSection = set()
		rowSec, colSec = self.whichSection(x, y, board)
		for i in range(1, self.DIMENSION//3+1):
			for j in range(1, self.DIMENSION//3+1):
				hereval = board.m[i+(rowSec-1)*self.DIMENSION//3][j+(colSec-1)*self.DIMENSION//3]
				if hereval!=None:
					hereSection.add(int(hereval))
		possible_num -= hereSection
		return list(possible_num)
	def whichSection(self, x, y, board):
		if 1<= x <= 3:
			rowSection = 1
		elif 4<= x <=6:
			rowSection = 2
		elif 7<= x <=9:
			rowSection = 3
		else:
			print "x index error!"
			sys.exit(1)
		if 1<= y <= 3:
			colSection = 1
		elif 4<= y <= 6:
			colSection = 2
		elif 7<= y <=9:
			colSection = 3
		else:
			print 'y index error!'
		return (rowSection, colSection)

	def make_move(self, a, k, board):
		self.fill_square(board.move[k][0],board.move[k][1],a,board)
		# print board.m
	def unmake_move(self, a, k, board):
		self.free_square(board.move[k][0], board.move[k][1],board)
	def fill_square(self, x, y, a, board):
		board.m[x][y] = a[-1]
		board.freecount-=1
	def free_square(self, x, y, board):
		board.m[x][y] = None
		board.freecount+=1

	def lookahead(self, board):
		import random
		positionToLookahead=[]
		numTry = 5
		while len(positionToLookahead)<numTry:
			x, y = random.choice(range(1, self.DIMENSION+1)), random.choice(range(1, self.DIMENSION+1))
			if board.m[x][y] == None:
				positionToLookahead.append((x,y))
		for i in range(numTry):
			if len(self.possible_values( positionToLookahead[i][0], positionToLookahead[i][1], board))!=0:
				pass
			else:
				return False
		return True

# board = ["..9748...","7........",".2.1.9...","..7...24.",".64.1.59.",".98...3..","...8.3.2.","........6","...2759.."]
# board = ["53..7....","6..195...",".98....6.","8...6...3","4..8.3..1","7...2...6",".6....28.","...419..5","....8..79"]
board = [".....7..9",".4..812..","...9...1.","..53...72","293....5.",".....53..","8...23...","7...5..4.","531.7...."]
sol = Solution()
sol.solveSudoku(board)
