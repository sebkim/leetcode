class Solution(object):
	def searchInsert(self, nums, target):
		"""
		:type nums: List[int]
		:type target: int
		:rtype: int
		"""
		return self.searchRec(nums, target, 0, len(nums)-1 )

	def searchRec(self, nums, target, lo, hi):
		if lo==hi:
			if nums[lo] == target:
				return lo
			elif nums[lo] > target:
				return lo
			else:
				return lo+1
		elif lo<hi:
			mid = lo + (hi-lo+1) // 2
			if nums[mid] == target:
				return mid
			elif nums[mid] > target:
				return self.searchRec(nums, target, lo ,mid-1)
			else:
				return self.searchRec(nums, target, mid+1 , hi)
		else:
			return lo

sol = Solution()
x=sol.searchInsert([1,3,5,6], 7)
print x
