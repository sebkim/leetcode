import copy

class Solution(object):
	def combinationSum(self, candidates, target):
		"""
		:type candidates: List[int]
		:type target: int
		:rtype: List[List[int]]
		"""
		candidates.sort()
		partialSol = []
		ans = []
		self.combiSumRec(candidates, target, ans, partialSol)
		return ans
	def combiSumRec(self, candidates, target, ans, partialSol):
		if sum(partialSol) == target:
			ans.append(copy.copy(partialSol))
			return
		nextCand = []
		topVal = partialSol[-1] if len(partialSol)>0 else 0
		for eachC in candidates:
			if eachC >= topVal and sum(partialSol) + eachC <= target:
				nextCand.append(eachC)
		for eachC in nextCand:
			partialSol.append(eachC)
			self.combiSumRec(candidates, target, ans, partialSol)
			partialSol.pop()

sol = Solution()
x=sol.combinationSum([2,3,6,7],7)
print x
