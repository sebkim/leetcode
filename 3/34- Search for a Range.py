class Solution(object):
	def searchRange(self, nums, target):
		"""
		:type nums: List[int]
		:type target: int
		:rtype: List[int]
		"""
		firstFindInd = self.searchRec(nums, target, 0, len(nums)-1)
		print firstFindInd
		if firstFindInd==-1:
			return (-1, -1)
		movingInd = firstFindInd
		while movingInd<len(nums) and nums[movingInd] == target:
			movingInd+=1
		movingInd-=1
		rightInd = movingInd
		movingInd = firstFindInd
		while movingInd>=0 and nums[movingInd] == target:
			movingInd-=1
		movingInd+=1
		leftInd = movingInd
		return (leftInd, rightInd)
	def searchRec(self, nums, target, lo, hi):
		if lo==hi:
			if nums[lo] == target:
				return lo
			else:
				return -1
		elif lo<hi:
			mid = lo + (hi-lo+1) // 2
			if nums[mid] == target:
				return mid
			elif nums[mid] > target:
				return self.searchRec(nums, target, lo ,mid-1)
			else:
				return self.searchRec(nums, target, mid+1 , hi)
		else:
			return -1


sol = Solution()
# x=sol.searchRange([1,2,3,4,4,5,6,7,7,7,7], 2)
x=sol.searchRange([1,2,3,4,4,5,6,7,7,7,7], 0)
print x
