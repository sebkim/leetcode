class Solution(object):
	def isValidSudoku(self, board):
		"""
		:type board: List[List[str]]
		:rtype: bool
		"""
		lenX = len(board[0])
		lenY = len(board)
	
		if not all(self.checkOneRowValid(i) for i in board):
			return False

		transposed = []
		for i in range(lenX):
			hereRow=[]
			for j in range(lenY):
				hereRow.append(board[j][i])
			transposed.append(hereRow)
		if not all(self.checkOneRowValid(i) for i in transposed):
			return False

		for k in range(lenY//3):
			for k2 in range(lenX//3):
				hereRow=[]
				for j in range(lenY//3):
					for i in range(lenX//3):
						hereRow.append(board[j+k*lenY//3][i+k2*lenX//3])
				if not self.checkOneRowValid(hereRow):
					return False
		return True

	def checkOneRowValid(self, oneRow):
		hereDic = {i: oneRow.count(i) for i in set(oneRow) }
		ret=False
		import operator
		if reduce(operator.add, hereDic.values())==9:
			hereDic.pop('.', None)
			if all(i<=1 for i in hereDic.values()):
				return True
		return ret


sol = Solution()
x=sol.isValidSudoku([[]])
print x
