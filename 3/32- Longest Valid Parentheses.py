class Solution(object):
	def longestValidParentheses(self, s):
		"""
		:type s: str
		:rtype: int
		"""
		stack=[-1]
		maxLen = 0
		for ind, e in enumerate(s):
			if e=='(':
				stack.append(ind)
			elif e==')':
				stack.pop()
				if len(stack) == 0:
					stack.append(ind)
				else:
					hereLen = ind - stack[-1]
					if hereLen > maxLen:
						maxLen = hereLen
		return maxLen

        
sol = Solution()
# x=sol.longestValidParentheses("(())))")
# x=sol.longestValidParentheses(")(()))")
x=sol.longestValidParentheses("()(())")
print x