class Solution(object):
	def nextPermutation(self, nums):
		"""
		:type nums: List[int]
		:rtype: void Do not return anything, modify nums in-place instead.
		"""
		highestI = None
		for i in range(len(nums)-2, -1, -1):
			if nums[i]<nums[i+1]:
				highestI = i
				break
	 	if highestI == None:
	 		nums.reverse()
	 		return
	 	highestJ=None
	 	for j in range(len(nums)-1, -1, -1):
	 		if nums[j] > nums[highestI]:
	 			highestJ = j
	 			break
	 	nums[highestI], nums[highestJ] = nums[highestJ], nums[highestI]
	 	partToReverse = nums[highestI+1:]
	 	partToReverse.reverse()
	 	nums[highestI+1:] = partToReverse
	 	# print nums

sol = Solution()
x=sol.nextPermutation([1,3,2])
print x
