class NumMatrix(object):
	def __init__(self, matrix):
		"""
		initialize your data structure here.
		:type matrix: List[List[int]]
		"""
		if matrix==[]:
			self.precalced = None
		else:
			self.precalced = [[ 0 for i in xrange(len(matrix[0]))] for j in xrange(len(matrix))]
			self.matrix = matrix
			self.precalc()
			print self.precalced
	def precalc(self):
		for row in xrange(len(self.matrix)):
			cum = 0
			for col in xrange(len(self.matrix[0])):
				cum += self.matrix[row][col]
				additional = 0
				if row>0: additional = self.precalced[row-1][col]
				self.precalced[row][col] = cum + additional

	def update(self, row, col, val):
		"""
		update the element at matrix[row,col] to val.
		:type row: int
		:type col: int
		:type val: int
		:rtype: void
		"""
		prevVal = self.matrix[row][col]
		self.matrix[row][col] = val
		diff = val - prevVal
		for r in xrange(row, len(self.matrix)):
			for c in xrange(col, len(self.matrix[0])):
				self.precalced[r][c] += diff
	def sumRegion(self, row1, col1, row2, col2):
		"""
		sum of elements matrix[(row1,col1)..(row2,col2)], inclusive.
		:type row1: int
		:type col1: int
		:type row2: int
		:type col2: int
		:rtype: int
		"""
		hereSum = self.precalced[row2][col2]
		if row1-1 >= 0:
			hereSum -= self.precalced[row1-1][col2]
		if col1-1 >= 0:
			hereSum -= self.precalced[row2][col1-1]
		if row1-1>=0 and col1-1>=0:
			hereSum += self.precalced[row1-1][col1-1]
		return hereSum
