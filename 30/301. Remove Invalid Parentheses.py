from collections import deque
class Solution(object):
	def removeInvalidParentheses(self, s):
		"""
		:type s: str
		:rtype: List[str]
		"""
		if len(s)==0: return [""]
		q = deque()
		discovered = set()
		q.append(s)
		discovered.add(s)
		# level is for stopping getting not minimum removal result.
		level = False
		res = []
		while len(q)!=0:
			here = q.popleft()
			if self.isValidParen(here):
				res.append(here)
				level=True
			if level:
				continue
			for i in xrange(len(here)):
				if here[i]!='(' and here[i]!=')':
					continue
				there = here[:i] + here[i+1:]
				if there not in discovered:
					discovered.add(there)
					q.append(there)
		return res
	def isValidParen(self, s):
		stack = []
		for i in s:
			if i!='(' and i!=')':
				continue
			if i=='(':
				stack.append(i)
			elif i==')':
				try:
					stack.pop()
				except:
					return False
		if len(stack)==0: return True
		return False

s = '()())()'
sol = Solution()
print sol.removeInvalidParentheses(s)
