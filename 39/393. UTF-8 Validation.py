class Solution(object):
	def validUtf8(self, data):
		"""
		:type data: List[int]
		:rtype: bool
		"""
		def onezeroCheck(data, upto):
			for i in xrange(1, upto+1):
				try:
					binRep = bin(data[eachInd+i])[2:]
					if len(binRep)>8: return False
					binRep = binRep.zfill(8)
					if binRep[:2] != '10': return False
				except:
					return False
			return True
		eachInd = 0
		while eachInd < len(data):
			binRep = bin(data[eachInd])[2:]
			if len(binRep)>8: return False
			binRep = binRep.zfill(8)
			if binRep[0] == '0':
				eachInd+=1
			elif binRep[:3] == '110':
				if not onezeroCheck(data, 1): return False
				eachInd+=2
			elif binRep[:4] == '1110':
				if not onezeroCheck(data, 2): return False
				eachInd+=3
			elif binRep[:5] == '11110':
				if not onezeroCheck(data, 3): return False
				eachInd+=4
			else:
				return False
		return True

data = [230, 136, 145]
sol  = Solution()
print sol.validUtf8(data)
