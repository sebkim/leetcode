class Solution(object):
	def isRectangleCover(self, rectangles):
		"""
		:type rectangles: List[List[int]]
		:rtype: bool
		"""
		area = 0
		corners = set()
		x1 = float('inf')
		y1 = float('inf')
		x2 = -float('inf')
		y2 = -float('inf')
		for rect in rectangles:
			x1 = min(x1, rect[0])
			y1 = min(y1, rect[1])
			x2 = max(x2, rect[2])
			y2 = max(y2, rect[3])
			area += (rect[2] - rect[0]) * (rect[3]-rect[1])
			s1 = str(rect[0]) + " " + str(rect[1])
			s2 = str(rect[2]) + " " + str(rect[1])
			s3 = str(rect[0]) + " " + str(rect[3])
			s4 = str(rect[2]) + " " + str(rect[3])
			ss = [s1, s2, s3, s4]
			for eachS in ss:
				if eachS in corners: corners.remove(eachS)
				else: corners.add(eachS)
		if not ( (str(x1)+" "+str(y1)) in corners and (str(x2)+" "+str(y1)) in corners and \
			(str(x1)+ " "+str(y2)) in corners and (str(x2)+" "+str(y2)) in corners ) or len(corners)!=4: return False
		if area == (x2-x1) * (y2-y1): return True
		return False
