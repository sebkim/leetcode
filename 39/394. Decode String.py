import string
class Solution(object):
	def decodeString(self, s):
		"""
		:type s: str
		:rtype: str
		"""
		return self.dfs(s)
	def dfs(self, here):
		chunks = self.splitByChunk(here)
		if len(chunks)==1 and all(i in string.ascii_lowercase for i in chunks[0]):
			return chunks[0]
		ret = ''
		for eachChunk in chunks:
			howMany = 1
			openBracInd = None
			closeBracInd = None
			for i in xrange(len(eachChunk)):
				if eachChunk[i] == '[':
					openBracInd = i
					break
			for i in xrange(len(eachChunk)-1, -1, -1):
				if eachChunk[i] == ']':
					closeBracInd = i
					break
			if openBracInd is None:
				ret += self.dfs(eachChunk)
				continue
			howMany = int(eachChunk[0:openBracInd])
			insideStr = eachChunk[openBracInd+1:closeBracInd]
			ret += howMany * self.dfs(insideStr)
		return ret
	# chunk means 2[ab] or 3[ab] or 3[ab5[bc]]
	def splitByChunk(self, s):
		numOpenBrac=0
		oneChunk = ''
		res = []
		for i in xrange(len(s)):
			oneChunk += s[i]
			if s[i] == '[':
				numOpenBrac+=1
			elif s[i] == ']':
				if numOpenBrac == 1:
					res.append(oneChunk)
					oneChunk = ''
					numOpenBrac = 0
				else:
					numOpenBrac-=1
			if i<len(s)-1:
				if s[i+1].isdigit() and numOpenBrac==0:
					res.append(oneChunk)
					oneChunk = ''
					continue
		res.append(oneChunk)
		newRes = []
		oneChunk = []
		for eachRes in res:
			if not eachRes.isdigit():
				if len(oneChunk) !=0:
					oneChunk.append(eachRes)
					newRes.append(''.join(oneChunk))
					oneChunk = []
				else:
					newRes.append(eachRes)
			else:
				oneChunk.append(eachRes)
		if newRes[-1] == '':
			newRes.pop()
		return newRes
		
s = "3[a2[c]]"
# s="3[a]2[bc]"
# s = "2[abc]ef3[cd]zz"
# s="2[2[b]]"
sol = Solution()
print sol.decodeString(s)
