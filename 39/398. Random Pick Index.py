from random import randrange
class Solution(object):
    def __init__(self, nums):
        """
        :type nums: List[int]
        :type numsSize: int
        """
        self.nums = nums
    def pick(self, target):
        """
        :type target: int
        :rtype: int
        """
        res = -1
        count = 1
        for i_ind, i in enumerate(self.nums):
            if i!=target: continue
            if randrange(count) == 0:
                count += 1
                res = i_ind
        return res


# Your Solution object will be instantiated and called as such:
NUMS = [1,2,3,3,4,3]
obj = Solution(NUMS)
target = 3
# param_1 = obj.pick(target)
from collections import defaultdict
import time
import random
res = defaultdict(int)
for i in xrange(1000):
    val = obj.pick(target)
    res[val]+=1
print res

# test = defaultdict(int)
# for i in xrange(10000):
#     val = random.randint(0, 4)
#     test[val] +=1
# print test
