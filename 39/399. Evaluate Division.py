class Solution(object):
	def __init__(self):
		self.adj = {}
	def calcEquation(self, equations, values, queries):
		"""
		:type equations: List[List[str]]
		:type values: List[float]
		:type queries: List[List[str]]
		:rtype: List[float]
		"""
		# create self.adj adjacent matrix
		nodeSet = set()
		for eachEqu in equations:
			nodeSet.add(eachEqu[0])
			nodeSet.add(eachEqu[1])
		for eachNode in nodeSet:
			for eachNode2 in nodeSet:
				if self.adj.get(eachNode) is None: self.adj[eachNode] = {}
				self.adj[eachNode][eachNode2] = float('inf')
		for eachEqu, eachVal in zip(equations, values):
			self.adj[eachEqu[0]][eachEqu[1]] = eachVal
			self.adj[eachEqu[1]][eachEqu[0]] = 1.0/eachVal
		##
		self.floyd(nodeSet)
		# print self.adj
		res = []
		for eachQ in queries:
			try:
				thing = self.adj[eachQ[0]][eachQ[1]]
				if thing==float('inf'):
					res.append(-1.0)
					continue
				res.append(thing)
			except KeyError:
				res.append(-1.0)
		return res
	def floyd(self, nodeSet):
		for i in nodeSet:
			self.adj[i][i] = 1.
		for k in nodeSet:
			for i in nodeSet:
				for j in nodeSet:
					val = self.adj[i][k] * self.adj[k][j]
					if self.adj[i][j] > val:
						self.adj[i][j] = val
			
equations = [ ["a", "b"], ["b", "c"] ]
values = [2.0, 3.0]
queries = [ ["a", "c"], ["b", "a"], ["a", "e"], ["a", "a"], ["x", "x"] ]
equations=[["a","e"],["b","e"]]
values=[4.0,3.0]
queries=[["a","b"],["e","e"],["x","x"]]
sol = Solution()
print sol.calcEquation(equations, values, queries)
