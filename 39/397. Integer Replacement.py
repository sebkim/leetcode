class Solution(object):
    def __init__(self):
        self.cached = None
    def integerReplacement(self, n):
        """
        :type n: int
        :rtype: int
        """
        self.cached = [[None] for _ in xrange(n+2)]
        return self.dp(n)
    def dp(self, n):
        if n==1:
            return 0
        ret = self.cached[n]
        if ret[0] is not None:
            return ret[0]
        if n%2 == 0:
            ret[0] = 1 + self.dp(n/2)
        else:
            ret[0] = self.dp(n+1) + 1
            ret[0] = min(ret[0], self.dp(n-1)+1)
        return ret[0]

SOL = Solution()
N = 7
print SOL.integerReplacement(N)
