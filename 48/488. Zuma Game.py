from collections import defaultdict
class Solution(object):
    def __init__(self):
        self.hand_dic = defaultdict(int)
    def findMinStep(self, board, hand):
        """
        :type board: str
        :type hand: str
        :rtype: int
        """
        for each in hand:
            self.hand_dic[each]+=1
        ret = self.backtrack(board+'#')
        if ret == float('inf'): return -1
        return ret
    def backtrack(self, s):
        s = self.remove_consec(s)
        if s == '#': return 0
        i=0; j=0
        ret = float('inf')
        while j<len(s):
            try:
                if s[i] == s[j]: continue
                need = 3 - (j-i)
                if need <= self.hand_dic[s[i]]:
                    self.hand_dic[s[i]] -= need
                    ret = min(ret, need + self.backtrack(s[0:i] + s[j:]))
                    self.hand_dic[s[i]] += need
                i=j
            finally:
                j+=1
        return ret
    def remove_consec(self, s):
        i=0; j=0
        while j<len(s):
            try:
                if s[i] == s[j]: continue
                if j-i>=3:
                    return self.remove_consec(s[0:i] + s[j:])
                i=j
            finally:
                j+=1
        return s

BOARD = "WWRRBBWW"
HAND = "WRBRW"
SOL = Solution()
print SOL.findMinStep(BOARD, HAND)
