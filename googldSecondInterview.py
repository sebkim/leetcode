# Please use this Google doc to code during your interview. To free your hands for coding, we recommend that you use a headset or a phone with speaker option.

# [1, 4, 7, 9, 2, -1]

# [1, 2, 3, 4, 5, 6, 7, 8, 9, 100, 99, 98, 97, 96, 95, 94, 93, 92, 91, 8, 7, 6, 5, 4, 3, 2, 1]

# [100, 99, 98, 97, 96, 0, -1, -2, -3] for this case

# [1, 2, 3, 4, 5, 6, 7, 8, 9] 

aList = [1, 4, 7, 9, 2, -1]
aList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 100, 99, 98, 97, 96, 95, 94, 93, 92, 91, 8, 7, 6, 5, 4, 3, 2, 1]
aList = [100, 99, 98, 97, 96, 0, -1, -2, -3]
aList = [1, 2, 3, 4, 5, 6, 7, 8, 9]

def findMax():
	return helper(0, len(aList)-1)
def helper(lo, hi):
	if lo<hi:
		if len(aList[lo:hi+1]) <= 3:
			return max(aList[lo:hi+1])
		aabInd = (2*lo + hi) / 3
		abbInd = (lo + 2*hi) / 3
		if aList[aabInd] > aList[abbInd]:
			return helper(lo, abbInd)
		else:
			return helper(aabInd, hi)

print findMax()
print max(aList)
