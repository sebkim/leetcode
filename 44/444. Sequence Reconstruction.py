class Solution(object):
    def sequenceReconstruction(self, org, seqs):
        """
        :type org: List[int]
        :type seqs: List[List[int]]
        :rtype: bool
        """
        orgind = {num: i for i, num in enumerate([None] + org)}
        pairset = set(zip([None] + org, org))
        for seq in seqs:
            for a, b in zip([None] + seq, seq):
                if a==b: return False
                if orgind.get(a, -1) > orgind.get(b, -2): return False
                pairset.discard((a, b))
        return not pairset

ORG = [1,2,3]
SEQ = [[1,2], [1,3]]
ORG = [1]
SEQ = [[], []]
SOL = Solution()
print SOL.sequenceReconstruction(ORG, SEQ)
