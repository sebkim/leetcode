# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Codec:
	def __init__(self):
		self.data = []
		self.preInd = 0
	def serialize(self, root):
		"""Encodes a tree to a single string.

		:type root: TreeNode
		:rtype: str
		"""
		if root is None: return
		self.data.append(root.val)
		if root.left:
			self.serialize(root.left)
		if root.right:
			self.serialize(root.right)
		return self.data
	def deserialize(self, data):
		"""Decodes your encoded data to tree.

		:type data: str
		:rtype: TreeNode
		"""
		if data is None: return
		preorder = data
		inorder = sorted(data)
		# print preorder, inorder
		return self.constructFromPreInOrder(preorder, inorder, 0, len(preorder))
	def constructFromPreInOrder(self, preorder, inorder, start, end):
		firstElem = preorder[self.preInd]
		node = TreeNode(firstElem)
		middleInd = start + inorder[start:end].index(firstElem)
		self.preInd+=1
		if inorder[start:middleInd]:
			node.left = self.constructFromPreInOrder(preorder, inorder, start, middleInd)
		if inorder[middleInd+1: end]:
			node.right = self.constructFromPreInOrder(preorder, inorder, middleInd+1, end)
		return node


# Your Codec object will be instantiated and called as such:
# codec = Codec()
# codec.deserialize(codec.serialize(root))