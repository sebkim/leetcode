class Solution(object):
	def __init__(self):
		self.adj = None
		self.visited = None
		self.charSet = None
	def alienOrder(self, words):
		"""
		:type words: List[str]
		:rtype: str
		"""
		if len(set(words)) == 1: return words[0]
		self.adj = [ set() for _ in xrange(26)]
		self.visited = [False for _ in xrange(26)]
		self.charSet = set()
		boolBreak = False
		# scan words
		for eachWordInd in xrange(len(words)-1):
			firstWord = words[eachWordInd]
			secondWord = words[eachWordInd+1]
			for i in firstWord:
				self.charSet.add(i)
			for i in secondWord:
				self.charSet.add(i)
			for charInd in xrange(min(len(firstWord), len(secondWord))):
				diffCharInFirst = firstWord[charInd]
				diffCharInSecond = secondWord[charInd]
				if diffCharInFirst != diffCharInSecond:
					self.adj[ord(diffCharInFirst) - ord('a')].add(ord(diffCharInSecond) - ord('a'))
					boolBreak = True
					break
			if boolBreak is False and len(firstWord) > len(secondWord):
				return ""
		##
		res = self.topSort()
		# print res
		res2 = filter(lambda x: chr(x + ord('a')) in self.charSet, res)
		res2 = [chr(i+ord('a')) for i in res2]
		return ''.join(res2)
	def topSort(self):
		res = []
		for i in self.charSet:
			i = ord(i) - ord('a')
			if self.visited[i] is False:
				self.dfs(i, res)
		res = list(reversed(res))
		for i in xrange(len(res)):
			for j in xrange(i+1, len(res)):
				if res[i] in self.adj[res[j]]:
					return []
		return res
	def dfs(self, here, res):
		self.visited[here] = True
		for there in self.adj[here]:
			if self.visited[there] is False:
				self.dfs(there, res)
		res.append(here)

words = [
  "wrt",
  "wrf",
  "er",
  "ett",
  "rftt"
]
words = ["wrtkj","wrt"]
# words = ['xz', 'xy']
# words = ['ab', 'ac', 'ab']
sol = Solution()
print sol.alienOrder(words)

