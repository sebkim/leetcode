from collections import Counter
class Solution(object):
    def canPermutePalindrome(self, s):
        """
        :type s: str
        :rtype: bool
        """
        c = Counter(s)
        lens = len(s)
        onetime = False
        for k, v in c.items():
            if v%2==1 and onetime is True:
                return False
            elif v%2==1 and onetime is False:
                onetime = True
        return True
