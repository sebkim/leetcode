class Solution(object):
    def __init__(self):
        self.nocycle = True
        self.visited = None
        self.finished = None
        self.adj_list = None
        self.parent = None
    def validTree(self, n, edges):
        """
        :type n: int
        :type edges: List[List[int]]
        :rtype: bool
        """
        self.visited = [False for _ in xrange(n)]
        self.finished = [False for _ in xrange(n)]
        self.parent = [None for _ in xrange(n)]
        self.adj_list = [list() for _ in xrange(n)]
        for edge in edges:
            self.adj_list[edge[0]].append(edge[1])
            self.adj_list[edge[1]].append(edge[0])
        n_component = 0
        for i in xrange(n):
            if self.visited[i] is False:
                n_component += 1
                self.dfs(i)
        return self.nocycle and (n_component==1)
    def dfs(self, here):
        if self.nocycle is not False:
            self.visited[here] = True
            for there in self.adj_list[here]:
                if self.visited[there] is False:
                    self.parent[there] = here
                    self.dfs(there)
                elif self.parent[here] != there and  self.finished[there] is False:
                    self.nocycle = False
            self.finished[here] = True
